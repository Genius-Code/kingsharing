<?php


use App\Http\Controllers\User\ChangePasswordController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\InboxController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\ReviewController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

    Route::group(['prefix' => 'user', 'as' => 'users.', 'middleware' => 'auth'], function () {

        Route::get('/index', [HomeController::class, 'index'])->name('home');
        /***=====================================================================***/
        Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
           Route::get('/index', [ProfileController::class, 'index'])->name('index');
           Route::put('/update', [ProfileController::class, 'update'])->name('update');
        });
        /***=====================================================================***/
        Route::group(['prefix' => 'messages', 'as' => 'message.'], function () {
            Route::get('/index', [InboxController::class, 'index'])->name('index');
            Route::get('/show/{message}', [InboxController::class, 'show'])->name('show');
            Route::get('/sendMessage', [InboxController::class, 'sendMessagePage'])->name('send-message-page');
            Route::post('/sendMessage', [InboxController::class, 'sendMessage'])->name('send-message');
        });
        /***=====================================================================***/
        Route::group(['prefix' => 'change-password', 'as' => 'password.'], function () {
           Route::get('/index', [ChangePasswordController::class, 'index'])->name('index');
           Route::put('/update/{user}', [ChangePasswordController::class, 'update'])->name('update');
        });
        /*--------------------Review-------------------------*/
        Route::group(['prefix' => 'review', 'as' => 'review.'], function (){
            Route::get('/', [ReviewController::class, 'index'])->name('index');
            Route::post('/store', [ReviewController::class, 'store'])->name('store');
        });
    });

});


