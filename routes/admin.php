<?php


use App\Http\Controllers\Admin\AdsController;
use App\Http\Controllers\Admin\AssetsController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CccamServerController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\InboxController;
use App\Http\Controllers\Admin\OfferController;
use App\Http\Controllers\Admin\PackageServerController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

    Route::group(['prefix' => 'admin', 'middleware' => ['auth','isAdmin'],'as' => 'admin.'], function (){

        Route::get('/', [HomeController::class, 'index'])->name('dashboard');

        Route::group(['prefix' => 'assets', 'as' => 'assets.'], function (){
            Route::get('/index', [AssetsController::class, 'index'])->name('index');
            Route::get('/create', [AssetsController::class, 'create'])->name('create');
            Route::post('/store', [AssetsController::class, 'store'])->name('store');
            Route::get('/edit/{asset}', [AssetsController::class, 'edit'])->name('edit');
            Route::put('/update', [AssetsController::class, 'update'])->name('update');
            Route::delete('/delete', [AssetsController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'sliders', 'as' => 'slider.'], function (){
            Route::get('/index', [SliderController::class, 'index'])->name('index');
            Route::get('/create', [SliderController::class, 'create'])->name('create');
            Route::post('/store', [SliderController::class, 'store'])->name('store');
            Route::get('/edit/{slider}', [SliderController::class, 'edit'])->name('edit');
            Route::put('/update', [SliderController::class, 'update'])->name('update');
            Route::delete('/delete/{slider}', [SliderController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'banners', 'as' => 'banner.'], function (){
            Route::get('/index', [BannerController::class, 'index'])->name('index');
            Route::get('/create', [BannerController::class, 'create'])->name('create');
            Route::post('/store', [BannerController::class, 'store'])->name('store');
            Route::get('/edit/{banner}', [BannerController::class, 'edit'])->name('edit');
            Route::put('/update', [BannerController::class, 'update'])->name('update');
            Route::delete('/delete/{banner}', [BannerController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'servers', 'as' => 'server.'], function (){
            Route::get('/index', [PackageServerController::class, 'index'])->name('index');
            Route::get('/create', [PackageServerController::class, 'create'])->name('create');
            Route::post('/store', [PackageServerController::class, 'store'])->name('store');
            Route::get('/show/{server}', [PackageServerController::class, 'show'])->name('show');
            Route::get('/edit/{server}', [PackageServerController::class, 'edit'])->name('edit');
            Route::put('/update', [PackageServerController::class, 'update'])->name('update');
            Route::delete('/delete/{server}', [PackageServerController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'offers', 'as' => 'offer.'], function (){
            Route::get('/index', [OfferController::class, 'index'])->name('index');
            Route::get('/create', [OfferController::class, 'create'])->name('create');
            Route::post('/store', [OfferController::class, 'store'])->name('store');
            Route::get('/edit/{offer}', [OfferController::class, 'edit'])->name('edit');
            Route::put('/update', [OfferController::class, 'update'])->name('update');
            Route::delete('/delete/{offer}', [OfferController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'ads', 'as' => 'ads.'], function (){
            Route::get('/index', [AdsController::class, 'index'])->name('index');
            Route::get('/create', [AdsController::class, 'create'])->name('create');
            Route::post('/store', [AdsController::class, 'store'])->name('store');
            Route::get('/edit/{ads}', [AdsController::class, 'edit'])->name('edit');
            Route::put('/update', [AdsController::class, 'update'])->name('update');
            Route::delete('/delete/{ads}', [AdsController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'blog', 'as' => 'blog.'], function (){
            Route::get('/index', [BlogController::class, 'index'])->name('index');
            Route::get('/create', [BlogController::class, 'create'])->name('create');
            Route::post('/store', [BlogController::class, 'store'])->name('store');
            Route::get('/edit/{blog}', [BlogController::class, 'edit'])->name('edit');
            Route::put('/update', [BlogController::class, 'update'])->name('update');
            Route::delete('/delete/{blog}', [BlogController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'review', 'as' => 'review.'], function (){
            Route::get('/index', [ReviewController::class, 'index'])->name('index');
            Route::get('/unread', [ReviewController::class, 'unread'])->name('unread');
            Route::post('/store', [ReviewController::class, 'store'])->name('store');
            Route::put('/update/{review}', [ReviewController::class, 'update'])->name('update');
            Route::delete('/delete/{review}', [ReviewController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
            Route::get('/index', [ProfileController::class, 'index'])->name('index');
            Route::put('/update', [ProfileController::class, 'update'])->name('update');
            Route::get('/change-password', [ProfileController::class, 'passwordPage'])->name('passwordPage');
            Route::put('/change-password', [ProfileController::class, 'changePassword'])->name('changePassword');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'messages', 'as' => 'message.'], function () {
            Route::get('/index', [InboxController::class, 'index'])->name('index');
            Route::get('/deleted-massages', [InboxController::class, 'deletedMessages'])->name('deleted-massages');
            Route::get('/replay/{message}', [InboxController::class, 'repalyToUserPage'])->name('repaly-to-user-page');
            Route::post('/replay', [InboxController::class, 'repalyToUser'])->name('repaly-to-user');
            Route::get('/show/{message}', [InboxController::class, 'show'])->name('show');
            Route::delete('/delete/{message}', [InboxController::class, 'delete'])->name('delete');
            Route::delete('/soft-delete/{message}', [InboxController::class, 'softDelete'])->name('soft-delete');
            Route::get('/restore-messages/{id}', [InboxController::class, 'restoreDeletedMeassges'])->name('restore-deleted-message');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'contacts', 'as' => 'mail.'], function () {
            Route::get('/index', [ContactController::class, 'index'])->name('index');
            Route::get('/show/{contact}', [ContactController::class, 'show'])->name('show-contact');
            Route::delete('/delete/{contact}', [ContactController::class, 'delete'])->name('delete-contact');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'users', 'as' => 'user.'], function () {
            Route::get('/index', [UserController::class, 'index'])->name('index');
            Route::get('/show/profile/{user}', [UserController::class, 'show'])->name('show-user');
            Route::get('/edit/{user}', [UserController::class, 'edit'])->name('edit-user');
            Route::get('/create', [UserController::class, 'create'])->name('create');
            Route::post('/store', [UserController::class, 'store'])->name('store');
            Route::put('/update/{user}', [UserController::class, 'update'])->name('update');
            Route::delete('/delete/{user}', [UserController::class, 'delete'])->name('delete-user');
        });
        /***-------------------------------------------------------------------------------***/
        Route::group(['prefix' => 'cccam', 'as' => 'cccam.'], function (){
            Route::get('/index', [CccamServerController::class, 'index'])->name('index');
            Route::get('/create', [CccamServerController::class, 'create'])->name('create');
            Route::post('/store', [CccamServerController::class, 'store'])->name('store');
            Route::get('/edit/{cccam}', [CccamServerController::class, 'edit'])->name('edit');
            Route::put('/update', [CccamServerController::class, 'update'])->name('update');
            Route::delete('/delete/{cccam}', [CccamServerController::class, 'destroy'])->name('destroy');
        });
        /***-------------------------------------------------------------------------------***/
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

    });

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function (){
        Route::get('/signin', [AuthController::class, 'loginPage'])->name('loginPage');
        Route::post('/login', [AuthController::class, 'login'])->name('login');
    });
});


