<?php

use App\Http\Controllers\EndUser\AuthController;
use App\Http\Controllers\EndUser\BlogController;
use App\Http\Controllers\EndUser\CccamServerController;
use App\Http\Controllers\EndUser\ContactController;
use App\Http\Controllers\EndUser\FacebookController;
use App\Http\Controllers\EndUser\GoogleController;
use App\Http\Controllers\EndUser\HomeController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

    Route::group(['prefix' => '/', 'as' => 'user.'], function (){
        Route::get('/index', [HomeController::class, 'index'])->name('index');
        /*--------------------ContactUs-------------------------*/
        Route::group(['prefix' => 'contact', 'as' => 'contact.'], function (){
            Route::get('/', [ContactController::class, 'index'])->name('index');
            Route::post('/send-message', [ContactController::class, 'sendMessage'])->name('send-message');
        });
        /*--------------------Blogs-------------------------*/
        Route::group(['prefix' => 'blog', 'as' => 'blog.'], function (){
            Route::get('/blogs', [BlogController::class, 'index'])->name('index');
            Route::get('/show/{blog}', [BlogController::class, 'show'])->name('show');
        });
        /*--------------------Blogs-------------------------*/
        Route::group(['prefix' => 'cccam', 'as' => 'cccam.'], function (){
            Route::get('/packages', [CccamServerController::class, 'index'])->name('index');
        });
        /*--------------------LogOut--------------------------*/
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
        /*--------------------Privacy-Policy-----------------*/
        Route::get('/privacy-policy', function (){
           return view('EndUser.privacy');
        })->name('privacy');
    });
    /*---------Auth------------*/
    Route::get('/auth', [AuthController::class, 'authPage'])->name('auth');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::get('/register', [AuthController::class, 'registerPage'])->name('registerPage');
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::get('/forget-password', [AuthController::class, 'forgetPasswordPage'])->name('forget-password-page');
    Route::post('/forget-password', [AuthController::class, 'forgetPassword'])->name('forget-password');
    Route::get('/reset-password/{token}', [AuthController::class, 'resetPasswordPage'])->name('reset-password-page');
    Route::post('/reset-password', [AuthController::class, 'resetPassword'])->name('reset-password');
    /*---------facebook-login------------*/
    Route::get('/auth/facebook-redirect', [FacebookController::class, 'handleFacebookRedirect'])->name('handle-facebook-redirect');
    Route::get('/auth/facebook-callback', [FacebookController::class, 'handleFacebookCallback'])->name('handle-facebook-callbackt');
    /*---------google-login------------*/
    Route::get('/auth/google-redirect', [GoogleController::class, 'handleGoogleRedirect'])->name('handle-google-redirect');
    Route::get('/auth/google-callback', [GoogleController::class, 'handleGoogleCallback'])->name('handle-google-callbackt');


});


