{
  // Nav Menu in Media 767px
  const navMenu = document.querySelector("nav");
  const closeBtnNav = document.querySelector(".closeBtnNav");
  const toggle = document.querySelector(".toggle");

  toggle.addEventListener("click", function () {
    // Add Class Active When click on bar btn
    navMenu.classList.add("activeNav");
  });

  closeBtnNav.addEventListener("click", function () {
    // Remove class active when click on close btn
    navMenu.classList.remove("activeNav");
  });
}

{
  // Animation load
  const onloadAnim = document.querySelector(".animation");
  function loaderAnimation() {
    onloadAnim.classList.add("show");
  }
  function hideAnimation() {
    setInterval(loaderAnimation, 5000);
  }
  window.onload = hideAnimation;
}

{
  // client slider
  const leftArrow = document.getElementById("left_arrow");
  const rightArrow = document.getElementById("right_arrow");
  const slider = document.querySelectorAll(".slider .slide_one");
  let m = 0;
  const arrayClient = Array.from(slider);
  // console.log(arrayClient)

  arrayClient[m].classList.add("active");

  leftArrow.addEventListener("click", function () {
    arrayClient[m].classList.remove("active");
    m++;

    if (m >= arrayClient.length) {
      m = 0;
    }
    arrayClient[m].classList.add("active");
  });

  rightArrow.addEventListener("click", function () {
    arrayClient[m].classList.remove("active");
    m++;

    if (m >= arrayClient.length) {
      m = 0;
    }
    arrayClient[m].classList.add("active");
  });
}

{
  // Scroll Button To Top
  let scrollTop = document.querySelector(".scrollTop");

  window.onscroll = function () {
    if (this.scrollY >= 300) {
      // This == Window
      scrollTop.classList.add("activeTop"); // Add class active
    } else {
      scrollTop.classList.remove("activeTop"); // Remove class active
    }
  };

  scrollTop.onclick = function () {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
}

{
  //Fixed Header
  let mainHeader = document.querySelector(".main_Header");

  window.addEventListener("scroll", function () {
    if (window.scrollY >= 300) {
      // Bage >= 300 add class active To nav header
      mainHeader.classList.add("activeHeader");
    } else {
      // Bage < 300 remove class active To nav header
      mainHeader.classList.remove("activeHeader");
    }
  });
}

{
  // Icon Gear Social
  const iconGear = document.getElementsByClassName("iconGear")[0];
  const allIcons = document.querySelector(".allIcons");
  const iconGearmain = () => {
    allIcons.classList.toggle("activeicon");
  };
  iconGear.addEventListener("click", function () {
    iconGearmain();
  });
}

{
  // swipper TeacherOne
  var swiper = new Swiper(".teachers-sliderOne", {
    loop: true,
    grabCursor: true,
    spaceBetween: 20,
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
      },
      991: {
        slidesPerView: 3,
      },
    },
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
  });
}
{
  // swipper Teacher
  var swiper = new Swiper(".teachers-slider", {
    loop: true,
    grabCursor: true,
    spaceBetween: 20,
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
      },
      991: {
        slidesPerView: 3,
      },
    },
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
  });
}
