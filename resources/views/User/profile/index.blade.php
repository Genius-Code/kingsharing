@extends('User.layouts.master')

@section('title')
    {{ trans('user/profile.Update Profile') }}
@endsection

@section('content')
    <!-- container -->
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('users.home') }}">{{ trans('user/profile.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-black-50">{{ trans('user/profile.Update Profile') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->

        <!-- row -->
        <div class="row row-sm">
            <!-- Col -->
            <div class="col-lg-4">
                <div class="card mg-b-20">
                    <div class="card-body">
                        <div class="pl-0">
                            <div class="main-profile-overview">
                                <div class="main-img-user profile-user">
                                    <img alt="" src="{{ $data->image }}"><a class="fas fa-camera profile-edit" href="JavaScript:void(0);"></a>
                                </div>
                                <div class="d-flex justify-content-between mg-b-20">
                                    <div>
                                        <h5 class="main-profile-name">{{ auth()->user()->name }}</h5>
                                        <p class="main-profile-name-text font-weight-bold">{{ auth()->user()->roles->role->name }}</p>
                                    </div>
                                </div>
                                <hr class="mg-y-30">
                                <label class="main-content-label tx-13 mg-b-20">{{ trans('user/profile.Social') }}</label>
                                <div class="main-profile-social-list">

                                    <div class="media">
                                        <div class="media-icon bg-success-transparent text-primary">
                                            <i class="icon ion-logo-facebook"></i>
                                        </div>
                                        <div class="media-body">
                                            <span>{{ trans('user/profile.Facebook') }}</span> <a href="">{{ isset($data->facebook_url) ? $data->facebook_url : trans('user/profile.no-data') }}</a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-icon bg-info-transparent text-success">
                                            <i class="icon ion-logo-whatsapp"></i>
                                        </div>
                                        <div class="media-body">
                                            <span>{{ trans('user/profile.whatsapp-Number') }}</span> <a href="">{{ isset($data->whatsapp_number) ? $data->whatsapp_number : trans('user/profile.no-data') }}</a>
                                        </div>
                                    </div>

                                </div>
                                <hr class="mg-y-30">
                                <div class="card mg-b-20">
                                    <div class="card-body">
                                        <div class="main-content-label tx-13 mg-b-25">
                                            {{ trans('user/profile.Conatct') }}
                                        </div>
                                        <div class="main-profile-contact-list">
                                            <div class="media">
                                                <div class="media-icon bg-primary-transparent text-primary">
                                                    <i class="icon ion-md-phone-portrait"></i>
                                                </div>
                                                <div class="media-body">
                                                    <span>{{ trans('user/profile.Mobile') }}</span>
                                                    <div>
                                                        {{ $data->userProfile->phone }}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="media">
                                                <div class="media-icon bg-info-transparent text-info">
                                                    <i class="icon ion-md-locate"></i>
                                                </div>
                                                <div class="media-body">
                                                    <span>{{ trans('user/profile.Current Address') }}</span>
                                                    <div>
                                                        {{ isset($data->country) ? $data->country : trans('user/profile.no-data') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- main-profile-contact-list -->
                                    </div>
                                </div>
                            </div><!-- main-profile-overview -->
                        </div>
                    </div>
                </div>

            </div>

            <!-- Col -->
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-4 main-content-label">{{ trans('user/profile.Personal Information') }}</div>
                        <form class="form-horizontal" method="post" action="{{ route('users.profile.update') }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="profile_id" value="{{ $data->id }}">
                            <div class="mb-4 main-content-label">{{ trans('user/profile.Name') }}</div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.User Name') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('name') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('user/profile.User Name_ph') }}" name="name" value="{{ old('name', $data->userProfile->name) }}">
                                        @error('name')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="mb-4 main-content-label">{{ trans('user/profile.Contact Info') }}</div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.Email') }}<i>({{ trans('user/profile.required') }})</i></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('email') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('user/profile.Email_ph') }}" name="email" value="{{ old('email', $data->userProfile->email) }}">
                                        @error('email')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.Phone') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('phone') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('user/profile.Phone_ph') }}" name="phone" value="{{ old('phone', $data->userProfile->phone) }}">
                                        @error('phone')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.Country') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea class="form-control @error('country') is-invalid fparsley-error parsley-error @enderror" name="country" rows="2"  placeholder="{{ trans('user/profile.Country_ph') }}">{{ old('country', $data->country) }}</textarea>
                                        @error('country')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-4 main-content-label">{{ trans('user/profile.Social Info') }}</div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.Facebook') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('facebook') is-invalid fparsley-error parsley-error @enderror"  name="facebook" placeholder="{{ trans('user/profile.Facebook_ph') }}" value="{{ old('facebook', $data->facebook_url) }}">
                                        @error('facebook')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.whatsapp-Number') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('whatsapp') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('user/profile.whatsapp Number_ph') }}" name="whatsapp" value="{{ old('whatsapp', $data->whatsapp_number) }}">
                                        @error('whatsapp')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="mb-4 main-content-label">{{ trans('user/profile.Upload Image Profile') }}</div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('user/profile.Upload Photo') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" name="image"  class="form-control @error('image') is-invalid fparsley-error parsley-error @enderror"/>
                                        @error('image')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{ trans('user/profile.Update Profile') }}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /Col -->
        </div>
        <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
@endsection
