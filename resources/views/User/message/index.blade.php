@extends('User.layouts.master')

@section('title')
    {{ trans('user/message.Messages') }} | {{ trans('user/message.InBox') }}
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('users.home') }}">{{ trans('user/message.dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('users.message.index') }}"><h5 class="text-black-50">{{ trans('user/message.Inbox') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col">
            <div class="card">
                <div class="main-content-body main-content-body-mail card-body">
                    <div class="main-mail-header">
                        <div>
                            <h4 class="main-content-title mg-b-5">{{ trans('user/message.Inbox') }}</h4>
                            <p>{{ trans('user/message.You have') }} {{ count($messages) }}</p>
                        </div>

                            <div class="d-flex justify-content-between">
                                <a href="{{ route('users.message.send-message') }}" class="btn btn-warning-gradient">{{ trans('user/message.Send New Message') }}</a>
                            </div>
                    </div><!-- main-mail-list-header -->

                    @if (isset($allMessages) && $allMessages->count() > 0)
                        @foreach($allMessages as $message)
                            <div class="main-mail-list">
                                <div class="main-mail-item">

                                    <div class="main-mail-star">
                                        <i class="typcn typcn-star"></i>
                                    </div>
                                    <div class="main-img-user"><img alt="" src="{{ isset(auth()->user()->profileUser()->image) ? auth()->user()->profileUser()->image : URL::asset('assetsAdmin/shared/img/faces/5.jpg') }}"></div>
                                    <div class="main-mail-body">
                                        <div class="main-mail-from">
                                            {{ ucfirst($message->sender->roles->role->name) }}
                                        </div>
                                        <div class="main-mail-subject">
                                            <a href="{{ route('users.message.show', $message) }}"><strong>{{ $message->message->title }}</strong></a>
                                        </div>
                                    </div>
                                    @if($message->file)
                                    <div class="main-mail-attachment">
                                        <a href="{{ $message->file }}" download> <i class="typcn typcn-attachment"></i></a>
                                    </div>
                                    @endif
                                    <div class="main-mail-date mt-4">
                                        {{ $message->created_at->format("d/m/y  H:i A") }}
                                    </div>
                                </div>
                            </div>
                            <div class="mg-lg-b-15"></div>
                        @endforeach
                    @endif
                    <div class="flex justify-content-center my-5">
                        {!! $allMessages -> links() !!}
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection

@section('js')

    <!--- Internal Check-all-mail js --->
    <script src="{{ URL::asset('assetsAdmin/shared/js/check-all-mail.js') }}"></script>
@endsection
