@extends('User.layouts.master')

@section('title')
    {{ trans('user/message.Show Message') }}
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('users.home') }}">{{ trans('user/message.dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('users.message.index') }}"><h5 class="text-black-50">{{ trans('user/message.Inbox') }} </h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $message->recieved->name }} <span class="badge badge-primary">{{ trans('user/message.Inbox') }}</span></h4>
                </div>
                <div class="card-body">
                    <div class="email-media">
                        <div class="mt-0 d-sm-flex">
                            <img class="mr-2 rounded-circle avatar-xl" src="{{ isset($message->message->sender->userProfile->image) ? $message->message->sender->userProfile->image : URL::asset('assetsAdmin/shared/img/faces/6.jpg') }}" alt="avatar">
                            <div class="media-body">
                                <div class="float-right d-none d-md-flex fs-15">
                                    <span class="mr-3">{{ $message->message->created_at->format("d/m/y  H:i A") }}</span>
                                </div>
                                <div class="media-title  font-weight-bold mt-3">{{ ucfirst($message->message->sender->roles->role->name) }} <span class="text-muted">( {{ ucfirst($message->message->sender->name) }} )</span></div>
                                <small class="mr-2 d-md-none">{{ $message->message->created_at->format("d/m/y  H:i A") }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="eamil-body mt-5">
                        <h6>{{ $message->message->title }}</h6>
                        <p> {!! nl2br($message->message->description) !!}</p>
                        <hr>
                    </div>
                <h3>Replay From Admin</h3>
                <div class="card-body">
                    <div class="email-media">
                        <div class="mt-0 d-sm-flex">
                            <img class="mr-2 rounded-circle avatar-xl" src="{{ isset($message->sender->userProfile->image) ? $message->sender->userProfile->image : URL::asset('assetsAdmin/shared/img/faces/6.jpg') }}" alt="avatar">
                            <div class="media-body">
                                <div class="float-right d-none d-md-flex fs-15">
                                    <span class="mr-3">{{ $message->created_at->format("d/m/y  H:i A") }}</span>
                                </div>
                                <div class="media-title  font-weight-bold mt-3">{{ ucfirst($message->sender->roles->role->name) }} <span class="text-muted">( {{ ucfirst($message->sender->name) }} )</span></div>
                                <small class="mr-2 d-md-none">{{ $message->created_at->format("d/m/y  H:i A") }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="eamil-body mt-5">
                        <h6>{{ $message->message->title }}</h6>
                        <p> {!! nl2br($message->replay) !!}</p>
                        <hr>
                        @if (!is_null($message->file))
                            <div class="email-attch">
                                <div class="float-right">
                                    <a href="{{ isset($message->file) ? $message->file : '' }}"><i class="bx bxs-download tx-18" data-toggle="tooltip" title="" data-original-title="Download"></i></a>
                                </div>

                                <p>{{ trans('user/message.Files') }}</p>
                                <div class="emai-img">
                                    <div class="d-sm-flex">
                                        <div class=" m-2">
                                            <a href="{{ $message->file }}"><img class="wd-150 mb-2" src="{{ isset($message->file) ? $message->file : '' }}" alt="placeholder file"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

