@extends('User.layouts.master')

@section('title')
    {{ trans('user/message.Messages') }} | {{ trans('user/message.Send Message') }}
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('users.home') }}">{{ trans('user/message.dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('users.message.index') }}"><h5 class="text-black-50">{{ trans('user/message.Inbox') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->

        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('user/message.Inbox') }}</h4>
                        <i class="mdi mdi-dots-horizontal text-gray"></i>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ trans('user/message.Send Message') }}</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('users.message.send-message') }}" method="post" enctype="application/x-www-form-urlencoded">
                                @csrf
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <label class="col-sm-2">{{ trans('user/message.Subject') }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @error('title') is-invalid fparsley-error parsley-error @enderror" name="title" value="{{ old('title') }}" placeholder="{{ trans('user/message.Subject_ph') }}">
                                            @error('title')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row ">
                                        <label class="col-sm-2">{{ trans('user/message.Message') }}</label>
                                        <div class="col-sm-10">
                                            <textarea rows="10" class="form-control @error('description') is-invalid fparsley-error parsley-error @enderror" name="description" placeholder="{{ trans('user/message.Message_ph') }}">{{ old('description') }}</textarea>
                                            @error('description')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row ">
                                        <label class="col-sm-2">{{ trans('user/message.Upload') }}</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control-file @error('file') is-invalid fparsley-error parsley-error @enderror" name="file">
                                            @error('file')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer d-sm-flex">
                                    <div class="btn-list ml-auto">
                                        <button type="submit" class="btn btn-danger btn-space">{{ trans('user/message.Send') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
