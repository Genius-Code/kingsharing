@extends('User.layouts.master')

@section('title')
    {{ trans('user/home.Dashboard User') }} | {{ trans('user/home.Kingsharing') }}
@endsection

@section('content')
    <!-- container -->
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="left-content">
                <div>
                    <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">{{ trans('user/home.Hi') }} {{ auth()->user()->name }}, {{ trans('user/home.welcome back!') }}</h2>
                </div>
            </div>

        </div>
        <!-- /breadcrumb -->

        <!-- row -->
        <div class="row row-sm">
            <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                <div class="card overflow-hidden sales-card bg-primary-gradient">
                    <div class="pl-3 pt-3 pr-3 pb-2 pt-0">
                        <div class="">
                            <h6 class="mb-3 tx-12 text-white">TODAY ORDERS</h6>
                        </div>
                        <div class="pb-0 mt-0">
                            <div class="d-flex">
                                <div class="">
                                    <h4 class="tx-20 font-weight-bold mb-1 text-white">$5,74.12</h4>
                                    <p class="mb-0 tx-12 text-white op-7">Compared to last week</p>
                                </div>
                                <span class="float-right my-auto ml-auto">
												<i class="fas fa-arrow-circle-up text-white"></i>
												<span class="text-white op-7"> +427</span>
											</span>
                            </div>
                        </div>
                    </div>
                    <span id="compositeline" class="pt-1">5,9,5,6,4,12,18,14,10,15,12,5,8,5,12,5,12,10,16,12</span>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                <div class="card overflow-hidden sales-card bg-danger-gradient">
                    <div class="pl-3 pt-3 pr-3 pb-2 pt-0">
                        <div class="">
                            <h6 class="mb-3 tx-12 text-white">TODAY EARNINGS</h6>
                        </div>
                        <div class="pb-0 mt-0">
                            <div class="d-flex">
                                <div class="">
                                    <h4 class="tx-20 font-weight-bold mb-1 text-white">$1,230.17</h4>
                                    <p class="mb-0 tx-12 text-white op-7">Compared to last week</p>
                                </div>
                                <span class="float-right my-auto ml-auto">
                                    <i class="fas fa-arrow-circle-down text-white"></i>
                                    <span class="text-white op-7"> -23.09%</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <span id="compositeline2" class="pt-1">3,2,4,6,12,14,8,7,14,16,12,7,8,4,3,2,2,5,6,7</span>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                <div class="card overflow-hidden sales-card bg-success-gradient">
                    <div class="pl-3 pt-3 pr-3 pb-2 pt-0">
                        <div class="">
                            <h6 class="mb-3 tx-12 text-white">TOTAL EARNINGS</h6>
                        </div>
                        <div class="pb-0 mt-0">
                            <div class="d-flex">
                                <div class="">
                                    <h4 class="tx-20 font-weight-bold mb-1 text-white">$7,125.70</h4>
                                    <p class="mb-0 tx-12 text-white op-7">Compared to last week</p>
                                </div>
                                <span class="float-right my-auto ml-auto">
                                    <i class="fas fa-arrow-circle-up text-white"></i>
                                    <span class="text-white op-7"> 52.09%</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <span id="compositeline3" class="pt-1">5,10,5,20,22,12,15,18,20,15,8,12,22,5,10,12,22,15,16,10</span>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                <div class="card overflow-hidden sales-card bg-warning-gradient">
                    <div class="pl-3 pt-3 pr-3 pb-2 pt-0">
                        <div class="">
                            <h6 class="mb-3 tx-12 text-white">PRODUCT SOLD</h6>
                        </div>
                        <div class="pb-0 mt-0">
                            <div class="d-flex">
                                <div class="">
                                    <h4 class="tx-20 font-weight-bold mb-1 text-white">$4,820.50</h4>
                                    <p class="mb-0 tx-12 text-white op-7">Compared to last week</p>
                                </div>
                                <span class="float-right my-auto ml-auto">
                                    <i class="fas fa-arrow-circle-down text-white"></i>
                                    <span class="text-white op-7"> -152.3</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <span id="compositeline4" class="pt-1">5,9,5,6,4,12,18,14,10,15,12,5,8,5,12,5,12,10,16,12</span>
                </div>
            </div>
        </div>
        <!-- row closed -->

        <!-- row opened -->
        <div class="row row-sm">
            <div class="col-md-12 col-lg-12 col-xl-7 mx-auto">
                <div class="card">
                    <div class="card-header bg-transparent pd-b-0 pd-t-20 bd-b-0">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title mb-0">{{ trans('user/home.Client status') }}</h4>
                            <i class="mdi mdi-dots-horizontal text-gray"></i>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="total-revenue">
                        </div>
                        <div class="sales-bar mt-4">
                            <p class="text-center"><i class="fas fa-network-wired fa-2x mr-4"></i>  {{ $locationData->ip }}</p>
                            <p class="text-center"><i class="fas fa-map-marked-alt fa-2x mr-4"></i> {{ $locationData->countryName }}, {{ $locationData->cityName }}</p>
                            <p class="text-center"><i class="fas fa-drafting-compass fa-2x mr-4"></i> {{ $locationData->latitude }}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- row closed -->

        <div class="card-body">
            <!-- row -->
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="card">
                        <div class="card-body">

                            <form method="post" action="{{ route('users.review.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="pd-30 pd-sm-40 bg-gray-200">

                                    <div class="col-lg-12 col-md-12 mt-5 mx-auto">

                                        <div class="row">
                                            <div class="col">
                                                <h4>{{ trans('user/review.add-comment') }}</h4>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="main-content-label mg-b-5">
                                                                {{ trans('user/review.add-review') }}
                                                            </div>

                                                            <div class="col-lg">
                                                                <textarea class="form-control @error('comment') is-invalid fparsley-error parsley-error @enderror" name="desc_ar" placeholder=" {{ trans('user/review.comment_ph') }}" rows="3">{{ old('comment') }}</textarea>
                                                                @error('comment')
                                                                <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                               <p>{{ $message }}</p>
                                                                            </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>


                                    <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit"> {{ trans('admin/action.Create') }}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /row -->

        </div>


    </div>
    <!-- /Container -->
@endsection
