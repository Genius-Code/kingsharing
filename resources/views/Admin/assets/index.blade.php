@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/assets.Assets') }}
@endsection



@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/assets.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.assets.index') }}"><h5 class="text-white-50">{{ trans('admin/assets.Settings') }} /</h5></a> </span>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.assets.create') }}"><h5 class="text-white-50">{{ trans('admin/assets.Add New Setting') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
        <div class="card mg-b-20">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">{{ trans('admin/assets.Assets WebSite') }}</h4>
                    <a href="{{ route('admin.assets.create') }}" class="btn btn-warning-gradient">{{ trans('admin/assets.Create New') }}</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table key-buttons text-md-nowrap">
                        <thead>
                        <tr>
                            <th class="border-bottom-0 text-center">{{ trans('admin/assets.Email') }}</th>
                            <th class="border-bottom-0 text-center">{{ trans('admin/assets.Logo') }}</th>
                            <th class="border-bottom-0 text-center">{{ trans('admin/assets.Phone') }}</th>
                            <th class="border-bottom-0 text-center">{{ trans('admin/assets.Address') }}</th>
                            <th class="border-bottom-0 text-center">{{ trans('admin/assets.FaceBook') }}</th>
                            <th class="border-bottom-0 text-center">{{ trans('admin/action.Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($settings) && $settings->count() > 0)
                            @foreach($settings as $setting)
                                <tr>
                                    <td class="text-center">{{ $setting->email }}</td>
                                    <td class="text-center"><img class="img-thumbnail-round" width="120px" height="120px" src="{{ $setting->logo }}"></td>
                                    <td class="text-center">{{ $setting->phone }}</td>
                                    <td class="text-center">{{ $setting->address }}</td>
                                    <td class="text-center">{{ $setting->facebook }}</td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <button aria-expanded="false" aria-haspopup="true" class="btn ripple btn-info-gradient"
                                                    data-toggle="dropdown" type="button">{{ trans('admin/action.Choose Action') }}<i class="fas fa-caret-down ml-1"></i></button>
                                            <div class="dropdown-menu tx-13">
                                                <a class="dropdown-item" href="{{ route('admin.assets.edit', $setting) }}">{{ trans('admin/action.Edit') }}</a>

                                                    <form action="{{ route('admin.assets.destroy') }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="hidden" name="asset_id" value="{{ $setting->id }}">
                                                        <button type="submit" class="dropdown-item">{{ trans('admin/action.Delete') }}</button>
                                                    </form>

                                            </div>
                                        </div>

                                    </td>

                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('js')
    <!-- Internal Data tables -->
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/pdfmake.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.bootstrap4.min.js')}}"></script>

        <!--Internal  Datatable js -->
        <script src="{{ URL::asset('assetsAdmin/shared/js/table-data.js')}}"></script>
@endsection
