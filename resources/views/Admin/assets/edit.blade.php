@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/assets.Edit Assets') }}
@endsection

@section('css')
    <!--- Internal Fileupload css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <!--- Internal Fancy uploader css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
    <!--- Internal Sumoselect css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/assets.Dashboard') }}Dashboard / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.assets.index') }}"><h5 class="text-white-50">{{ trans('admin/assets.Settings') }}</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-5"> / {{ trans('admin/assets.Edit Asset') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/assets.Setting') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="main-content-label mg-b-5">
                                        {{ trans('admin/assets.Update  Setting') }}
                                    </div>
                                    <form method="post" action="{{ route('admin.assets.update') }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="asset_id" value="{{ $setting->id }}">
                                        <div class="pd-30 pd-sm-40 bg-gray-200">
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.Email') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('email') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.Email_ph') }}" name="email" type="email" value="{{ old('email', $setting->email) }}">
                                                    @error('email')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.Phone') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('phone') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.Phone_ph') }}" name="phone" type="text" value="{{ old('phone', $setting->phone) }}">
                                                    @error('phone')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.Address') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('address') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.Address_ph') }}" name="address" type="text" value="{{ old('address', $setting->address) }}">
                                                    @error('address')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.FaceBook Page Url') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('facebook') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.facebook_ph') }}" name="facebook" type="url" value="{{ old('facebook', $setting->facebook) }}">
                                                    @error('facebook')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.telegram') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('telegram') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.telegram_ph') }}" name="telegram" type="url" value="{{ old('telegram', $setting->telegram) }}">
                                                    @error('telegram')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.youtube') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('youtube') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.youtube_ph') }}" name="youtube" type="url" value="{{ old('youtube', $setting->youtube) }}">
                                                    @error('youtube')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/assets.instagram') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('instagram') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/assets.instagram_ph') }}" name="instagram" type="url" value="{{ old('instagram', $setting->instagram) }}">
                                                    @error('instagram')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-8 mg-t-5 mg-md-t-0 mx-auto">
                                                <label class="form-label">{{ trans('admin/assets.Logo') }} <span title="required" class="tx-danger">*</span></label>
                                                <input type="file" class="dropify @error('logo') is-invalid fparsley-error parsley-error @enderror" name="logo"  data-width="280" data-height="420"/>
                                                @error('logo')
                                                <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                @enderror
                                            </div>

                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">{{ trans('admin/action.update') }}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>
@endsection
