@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/cccam.update-new-package') }}
@endsection

@section('css')
    <!--- Internal Fileupload css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <!--- Internal Fancy uploader css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
    <!--- Internal Sumoselect css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">
    <!--Internal  Quill css -->
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.bubble.css') }}" rel="stylesheet">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/cccam.dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.cccam.index') }}"><h5 class="text-white-50">{{ trans('admin/cccam.cccam') }}</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50"> / {{ trans('admin/cccam.update-new-package') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/cccam.cccam') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="main-content-label mg-b-5">
                                        {{ trans('admin/cccam.update-new-package') }}
                                    </div>
                                    <form method="post" action="{{ route('admin.cccam.update', $cccam) }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="pd-30 pd-sm-40 bg-gray-200">
                                            <div class="col-lg-12 col-md-12 mt-5 mx-auto">
                                                <div class="row mb-5 mx-auto">
                                                    <div class="col-6">
                                                        <div class="col">
                                                            <label class="form-label mg-b-0">{{ trans('admin/cccam.name_ar') }}</label>
                                                        </div>
                                                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                            <input class="form-control @error('name_ar') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/cccam.name_ar_ph') }}" name="name_ar" type="text" value="{{ old('name_ar', $cccam->getTranslation('name', 'ar')) }}">
                                                            @error('name_ar')
                                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="col">
                                                            <label class="form-label mg-b-0">{{ trans('admin/cccam.name_en') }}</label>
                                                        </div>
                                                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                            <input class="form-control @error('name_en') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/cccam.name_en_ph') }}" name="name_en" type="text" value="{{ old('name_en', $cccam->getTranslation('name', 'en')) }}">
                                                            @error('name_en')
                                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-5 mx-auto">
                                                    <div class="col">
                                                        <div class="col-md-6">
                                                            <label class="form-label mg-b-0">{{ trans('admin/cccam.duration') }}</label>
                                                        </div>
                                                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                            <input class="form-control @error('duration') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/cccam.duration_ph') }}" name="duration" type="number" value="{{ old('duration', $cccam->duration) }}">
                                                            @error('duration')
                                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="col-md-6">
                                                            <label class="form-label mg-b-0">{{ trans('admin/cccam.price') }}</label>
                                                        </div>
                                                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                            <input class="form-control @error('price') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/cccam.price_ph') }}" name="price" type="number" value="{{ old('price', $cccam->price) }}">
                                                            @error('price')
                                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="main-content-label mg-b-5">
                                                                        {{ trans('admin/cccam.details-en') }}
                                                                    </div>
                                                                    <div class="col-lg">
                                                                        <textarea class="form-control @error('desc_en') is-invalid fparsley-error parsley-error @enderror" name="desc_en" placeholder="{{ trans('admin/cccam.details-en_ph') }}" rows="3">{{ old('desc_en', $cccam->details) }}</textarea>
                                                                        @error('desc_en')
                                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                                 <p>{{ $message }}</p>
                                                                             </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">{{ trans('admin/action.update') }}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
    <!-- Internal Form-editor js -->
    <script src="{{ URL::asset('assetsAdmin/shared/js/form-editor.js')}}"></script>
    <!--Internal quill js -->
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.min.js')}}"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection
