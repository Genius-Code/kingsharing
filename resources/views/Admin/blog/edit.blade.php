@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/blog.update-blog') }}
@endsection

@section('css')
    <!--- Internal Fileupload css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <!--- Internal Fancy uploader css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
    <!--- Internal Sumoselect css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">
    <!--Internal  Quill css -->
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.bubble.css') }}" rel="stylesheet">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/blog.dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.blog.index') }}"><h5 class="text-white-50">{{ trans('admin/blog.blog') }}</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50"> / {{ trans('admin/blog.update-blog') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/blog.blog') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="main-content-label mg-b-5">
                                        {{ trans('admin/blog.create-new-blog') }}
                                    </div>
                                    <form method="post" action="{{ route('admin.blog.update') }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="blog_id" value="{{ $blog->id }}">
                                        <div class="pd-30 pd-sm-40 bg-gray-200">
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/blog.title-en') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('title_en') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/blog.title-en_ph') }}" name="title_en" type="text" value="{{ $blog->getTranslation('title', 'en') }}">
                                                        @error('title_en')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/blog.title-ar') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('title_ar') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/blog.title-ar_ph') }}" name="title_ar" type="text" value="{{ $blog->getTranslation('title', 'ar') }}">
                                                        @error('title_ar')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-lg-12 col-md-12 mt-5 mx-auto">

                                                <div class="row">
                                                    <div class="col">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="main-content-label mg-b-5">
                                                                        {{ trans('admin/blog.description') }}
                                                                    </div>
                                                                    <div class="col-lg">
                                                                        <div class="ql-wrapper ql-wrapper-demo bg-gray-100">
                                                                            <div>
                                                                                <textarea id="editor" class="form-control @error('description') is-invalid fparsley-error parsley-error @enderror" style="background-color: #ccc" name="description" placeholder="  {{ trans('admin/blog.description') }}" rows="3">{!! $blog->description !!}</textarea>
                                                                                @error('description')
                                                                                <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                                 <p>{{ $message }}</p>
                                                                             </span>
                                                                                @enderror
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-8 mg-t-5 mg-md-t-0 mx-auto">
                                                <label class="form-label">  {{ trans('admin/blog.image') }} <span title="required" class="tx-danger">*</span></label>
                                                <input type="file" class="dropify @error('image') is-invalid fparsley-error parsley-error @enderror" name="image"  data-width="280" data-height="420"/>
                                                @error('image')
                                                <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                @enderror
                                            </div>

                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">  {{ trans('admin/action.Update') }}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
    <!-- Internal Form-editor js -->
    <script src="{{ URL::asset('assetsAdmin/shared/js/form-editor.js')}}"></script>
    <!--Internal quill js -->
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.min.js')}}"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection
