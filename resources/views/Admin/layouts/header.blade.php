<div class="main-header sticky side-header nav nav-item">
    <div class="container-fluid">
        <div class="main-header-left ">
            <div class="responsive-logo">
                <a href="index.html"><img src="{{URL::asset('assetsAdmin/shared/img/brand/logo.png')}}" class="logo-1" alt="logo"></a>
                <a href="index.html"><img src="{{URL::asset('assetsAdmin/shared/img/brand/logo-white.png')}}" class="dark-logo-1" alt="logo"></a>
                <a href="index.html"><img src="{{URL::asset('assetsAdmin/shared/img/brand/favicon.png')}}" class="logo-2" alt="logo"></a>
                <a href="index.html"><img src="{{URL::asset('assetsAdmin/shared/img/brand/favicon.png')}}" class="dark-logo-2" alt="logo"></a>
            </div>
            <div class="app-sidebar__toggle" data-toggle="sidebar">
                <a class="open-toggle" href="#"><i class="header-icon fe fe-align-left" ></i></a>
                <a class="close-toggle" href="#"><i class="header-icons fe fe-x"></i></a>
            </div>
            <div class="main-header-center ml-3 d-sm-none d-md-none d-lg-block">
                <input class="form-control" placeholder="{{ trans('admin/dashboard.search_ph') }}..." type="search"> <button class="btn"><i class="fas fa-search d-none d-md-block"></i></button>
            </div>
        </div>
        <div class="main-header-right">
            <ul class="nav">
                <li class="">
                    <div class="dropdown  nav-itemd-none d-md-flex">
                        <a href="#" class="d-flex  nav-item nav-link pr-0 country-flag1" data-toggle="dropdown" aria-expanded="false">
                            <span class="avatar country-Flag mr-0 align-self-center bg-white"><img src="{{URL::asset('assetsAdmin/shared/img/flags/language.svg')}}" width="256px" height="256px" alt="language" title="language"></span>
                        </a>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <a rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                @if($properties['native']=="English")
                                    <div class="dropdown-menu dropdown-menu-left dropdown-menu-arrow" x-placement="bottom-end">
                                        <a href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" class="dropdown-item d-flex ">
                                            <span class="avatar  mr-3 align-self-center bg-transparent"><img src="{{URL::asset('assetsAdmin/shared/img/flags/us_flag.jpg')}}" alt="img"></span>
                                            <div class="d-flex">
                                                <span class="mt-2">English</span>
                                            </div>
                                        </a>
                                        @elseif($properties['native']=="العربية")
                                            <a href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" class="dropdown-item d-flex">
                                                <span class="avatar  mr-3 align-self-center bg-transparent"><img src="{{URL::asset('assetsAdmin/shared/img/flags/egypt.png')}}" alt="img"></span>
                                                <div class="d-flex">
                                                    <span class="mt-2">Arabic</span>
                                                </div>
                                            </a>
                                        @endif
                                        @endforeach
                                    </div>
                    </div>
                </li>
            </ul>
            <div class="nav nav-item  navbar-nav-right ml-auto">
                <div class="nav-link" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-btn">
								<button type="reset" class="btn btn-default">
									<i class="fas fa-times"></i>
								</button>
								<button type="submit" class="btn btn-default nav-link resp-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="header-icon-svgs" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
								</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="dropdown nav-item main-header-message ">
                    <a class="new nav-link" href="#"><svg xmlns="http://www.w3.org/2000/svg" class="header-icon-svgs" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg><span class="{{ (count($countMessages) > 0) ? 'pulse-danger' : ''}}"></span></a>
                    <div class="dropdown-menu">
                        <div class="menu-header-content bg-primary text-left">
                            <div class="d-flex">
                                <h6 class="dropdown-title mb-1 tx-15 text-white font-weight-semibold">{{ trans('admin/dashboard.Messages') }}</h6>
                                <span class="badge badge-pill badge-warning ml-auto my-auto float-right">{{ trans('admin/dashboard.Un Read Messages') }}</span>
                            </div>
                            <p class="dropdown-title-text subtext mb-0 text-white op-6 pb-0 tx-12 ">{{ trans('admin/dashboard.You have') }} {{ $countMessages->count() }} {{ trans('admin/dashboard.unread messages') }}</p>
                        </div>
                        <div class="main-message-list chat-scroll">
                            @forelse($messages as $message)
                            <a href="#" class="p-3 d-flex border-bottom">
                                <div class="  drop-img  cover-image  " data-image-src="{{ isset($message->sender->userProfile->image) ?  $message->sender->userProfile->image : URL::asset('assetsAdmin/shared/img/faces/3.jpg') }}">
                                    <span class="avatar-status bg-teal"></span>
                                </div>
                                <div class="wd-90p">
                                    <div class="d-flex">
                                        <h5 class="mb-1 name">{{ $message->sender->name }}</h5>
                                    </div>
                                    <p class="mb-0 desc">{{ $message->description }}</p>
                                    <p class="time mb-0 text-left float-left ml-2 mt-2">{{ $message->created_at->format("d/m/y  H:i A") }}</p>
                                </div>
                            </a>
                            @empty
                            @endforelse

                        </div>
                        <div class="text-center dropdown-footer">
                            <a href="{{ route('admin.message.index') }}">{{ trans('admin/dashboard.VIEW ALL') }}</a>
                        </div>
                    </div>
                </div>
                <div class="dropdown nav-item main-header-notification">
                    <a class="new nav-link" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" class="header-icon-svgs" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg><span class=" {{ (count($contactMails) > 0) ? 'pulse' : '' }}"></span></a>
                    <div class="dropdown-menu">
                        <div class="menu-header-content bg-primary text-left">
                            <div class="d-flex">
                                <h6 class="dropdown-title mb-1 tx-15 text-white font-weight-semibold">{{ trans('admin/dashboard.Notifications') }}</h6>
                                <span class="badge badge-pill badge-warning ml-auto my-auto float-right"> {{ trans('admin/dashboard.UnRead Mails') }}</span>
                            </div>
                            <p class="dropdown-title-text subtext mb-0 text-white op-6 pb-0 tx-12 ">{{ trans('admin/dashboard.You have') }} {{ count($contactMails) }} {{ trans('admin/dashboard.unread Notifications') }}</p>
                        </div>
                        <div class="main-notification-list Notification-scroll">
                            @foreach($contactMails as $contactMail)
                                <a class="d-flex p-3 border-bottom" href="{{ route('admin.mail.show-contact', $contactMail) }}">
                                    <div class="notifyimg bg-pink">
                                        <i class="la la-file-alt text-white"></i>
                                    </div>
                                    <div class="ml-3">
                                        <h5 class="notification-label mb-1">{{ $contactMail->subject }}</h5>
                                        <div class="notification-subtext">{{ $contactMail->created_at->format("d/m/y  H:i A") }}</div>
                                    </div>
                                    <div class="ml-auto" >
                                        <i class="las la-angle-right text-right text-muted"></i>
                                    </div>
                                </a>
                            @endforeach

                        </div>
                        <div class="dropdown-footer">
                            <a href="{{ route('admin.mail.index') }}">{{ trans('admin/dashboard.VIEW ALL') }}</a>
                        </div>
                    </div>
                </div>
                <div class="nav-item full-screen fullscreen-button">
                    <a class="new nav-link full-screen-link" href="#"><svg xmlns="http://www.w3.org/2000/svg" class="header-icon-svgs" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-maximize"><path d="M8 3H5a2 2 0 0 0-2 2v3m18 0V5a2 2 0 0 0-2-2h-3m0 18h3a2 2 0 0 0 2-2v-3M3 16v3a2 2 0 0 0 2 2h3"></path></svg></a>
                </div>
                <div class="dropdown main-profile-menu nav nav-item nav-link">
                    <a class="profile-user d-flex" href=""><img alt="" src="{{URL::asset('assetsAdmin/shared/img/faces/6.jpg')}}"></a>
                    <div class="dropdown-menu">
                        <div class="main-header-profile bg-primary p-3">
                            <div class="d-flex wd-100p">
                                <div class="main-img-user"><img alt="" src="{{URL::asset('assetsAdmin/shared/img/faces/6.jpg')}}" class=""></div>
                                <div class="ml-3 my-auto">
                                    <h6>{{ Auth::user()->name }}</h6><span>{{ ucfirst(Auth::user()->roles->role->name) }}</span>
                                </div>
                            </div>
                        </div>
                        <a class="dropdown-item" href="{{ route('admin.profile.index') }}"><i class="bx bx-user-circle"></i>{{ trans('admin/dashboard.Profile') }}</a>
                        <a class="dropdown-item" href="{{ route('admin.profile.passwordPage') }}"><i class="bx bx-lock-alt"></i> {{ trans('admin/dashboard.Change-Password') }}</a>
                        <form method="post" action="{{route('admin.logout')}}">
                            @csrf
                            <button class="dropdown-item" type="submit"><i class="bx bx-log-out"></i> {{ trans('admin/dashboard.Sign Out') }}</button>
                        </form>
                    </div>
                </div>
                <div class="dropdown main-header-message right-toggle">
                    <a class="nav-link pr-0" data-toggle="sidebar-right" data-target=".sidebar-right">
                        <svg xmlns="http://www.w3.org/2000/svg" class="header-icon-svgs" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
