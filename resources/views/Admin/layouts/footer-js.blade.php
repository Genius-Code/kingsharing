<!-- JQuery min js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Bundle js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!--Internal  Chart.bundle js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/chart.js/Chart.bundle.min.js')}}"></script>

<!-- Ionicons js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/ionicons/ionicons.js')}}"></script>

<!-- Moment js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/moment/moment.js')}}"></script>

<!--Internal Sparkline js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Moment js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/raphael/raphael.min.js')}}"></script>

<!--Internal  Flot js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery.flot/jquery.flot.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery.flot/jquery.flot.pie.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery.flot/jquery.flot.resize.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery.flot/jquery.flot.categories.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/js/dashboard.sampledata.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/js/chart.flot.sampledata.js')}}"></script>

<!-- Custom Scroll bar Js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/mscrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<!--Internal Apexchart js-->
<script src="{{URL::asset('assetsAdmin/shared/js/apexcharts.js')}}"></script>

<!-- Rating js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/rating/jquery.rating-stars.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/rating/jquery.barrating.js')}}"></script>

<!--Internal  Perfect-scrollbar js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/perfect-scrollbar/p-scroll.js')}}"></script>

<!-- Eva-icons js -->
<script src="{{URL::asset('assetsAdmin/shared/js/eva-icons.min.js')}}"></script>

<!-- right-sidebar js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/sidebar/sidebar.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/sidebar/sidebar-custom.js')}}"></script>

<!-- Sticky js -->
<script src="{{URL::asset('assetsAdmin/shared/js/sticky.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/js/modal-popup.js')}}"></script>

<!-- Left-menu js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/side-menu/sidemenu.js')}}"></script>

<!-- Internal Map -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>

<!--Internal  index js -->
<script src="{{URL::asset('assetsAdmin/shared/js/index-dark.js')}}"></script>

<!-- Apexchart js-->
<script src="{{URL::asset('assetsAdmin/shared/js/apexcharts.js')}}"></script>

<!-- custom js -->
<script src="{{URL::asset('assetsAdmin/shared/js/custom.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/js/jquery.vmap.sampledata.js')}}"></script>

@include('sweetalert::alert')
@yield('js')
