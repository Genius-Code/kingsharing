<!-- Favicon -->
<link rel="icon" href="{{URL::asset('assetsAdmin/shared/img/brand/favicon.png')}}" type="image/x-icon"/>
@if(app()->getLocale() == 'ar')
    <!-- animate css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/ar/css/animate.css')}}">
    {{--    <link href="{{URL::asset('assetsAdmin/ar/css/boxed.css')}}" rel="stylesheet">--}}
    {{--    <link href="{{URL::asset('assetsAdmin/ar/css/dark-boxed.css')}}" rel="stylesheet">--}}
    <!-- Icons css -->
    <link href="{{URL::asset('assetsAdmin/ar/css/icons.css')}}" rel="stylesheet">
    <!-- Sidemenu css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/ar/css/sidemenu.css')}}">
    {{--    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/ar/css/sidemenu1.css')}}">--}}
    {{--    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/ar/css/closed-sidemenu.css')}}">--}}
    {{--    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/ar/css/sidemenu-responsive-tabs.css')}}">--}}
    <!-- style css -->
    <link href="{{URL::asset('assetsAdmin/ar/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assetsAdmin/ar/css/style-dark.css')}}" rel="stylesheet">
    <!---Skinmodes css-->
    <link href="{{URL::asset('assetsAdmin/ar/css/skin-modes.css')}}" rel="stylesheet" />

@else
    <!-- Icons css -->
    <link href="{{URL::asset('assetsAdmin/en/css/icons.css')}}" rel="stylesheet">

    <!-- Sidemenu css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/en/css/sidemenu.css')}}">

    <!-- style css -->
    <link href="{{URL::asset('assetsAdmin/en/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assetsAdmin/en/css/style-dark.css')}}" rel="stylesheet">

    <!---Skinmodes css-->
    <link href="{{URL::asset('assetsAdmin/en/css/skin-modes.css')}}" rel="stylesheet" />
@endif
@yield('css')
<!-- Maps css -->
<link href="{{URL::asset('assetsAdmin/shared/plugins/jqvmap/jqvmap.min.css')}}" rel="stylesheet">
<!--  Owl-carousel css-->
<link href="{{URL::asset('assetsAdmin/shared/plugins/owl-carousel/owl.carousel.css')}}" rel="stylesheet" />

<!--  Custom Scroll bar-->
<link href="{{URL::asset('assetsAdmin/shared/plugins/mscrollbar/jquery.mCustomScrollbar.css')}}" rel="stylesheet"/>

<!--  Right-sidemenu css -->
<link href="{{URL::asset('assetsAdmin/shared/plugins/sidebar/sidebar.css')}}" rel="stylesheet">
