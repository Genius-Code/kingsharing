@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/action.update') }} {{ $server->name }}
@endsection

@section('css')
    <!--- Internal Fileupload css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <!--- Internal Fancy uploader css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
    <!--- Internal Sumoselect css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/package-server.Dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.server.index') }}"><h5 class="text-white-50">{{ trans('admin/package-server.PackageServers') }}</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50"> / {{ trans('admin/package-server.Update') }} {{ $server->name }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/package-server.PackageServers') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <form method="post" action="{{ route('admin.server.update') }}">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="server_id" value="{{ $server->id }}">
                                        <div class="pd-30 pd-sm-40 bg-gray-200">
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/package-server.name-en') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('name_en') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/package-server.name-en') }}" name="name_en" type="text" value="{{ $server->getTranslation('name', 'en')}}">
                                                        @error('name_en')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/package-server.name-ar') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('name_ar') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/package-server.name-en') }}" name="name_ar" type="text" value="{{ $server->getTranslation('name', 'ar') }}">
                                                        @error('name_ar')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/package-server.price') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('price') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/package-server.price') }}" name="price" type="number" value="{{ $server->price }}">
                                                        @error('price')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-6">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/package-server.duration') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('duration') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/package-server.duration') }}" name="duration" type="number" value="{{ $server->duration }}">
                                                        @error('duration')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 mt-5 mx-auto">

                                                <div class="row">
                                                    <div class="col">

                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="main-content-label mg-b-5">
                                                                        {{ trans('admin/package-server.details-ar') }}
                                                                    </div>
                                                                    <div class="col-lg">
                                                                        <textarea class="form-control @error('desc_ar') is-invalid fparsley-error parsley-error @enderror" name="desc_ar" placeholder="{{ trans('admin/package-server.details-ar_place_holder') }}" rows="3">{{ $server->getTranslation('details', 'ar') }}</textarea>
                                                                        @error('desc_ar')
                                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                               <p>{{ $message }}</p>
                                                                            </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="main-content-label mg-b-5">
                                                                        {{ trans('admin/package-server.details-en_place_holder') }}
                                                                    </div>
                                                                    <div class="col-lg">
                                                                        <textarea class="form-control @error('desc_en') is-invalid fparsley-error parsley-error @enderror" name="desc_en" placeholder="{{ trans('admin/package-server.details-en_place_holder') }}" rows="3">{{ $server->getTranslation('details', 'en') }}</textarea>
                                                                        @error('desc_en')
                                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                                 <p>{{ $message }}</p>
                                                                             </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">{{ trans('admin/action.update') }}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>

@endsection
