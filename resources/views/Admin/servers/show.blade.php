@extends('Admin.layouts.master')

@section('title')
    {{ $server->name }}
@endsection

@section('css')
    <!-- Internal Data table css -->
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

    <!--  Right-sidemenu css -->
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/sidebar/sidebar.css')}}" rel="stylesheet">

    <!--  Custom Scroll bar-->
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/mscrollbar/jquery.mCustomScrollbar.css')}}" rel="stylesheet"/>

    <!--  Left-Sidebar css -->
    <link rel="stylesheet" href="{{ URL::asset('assetsAdmin/en/css/sidemenu.css')}}">

    <!--- Style css --->
    <link href="{{ URL::asset('assetsAdmin/en/css/style.css')}}" rel="stylesheet">

    <!--- Dark-mode css --->
    <link href="{{ URL::asset('assetsAdmin/en/css/style-dark.css')}}" rel="stylesheet">

    <!---Skinmodes css-->
    <link href="{{ URL::asset('assetsAdmin/en/css/skin-modes.css')}}" rel="stylesheet" />

    <!--- Animations css-->
    <link href="{{ URL::asset('assetsAdmin/en/css/animate.css')}}" rel="stylesheet">

    <link href="{{ URL::asset('assetsAdmin/en/css/icons.css')}}" rel="stylesheet">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/package-server.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.server.index') }}"><h5 class="text-white-50">{{ trans('admin/package-server.PackageServers') }} /</h5></a> </span>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.server.show', $server) }}"><h5 class="text-white-50">{{ $server->name }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/package-server.PackageServers') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12  col-lg-12 col-xl-4 mx-auto">
                            <div class="card card-body bg-primary ">
                                <div class="card-body">
                                    <h5 class="card-title text-white text-center">{{ $server->name }}</h5>
                                    <h6 class="card-subtitle mb-2 text-white text-center">{{ trans('admin/package-server.details') }}</h6>
                                    <p class="card-text text-center">{!! nl2br($server->details) !!}</p>
                                    <p class="card-text text-white text-center">{{ $server->price }} $ | {{ $server->duration }} {{ trans('admin/package-server.month') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Internal Data tables -->
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.bootstrap4.min.js')}}"></script>

    <!--Internal  Datatable js -->
    <script src="{{ URL::asset('assetsAdmin/shared/js/table-data.js')}}"></script>
@endsection
