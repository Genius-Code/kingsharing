@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/slider.Slider') }}
@endsection


@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/slider.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.slider.index') }}"><h5 class="text-white-50">{{ trans('admin/slider.Slider') }} /</h5></a> </span>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.slider.create') }}"><h5 class="text-white-50">{{ trans('admin/slider.Add New Slider') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/slider.Sliders') }}</h4>
                        <a href="{{ route('admin.slider.create') }}" class="btn btn-warning-gradient">{{ trans('admin/slider.Create New Slider') }}</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table key-buttons text-md-nowrap">
                            <thead>
                            <tr>
                                <th class="border-bottom-0 text-center">{{ trans('admin/slider.Image') }}</th>
                                <th class="border-bottom-0 text-center">{{ trans('admin/slider.Status') }}</th>
                                <th class="border-bottom-0 text-center">{{ trans('admin/action.Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($sliders) && $sliders->count() > 0)
                                @foreach($sliders as $slider)
                                    <tr>
                                        <td class="text-center"><img class="img-thumbnail-round" width="120px" height="120px" src="{{ $slider->image }}"></td>
                                        <td class="text-center"><button class="btn btn-{{ ($slider->status == 'published') ? 'success' : 'danger'}}">{{ $slider->status }}</button></td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <button aria-expanded="false" aria-haspopup="true" class="btn ripple btn-info-gradient"
                                                        data-toggle="dropdown" type="button">{{ trans('admin/action.Choose Action') }}<i class="fas fa-caret-down ml-1"></i></button>
                                                <div class="dropdown-menu tx-13">
                                                    <a class="dropdown-item" href="{{ route('admin.slider.edit', $slider) }}">{{ trans('admin/action.Edit') }}</a>

                                                    <form action="{{ route('admin.slider.destroy', $slider) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="hidden" name="slider_id" value="{{ $slider->id }}">
                                                        <button type="submit" class="dropdown-item">{{ trans('admin/action.Delete') }}</button>
                                                    </form>

                                                </div>
                                            </div>

                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Internal Data tables -->
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.bootstrap4.min.js')}}"></script>

    <!--Internal  Datatable js -->
    <script src="{{ URL::asset('assetsAdmin/shared/js/table-data.js')}}"></script>
@endsection
