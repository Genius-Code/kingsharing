@extends('User.layouts.master')

@section('title')
    Messages | InBox
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/message-client.Dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.message.index') }}"><h5 class="text-white-50">{{ trans('admin/message-clients.Inbox') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->

        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">Inbox</h4>
                        <i class="mdi mdi-dots-horizontal text-gray"></i>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ trans('admin/message-clients.Compose new message') }}</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.message.repaly-to-user') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="message_id" value="{{ $message->id }}">
                                <input type="hidden" name="recipient" value="{{ $message->sender_id }}">
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <label class="col-sm-2">To</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @error('user') is-invalid fparsley-error parsley-error @enderror" name="user_id" value="{{ $message->sender->name }}">
                                            @error('user')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <label class="col-sm-2">{{ trans('admin/message-clients.Subject') }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @error('title') is-invalid fparsley-error parsley-error @enderror" name="title" value="{{ $message->title }}">
                                            @error('title')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row ">
                                        <label class="col-sm-2">{{ trans('admin/message-clients.Message') }}</label>
                                        <div class="col-sm-10">
                                            <textarea rows="10" class="form-control @error('description') is-invalid fparsley-error parsley-error @enderror" name="description">{{ old('description') }}</textarea>
                                            @error('description')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row ">
                                        <label class="col-sm-2">{{ trans('admin/message-clients.Upload') }}</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control-file @error('file') is-invalid fparsley-error parsley-error @enderror" name="file">
                                            @error('file')
                                            <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                <p>{{ $message }}</p>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer d-sm-flex">
                                    <div class="btn-list ml-auto">
                                        <button type="submit" class="btn btn-danger btn-space">{{ trans('admin/action.Send') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
