@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/message-clients.Show Message') }}
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/message-clients.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.message.index') }}"><h5 class="text-white-50">{{ trans('admin/message-clients.Inbox') }} </h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $message->received->name }} <span class="badge badge-primary">{{ trans('admin/message-clients.Inbox') }}</span></h4>
                </div>
                <div class="card-body">
                    <div class="email-media">
                        <div class="mt-0 d-sm-flex">
                            <img class="mr-2 rounded-circle avatar-xl" src="{{ isset($message->sender->userProfile->image) ? $message->sender->userProfile->image : URL::asset('assetsAdmin/shared/img/faces/6.jpg') }}" alt="avatar">
                            <div class="media-body">
                                <div class="float-right d-none d-md-flex fs-15">
                                    <span class="mr-3">{{ $message->created_at->format("d/m/y  H:i A") }}</span>
                                </div>
                                <div class="media-title  font-weight-bold mt-3">{{ $message->sender->name }} <span class="text-muted">( {{ $message->sender->email }} )</span></div>
                                <small class="mr-2 d-md-none">{{ $message->created_at->format("d/m/y  H:i A") }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="eamil-body mt-5">
                        <h6>{{ $message->title }}</h6>
                        <p> {!! $message->description !!}</p>
                        <hr>
                        @if (!is_null($message->file))
                        <div class="email-attch">
                            <div class="float-right">
                                <a href="{{ isset($message->file) ? $message->file : '' }}"><i class="bx bxs-download tx-18" data-toggle="tooltip" title="" data-original-title="Download"></i></a>
                            </div>

                                <p>{{ trans('admin/message-clients.Files') }}</p>
                                <div class="emai-img">
                                    <div class="d-sm-flex">
                                        <div class=" m-2">
                                            <a href="{{ $message->file }}"><img class="wd-150 mb-2" src="{{ isset($message->file) ? $message->file : '' }}" alt="placeholder file"></a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        @endif
                        <hr>
                        @if (isset($data) && $data->count() > 0)
                            @foreach($data as $item)
                        <div class="eamil-body mt-5">
                            <h5>Replay From Admin</h5>

                                    <h6>Title : {{ $item->message->title }}</h6>
                                    <p>Replay : <br/>{!! nl2br($item->replay) !!}</p>
                                @endforeach
                            @endif

                        </div>
                        <hr>
                        @if (isset($data) && $data->count() > 0)
                            @foreach($data as $item)
                        @if (!is_null($item->file))
                            <div class="email-attch">
                                <div class="float-right">
                                    <a href="{{ isset($item->file) ? $item->file : '' }}"><i class="bx bxs-download tx-18" data-toggle="tooltip" title="" data-original-title="Download"></i></a>
                                    {{ $item->created_at }}
                                </div>

                                <p>{{ trans('admin/message-clients.Files Send By Admin') }}</p>
                                <div class="emai-img">
                                    <div class="d-sm-flex">
                                        <div class=" m-2">
                                            <a href="{{ $item->file }}"><img class="wd-150 mb-2" src="{{ isset($item->file) ? $item->file : '' }}" alt="placeholder file"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <hr/>
                    </div>
                    @endforeach
                    @endif
                </div>
                <div class="card-footer">
                    <a class="btn btn-primary mt-1 mb-1" href="{{ route('admin.message.repaly-to-user-page', $message) }}"><i class="fa fa-reply"></i> {{ trans('admin/action.Reply') }}</a>
                </div>
            </div>
        </div>
    </div>

@endsection

