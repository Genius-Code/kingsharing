@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/contact.Show Mail Contact') }}
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/contact.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.mail.index') }}"><h5 class="text-white-50">{{ trans('admin/contact.Mail-Contact') }} </h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $contact->name }} <span class="badge badge-primary">{{ trans('admin/contact.Mail') }}</span></h4>
                </div>
                <div class="card-body">
                    <div class="email-media">
                        <div class="mt-0 d-sm-flex">
                            <img class="mr-2 rounded-circle avatar-xl" src="{{ URL::asset('assetsAdmin/shared/img/faces/6.jpg') }}" alt="avatar">
                            <div class="media-body">
                                <div class="float-right d-none d-md-flex fs-15">
                                    <span class="mr-3">{{ $contact->created_at->format("d/m/y  H:i A") }}</span>
                                </div>
                                <div class="media-title  font-weight-bold mt-3">{{ $contact->name }} <span class="text-muted">( {{ $contact->email }} )</span></div>
                                <small class="mr-2 d-md-none">{{ $contact->created_at->format("d/m/y  H:i A") }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="eamil-body mt-5">
                        <h6>{{ $contact->subject }}</h6>
                        <p> {!! nl2br($contact->body) !!}</p>
                    </div>

                </div>
                <div class="card-footer">
                    <a class="btn btn-primary mt-1 mb-1" href="{{ route('admin.mail.index') }}"><i class="fa fa-reply"></i> {{ trans('admin/contact.Back') }}</a>
                </div>
            </div>
        </div>
    </div>

@endsection
