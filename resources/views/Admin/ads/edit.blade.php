@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/ads.update-ad') }}
@endsection

@section('css')
    <!--- Internal Fileupload css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <!--- Internal Fancy uploader css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
    <!--- Internal Sumoselect css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">
    <!--Internal  Quill css -->
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.bubble.css') }}" rel="stylesheet">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/ads.dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.ads.index') }}"><h5 class="text-white-50">{{ trans('admin/ads.ads') }}</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50"> / {{ trans('admin/ads.update-ad') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/ads.ads') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="main-content-label mg-b-5">
                                        {{ trans('admin/ads.add-new-ad') }}
                                    </div>
                                    <form method="post" action="{{ route('admin.ads.update') }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="ads_id" value="{{ $ads->id }}">
                                        <div class="pd-30 pd-sm-40 bg-gray-200">
                                            <div class="col-lg-12 col-md-12 mt-5 mx-auto">

                                                <div class="row">
                                                    <div class="col">

                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="main-content-label mg-b-5">
                                                                        {{ trans('admin/ads.description-ar') }}
                                                                    </div>
                                                                    <div class="col-lg">
                                                                        <textarea class="form-control @error('desc_ar') is-invalid fparsley-error parsley-error @enderror" name="desc_ar" placeholder="{{ trans('admin/ads.description-ar_ph') }}" rows="3">{{ $ads->getTranslation('description', 'ar') }}</textarea>
                                                                        @error('desc_ar')
                                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                               <p>{{ $message }}</p>
                                                                            </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="main-content-label mg-b-5">
                                                                        {{ trans('admin/ads.description-en') }}
                                                                    </div>
                                                                    <div class="col-lg">
                                                                        <textarea class="form-control @error('desc_en') is-invalid fparsley-error parsley-error @enderror" name="desc_en" placeholder="{{ trans('admin/ads.description-en_ph') }}" rows="3">{{ $ads->getTranslation('description', 'en') }}</textarea>
                                                                        @error('desc_en')
                                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                                 <p>{{ $message }}</p>
                                                                             </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-8 mg-t-5 mg-md-t-0 mx-auto">
                                                <label class="form-label">{{ trans('admin/ads.image') }}<span title="required" class="tx-danger">*</span></label>
                                                <input type="file" class="dropify @error('main_image') is-invalid fparsley-error parsley-error @enderror" name="main_image"  data-width="280" data-height="420"/>
                                                @error('main_image')
                                                <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                    <p>{{ $message }}</p>
                                                </span>
                                                @enderror
                                            </div>

                                            <!--div-->
                                            <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12 mt-5">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="main-content-label mg-b-5">
                                                            {{ trans('admin/ads.images-ad') }}
                                                        </div>
                                                        <p class="mg-b-20">{{ trans('admin/ads.image-slider') }}</p>
                                                        <div class="row row-sm">
                                                            <div class="col-sm-7 col-md-6 col-lg-4">
                                                                <div class="custom-file">
                                                                    <input class="custom-file-input @error('images[]') is-invalid fparsley-error parsley-error @enderror" id="customFile" type="file" name="images[]" multiple> <label class="custom-file-label" for="customFile">{{ trans('admin/ads.choose-file') }}</label>
                                                                    @error('images[]')
                                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                                       <p>{{ $message }}</p>
                                                                     </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/div-->

                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">{{ trans('admin/action.Update') }}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
    <!-- Internal Form-editor js -->
    <script src="{{ URL::asset('assetsAdmin/shared/js/form-editor.js')}}"></script>
    <!--Internal quill js -->
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/quill/quill.min.js')}}"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection
