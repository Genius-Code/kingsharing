@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/ads.ads') }}
@endsection


@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/ads.dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.ads.index') }}"><h5 class="text-white-50">{{ trans('admin/ads.ads') }} /</h5></a> </span>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.ads.create') }}"><h5 class="text-white-50">{{ trans('admin/ads.add-new-ad') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/ads.ads') }}</h4>
                        <a href="{{ route('admin.ads.create') }}" class="btn btn-warning-gradient">{{ trans('admin/ads.create-new-ad') }}</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table key-buttons text-md-nowrap">
                            <thead>
                            <tr>
                                <th class="border-bottom-0 text-center">{{ trans('admin/ads.image') }}</th>
                                <th class="border-bottom-0 text-center">{{ trans('admin/ads.description') }}</th>
                                <th class="border-bottom-0 text-center">{{ trans('admin/action.Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($ads) && $ads->count() > 0)
                                @foreach($ads as $ad)
                                    <tr>
                                        <td class="text-center"><img class="img-thumbnail-round" width="120px" height="120px" src="{{ $ad->main_image }}"></td>
                                        <td class="text-center">{{ $ad->description }}</td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <button aria-expanded="false" aria-haspopup="true" class="btn ripple btn-info-gradient"
                                                        data-toggle="dropdown" type="button">Choose Action<i class="fas fa-caret-down ml-1"></i></button>
                                                <div class="dropdown-menu tx-13">
                                                    <a class="dropdown-item" href="{{ route('admin.ads.edit', $ad) }}">{{ trans('admin/action.Edit') }}</a>

                                                    <form action="{{ route('admin.ads.destroy', $ad) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="hidden" name="ads_id" value="{{ $ad->id }}">
                                                        <button type="submit" class="dropdown-item">{{ trans('admin/action.Delete') }}</button>
                                                    </form>

                                                </div>
                                            </div>

                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Internal Data tables -->
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jquery.dataTables.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('assetsAdmin/shared/plugins/datatable/js/responsive.bootstrap4.min.js')}}"></script>

    <!--Internal  Datatable js -->
    <script src="{{ URL::asset('assetsAdmin/shared/js/table-data.js')}}"></script>
@endsection
