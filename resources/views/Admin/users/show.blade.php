@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/user.Show') }}  {{ trans('admin/user.Profile') }} {{ $user->name }}
@endsection

@section('content')
    <!-- container -->
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/user.Dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.user.index') }}"><h5 class="text-white-50">{{ trans('admin/user.Users') }} /</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50">{{ trans('admin/user.Show') }}  {{ trans('admin/user.Profile') }} {{ $user->name }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->

        <!-- row -->
        <div class="row row-sm">
            <!-- Col -->
            <div class="col-lg-8 mx-auto">
                <div class="card mg-b-20">
                    <div class="card-body">
                        <div class="pl-0">
                            <div class="main-profile-overview">
                                <div class="main-img-user profile-user">
                                    <img alt="" src="{{ $user->image }}"><a class="fas fa-camera profile-edit" href="JavaScript:void(0);"></a>
                                </div>
                                <div class="d-flex justify-content-between mg-b-20">
                                    <div>
                                        <h5 class="main-profile-name">{{ $user->name }}</h5>
                                        <p class="main-profile-name-text font-weight-bold">{{ $user->roles->role->name }}</p>
                                    </div>
                                </div>
                                <hr class="mg-y-30">
                                <label class="main-content-label tx-13 mg-b-20">{{ trans('admin/user.Social') }}</label>
                                <div class="main-profile-social-list">

                                    <div class="media">
                                        <div class="media-icon bg-success-transparent text-primary">
                                            <i class="icon ion-logo-facebook"></i>
                                        </div>
                                        <div class="media-body">
                                            <span>{{ trans('admin/user.Facebook') }}</span> <a href="">{{ isset($user->facebook_url) ? $user->facebook_url : trans('admin/user.No Data Found !') }}</a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-icon bg-info-transparent text-success">
                                            <i class="icon ion-logo-whatsapp"></i>
                                        </div>
                                        <div class="media-body">
                                            <span>{{ trans('admin/user.whatsapp-Number') }}</span> <a href="">{{ isset($user->whatsapp_number) ? $user->whatsapp_number : trans('admin/user.No Data Found !') }}</a>
                                        </div>
                                    </div>

                                </div>
                                <hr class="mg-y-30">
                                <div class="card mg-b-20">
                                    <div class="card-body">
                                        <div class="main-content-label tx-13 mg-b-25">
                                            {{ trans('admin/user.Contact') }}
                                        </div>
                                        <div class="main-profile-contact-list">
                                            <div class="media">
                                                <div class="media-icon bg-primary-transparent text-primary">
                                                    <i class="icon ion-md-phone-portrait"></i>
                                                </div>
                                                <div class="media-body">
                                                    <span> {{ trans('admin/user.Mobile') }}</span>
                                                    <div>
                                                        {{ $user->phone }}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="media">
                                                <div class="media-icon bg-info-transparent text-info">
                                                    <i class="icon ion-md-locate"></i>
                                                </div>
                                                <div class="media-body">
                                                    <span> {{ trans('admin/user.Country') }}</span>
                                                    <div>
                                                        {{ isset($user->country) ? $user->country : trans('admin/user.No Data Found !') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- main-profile-contact-list -->
                                    </div>
                                </div>
                            </div><!-- main-profile-overview -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
