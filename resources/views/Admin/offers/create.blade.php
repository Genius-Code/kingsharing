@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/offers.Create New Offer') }}
@endsection

@section('css')
    <!--- Internal Fileupload css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <!--- Internal Fancy uploader css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
    <!--- Internal Sumoselect css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">

@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/offers.dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.offer.index') }}"><h5 class="text-white-50">{{ trans('admin/offers.offers') }}</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50"> / {{ trans('admin/offers.Create New Offer') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/offers.offers') }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="main-content-label mg-b-5">
                                        {{ trans('admin/offers.add-new-offer') }}
                                    </div>
                                    <form method="post" action="{{ route('admin.offer.store') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="pd-30 pd-sm-40 bg-gray-200">

                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/offers.new-price') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('price') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/offers.price_ph') }}" name="price" type="number" value="{{ old('price') }}" required>
                                                        @error('price')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-6">
                                                    <div class="col-md-4">
                                                        <label class="form-label mg-b-0">{{ trans('admin/offers.duration') }}</label>
                                                    </div>
                                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                        <input class="form-control @error('duration') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/offers.duration_ph') }}" name="duration" type="number" value="{{ old('duration') }}">
                                                        @error('duration')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-sm mg-b-20 mt-5">
                                                <div class="col-lg-8 mx-auto">
                                                    <p class="mg-b-10">{{ trans('admin/offers.select-package-server') }}</p>
                                                    <select class="form-control select2-no-search @error('package_server') is-invalid fparsley-error parsley-error @enderror" name="package_server" required>
                                                        <option label="{{ trans('admin/offers.choose') }}">
                                                        </option>
                                                        @if(isset($servers) && count($servers) > 0)
                                                            @foreach($servers as $server)
                                                                <option value="{{ $server->id }}">
                                                                    {{ $server->name }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    @error('package_server')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div><!-- col-4 -->
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col">

                                                    <div class="col-lg-8 mx-auto">
                                                        <p class="mg-b-10">{{ trans('admin/offers.status') }}</p>
                                                        <select class="form-control select2-no-search @error('status') is-invalid fparsley-error parsley-error @enderror" name="status">
                                                            <option label="{{ trans('admin/offers.choose') }}">
                                                            </option>
                                                            <option value="approve">Approve</option>
                                                            <option value="not_approve">Not Approve</option>
                                                        </select>
                                                        @error('status')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                        @enderror
                                                    </div><!-- col-4 -->
                                                </div>


                                            </div>
                                            <div class="col-lg-12 col-md-12 mt-5 mx-auto">

                                                <div class="col-md-8 mg-t-5 mg-md-t-0 mx-auto">
                                                    <label class="form-label">{{ trans('admin/offers.image') }} <span title="required" class="tx-danger">*</span></label>
                                                    <input type="file" class="dropify @error('image') is-invalid fparsley-error parsley-error @enderror" name="image"  data-width="280" data-height="420"/>
                                                    @error('image')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">{{trans('admin/action.Create')}}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
    <script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>

@endsection
