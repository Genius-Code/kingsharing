<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Description" content="Bootstrap Responsive Admin Web Dashboard HTML5 Template">
    <meta name="Author" content="Spruko Technologies Private Limited">
    <meta name="Keywords" content="admin,admin dashboard,admin dashboard template,admin panel template,admin template,admin theme,bootstrap 4 admin template,bootstrap 4 dashboard,bootstrap admin,bootstrap admin dashboard,bootstrap admin panel,bootstrap admin template,bootstrap admin theme,bootstrap dashboard,bootstrap form template,bootstrap panel,bootstrap ui kit,dashboard bootstrap 4,dashboard design,dashboard html,dashboard template,dashboard ui kit,envato templates,flat ui,html,html and css templates,html dashboard template,html5,jquery html,premium,premium quality,sidebar bootstrap 4,template admin bootstrap 4"/>

    <!-- Title -->
    <title> {{ trans('admin/login.Admin Dashboard') }} -  {{ trans('admin/login.SignIn') }} </title>

    <!-- Favicon -->
    <link rel="icon" href="{{URL::asset('assetsAdmin/shared/img/brand/favicon.png')}}" type="image/x-icon"/>

    <!-- Icons css -->
    <link href="{{URL::asset('assetsAdmin/en/css/icons.css')}}" rel="stylesheet">

    <!--  Right-sidemenu css -->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/sidebar/sidebar.css')}}" rel="stylesheet">

    <!--  Custom Scroll bar-->
    <link href="{{URL::asset('assetsAdmin/shared/plugins/mscrollbar/jquery.mCustomScrollbar.css')}}" rel="stylesheet"/>

    <!--  Left-Sidebar css -->
    <link rel="stylesheet" href="{{URL::asset('assetsAdmin/en/css/sidemenu.css')}}">

    <!--- Style css --->
    <link href="{{URL::asset('assetsAdmin/en/css/style.css')}}" rel="stylesheet">

    <!--- Dark-mode css --->
    <link href="{{URL::asset('assetsAdmin/en/css/style-dark.css')}}" rel="stylesheet">

    <!---Skinmodes css-->
    <link href="{{URL::asset('assetsAdmin/en/css/skin-modes.css')}}" rel="stylesheet" />

    <!--- Animations css-->
    <link href="{{URL::asset('assetsAdmin/en/css/animate.css')}}" rel="stylesheet">

</head>
<body class="main-body dark-theme">

<!-- Loader -->
<div id="global-loader">
    <img src="{{URL::asset('assetsAdmin/shared/img/loader.svg')}}" class="loader-img" alt="Loader">
</div>
<!-- /Loader -->

<!-- Page -->
<div class="page">

    <div class="container-fluid">
        <div class="row no-gutter">
            <!-- The image half -->
            <div class="col-md-6 col-lg-6 col-xl-7 d-none d-md-flex bg-primary-transparent">
                <div class="row wd-100p mx-auto text-center">
                    <div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
                        <img src="{{URL::asset('assetsAdmin/shared/img/media/login.png')}}" class="my-auto ht-xl-80p wd-md-100p wd-xl-80p mx-auto" alt="logo">
                    </div>
                </div>
            </div>
            <!-- The content half -->
            <div class="col-md-6 col-lg-6 col-xl-5">
                <div class="login d-flex align-items-center py-2">
                    <!-- Demo content-->
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
                                <div class="card-sigin">
                                    <div class="mb-5 d-flex"> <a href="#"><img src="{{URL::asset('assetsAdmin/shared/img/brand/favicon.png')}}" class="sign-favicon ht-40" alt="logo"></a><h1 class="main-logo1 ml-1 mr-0 my-auto tx-28">Va<span>le</span>x</h1></div>
                                    <div class="card-sigin">
                                        <div class="main-signup-header">
                                            <h2>{{ trans('admin/login.Welcome back!') }}</h2>
                                            <h5 class="font-weight-semibold mb-4">{{ trans('admin/login.Please sign in to continue.') }}</h5>
                                            @include('partials.error')
                                            <form action="{{ route('admin.login') }}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label>{{ trans('admin/login.Email') }}</label>
                                                    <input class="form-control @error('email') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('admin/login.email_ph') }}" name="email" value="{{old('email')}}" type="email">
                                                    @error('email')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>{{ trans('admin/login.Password') }}</label>
                                                    <input class="form-control @error('password') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('admin/login.password_ph') }}" name="password"  type="password">
                                                    @error('password')
                                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                          <p>{{ $message }}</p>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <button class="btn btn-main-primary btn-block">{{ trans('admin/login.SignIn') }}</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End -->
                </div>
            </div><!-- End -->
        </div>
    </div>

</div>
<!-- End Page -->

<!-- JQuery min js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Bundle js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Ionicons js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/ionicons/ionicons.js')}}"></script>

<!-- Moment js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/moment/moment.js')}}"></script>

<!-- eva-icons js -->
<script src="{{URL::asset('assetsAdmin/shared/js/eva-icons.min.js')}}"></script>

<!-- Rating js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/rating/jquery.rating-stars.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/rating/jquery.barrating.js')}}"></script>

<!-- custom js -->
<script src="{{URL::asset('assetsAdmin/shared/js/custom.js')}}"></script>

</body>
</html>
