@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/profile.Update Profile') }} | {{ trans('admin/profile.Admin Dashboard') }}
@endsection

@section('content')
    <!-- container -->
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/profile.Dashboard') }} / </a></h4>
                    <span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50">{{ trans('admin/profile.Update Profile') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->

        <!-- row -->
        <div class="row row-sm">
            <!-- Col -->
            <div class="col-lg-4">
                <div class="card mg-b-20">
                    <div class="card-body">
                        <div class="pl-0">
                            <div class="main-profile-overview">
                                <div class="main-img-user profile-user">
                                    <img alt="" src="{{ isset($data->image) ? $data->image : '' }}"><a class="fas fa-camera profile-edit" href="JavaScript:void(0);"></a>
                                </div>
                                <div class="d-flex justify-content-between mg-b-20">
                                    <div>
                                        <h5 class="main-profile-name">{{ auth()->user()->name }}</h5>
                                        <p class="main-profile-name-text font-weight-bold">{{ ucfirst(auth()->user()->roles->role->name) }}</p>
                                    </div>
                                </div>
                                <hr class="mg-y-30">
                                <label class="main-content-label tx-13 mg-b-20">{{ trans('admin/profile.Social') }}</label>
                                <div class="main-profile-social-list">

                                    <div class="media">
                                        <div class="media-icon bg-success-transparent text-primary">
                                            <i class="icon ion-logo-facebook"></i>
                                        </div>
                                        <div class="media-body">
                                            <span>{{ trans('admin/profile.Facebook') }}</span> <a href="">{{ isset($data->facebook_url) ? $data->facebook_url : trans('admin/profile.no-data-found') }}</a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-icon bg-info-transparent text-success">
                                            <i class="icon ion-logo-whatsapp"></i>
                                        </div>
                                        <div class="media-body">
                                            <span>{{ trans('admin/profile.whatsapp-Number') }}</span> <a href="">{{ isset($data->whatsapp_number) ? $data->whatsapp_number : trans('admin/profile.no-data-found') }}</a>
                                        </div>
                                    </div>

                                </div>
                                <hr class="mg-y-30">
                                <div class="card mg-b-20">
                                    <div class="card-body">
                                        <div class="main-content-label tx-13 mg-b-25">
                                            {{ trans('admin/profile.Conatct') }}
                                        </div>
                                        <div class="main-profile-contact-list">
                                            <div class="media">
                                                <div class="media-icon bg-primary-transparent text-primary">
                                                    <i class="icon ion-md-phone-portrait"></i>
                                                </div>
                                                <div class="media-body">
                                                    <span>{{ trans('admin/profile.Mobile') }}</span>
                                                    <div>
                                                        {{ $data->userProfile->phone }}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="media">
                                                <div class="media-icon bg-info-transparent text-info">
                                                    <i class="icon ion-md-locate"></i>
                                                </div>
                                                <div class="media-body">
                                                    <span>{{ trans('admin/profile.Country') }}</span>
                                                    <div>
                                                        {{ isset($data->country) ? $data->country : trans('admin/profile.no-data-found') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- main-profile-contact-list -->
                                    </div>
                                </div>
                            </div><!-- main-profile-overview -->
                        </div>
                    </div>
                </div>

            </div>

            <!-- Col -->
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-4 main-content-label">{{ trans('admin/profile.Personal Information') }}</div>
                        <form class="form-horizontal" method="post" action="{{ route('admin.profile.update') }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="profile_id" value="{{ $data->id }}">
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.username') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('name') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('admin/profile.username') }}" name="name" value="{{ old('name', $data->userProfile->name) }}" required>
                                        @error('name')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="mb-4 main-content-label">{{ trans('admin/profile.contact-info') }}</div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.email') }}<i>({{ trans('admin/profile.required') }})</i></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('email') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('admin/profile.email_ph') }}" name="email" value="{{ old('email', $data->userProfile->email) }}" required>
                                        @error('email')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.phone') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('phone') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('admin/profile.phone_ph') }}" name="phone" value="{{ old('phone', $data->userProfile->phone) }}" required>
                                        @error('phone')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.country') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea class="form-control @error('country') is-invalid fparsley-error parsley-error @enderror" name="country" rows="2"  placeholder="{{ trans('admin/profile.country_ph') }}">{{ old('country', $data->country) }}</textarea>
                                        @error('country')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-4 main-content-label">{{ trans('admin/profile.Social Info') }}</div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.Facebook') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('facebook') is-invalid fparsley-error parsley-error @enderror"  name="facebook" placeholder="{{ trans('admin/profile.facebook_ph') }}" value="{{ old('facebook', $data->facebook_url) }}">
                                        @error('facebook')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.whatsapp-Number') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control @error('whatsapp') is-invalid fparsley-error parsley-error @enderror"  placeholder="{{ trans('admin/profile.whatsapp-Number_ph') }}" name="whatsapp" value="{{ old('whatsapp', $data->whatsapp_number) }}">
                                        @error('whatsapp')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="mb-4 main-content-label">{{ trans('admin/profile.Upload Image Profile') }}</div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">{{ trans('admin/profile.Upload Photo') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" name="image"  class="form-control @error('image') is-invalid fparsley-error parsley-error @enderror"/>
                                        @error('image')
                                        <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                            <p>{{ $message }}</p>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{ trans('admin/profile.Update') }}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /Col -->
        </div>
        <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
@endsection
