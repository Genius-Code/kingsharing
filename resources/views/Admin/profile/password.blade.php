@extends('Admin.layouts.master')

@section('title')
    {{ trans('admin/user.Change Password') }} | {{ trans('admin/user.Kingsharing') }}
@endsection

@section('content')
    <div class="container-fluid">

        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
            <div class="my-auto">
                <div class="d-flex">
                    <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">{{ trans('admin/user.Dashboard') }} / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50">{{ trans('admin/user.Change Password') }}</h5></a> </span>
                </div>
            </div>
            <div class="d-flex my-xl-auto right-content">
                <div class="mb-3 mb-xl-0">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb -->

        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{ trans('admin/user.Change Password') }}</h4>
                        <i class="mdi mdi-dots-horizontal text-gray"></i>
                    </div>
                </div>
                <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="post" action="{{ route('admin.profile.changePassword') }}">
                                        @csrf
                                        @method('PUT')
                                        <div class="pd-30 pd-sm-40 bg-gray-200">
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/user.Old Password') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('old_password') is-invalid fparsley-error parsley-error @enderror" name="old_password" type="password" placeholder="{{ trans('admin/user.Old Password_ph') }}" required>
                                                    @error('old_password')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                    <p>{{ $message }}</p>
                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row row-xs align-items-center mg-b-20">
                                                <div class="col-md-4">
                                                    <label class="form-label mg-b-0">{{ trans('admin/user.New Password') }}</label>
                                                </div>
                                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                    <input class="form-control @error('password') is-invalid fparsley-error parsley-error @enderror" placeholder="{{ trans('admin/user.New Password_ph') }}" name="password" type="password" required>
                                                    @error('password')
                                                    <span class="invalid-feedback text-white font-weight-bold text-capitalize mt-2" role="alert">
                                                    <p>{{ $message }}</p>
                                                </span>
                                                    @enderror
                                                </div>
                                            </div>



                                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">{{ trans('admin/user.Change') }}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>
            </div>
        </div>
    </div>
@endsection
