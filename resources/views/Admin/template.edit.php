@extends('Admin.layouts.master')

@section('title')
Assets
@endsection

@section('css')
<!---Internal Fileupload css-->
<link href="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<!---Internal Fancy uploader css-->
<link href="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<!--Internal Sumoselect css-->
<link rel="stylesheet" href="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/sumoselect.css')}}">

@endsection

@section('content')
<div class="container-fluid">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto"><a href="{{ route('admin.dashboard') }}">Dashboard / </a></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ route('admin.assets.index') }}"><h5 class="text-white-50">Settings</h5></a> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"><a href="{{ URL::current() }}"><h5 class="text-white-50"> / Create New Asset</h5></a> </span>
            </div>
        </div>
        <div class="d-flex my-xl-auto right-content">
            <div class="mb-3 mb-xl-0">
                <div class="btn-group dropdown">
                    <button type="button" class="btn btn-primary">{{ date('d,m,Y') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb -->
    <div class="col-xl-12">
        <div class="card mg-b-20">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Bordered Table</h4>
                    <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
                <p class="tx-12 tx-gray-500 mb-2">Example of Valex Bordered Table.. <a href="">Learn more</a></p>
            </div>
            <div class="card-body">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                    Left Label Alignment
                                </div>
                                <p class="mg-b-20">It is Very Easy to Customize and it uses in your website apllication.</p>
                                <form>
                                    <div class="pd-30 pd-sm-40 bg-gray-200">
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">Email</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your Email" name="email" type="email">
                                            </div>
                                        </div>
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">Phone</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your Phone" name="phone" type="text">
                                            </div>
                                        </div>
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">Address</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your address" name="address" type="email">
                                            </div>
                                        </div>
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">FaceBook Page Url</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your facebook page url" name="facebook" type="url">
                                            </div>
                                        </div>
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">Telegram Page Url</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your telegram page url" name="telegram" type="url">
                                            </div>
                                        </div>
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">Youtube Page Url</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your youtube page url" name="youtube" type="url">
                                            </div>
                                        </div>
                                        <div class="row row-xs align-items-center mg-b-20">
                                            <div class="col-md-4">
                                                <label class="form-label mg-b-0">Instagram Page Url</label>
                                            </div>
                                            <div class="col-md-8 mg-t-5 mg-md-t-0">
                                                <input class="form-control" placeholder="Enter your instagram page url" name="instagram" type="url">
                                            </div>
                                        </div>

                                        <div class="col-md-8 mg-t-5 mg-md-t-0 mx-auto">
                                            <label class="form-label">Logo <span title="required" class="tx-danger">*</span></label>
                                            <input type="file" class="dropify" name="gallery" required data-width="280" data-height="420"/>
                                        </div>

                                        <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">Create</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /row -->

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('assetsAdmin/shared/js/form-validation.js')}}"></script>
<!--Internal  Datepicker js -->
<script src="{{URL::asset('assetsAdmin/shared/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
<!--Internal Fileuploads js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/fileupload.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/fileuploads/js/file-upload.js')}}"></script>
<!--Internal Fancy uploader js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/plugins/fancyuploder/fancy-uploader.js')}}"></script>
<!--Internal  Form-elements js-->
<script src="{{URL::asset('assetsAdmin/shared/js/advanced-form-elements.js')}}"></script>
<script src="{{URL::asset('assetsAdmin/shared/js/select2.js')}}"></script>
<!--Internal Sumoselect js-->
<script src="{{URL::asset('assetsAdmin/shared/plugins/sumoselect/jquery.sumoselect.js')}}"></script>
@endsection
