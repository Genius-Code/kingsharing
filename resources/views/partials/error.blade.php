@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
        @foreach ($errors->all() as $error)
            <span class="alert-inner--icon"><i class="fe fe-slash"></i></span>
            <span class="alert-inner--text"><strong>{{ trans('admin/login.danger') }}</strong> {{ $error }}</span>
            <br>
        @endforeach
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" class="text-black">×</span>
        </button>
    </div>
@endif
