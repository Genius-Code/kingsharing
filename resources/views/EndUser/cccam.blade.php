@extends('EndUser.layouts.master')

@section('title')
    Kingsharing
@endsection

@section('content')

    <!-- Start section bann -->
    <section class="bann">
        @isset($sliders)
            @forelse($sliders as $slider)
                <div class="bannerheader1">
                    <img
                        src="{{ $slider->image }}"
                        class="imgBanner1"
                        alt=""
                    />
                </div>
            @empty
            @endforelse
        @endisset
    </section>
    <!-- End section bann -->
    <!-- Start content pro -->
    <section class="ContentPro">
        <!-- Start container -->
        <div class="container">
            <!-- start all content  -->
            <div class="allContnet">
                <!-- start conentOne -->
                <div class="contentone">
                    <div class="cont">
                        <h1>{{ trans('homePage.channel-live') }}</h1>
                        <p>
                            {{ trans('homePage.details-live') }}
                        </p>
                    </div>
                    <i class="fa-solid fa-satellite-dish fa-4x"></i>
                </div>
                <!-- End conentOne -->
                <!-- start conentOne -->
                <div class="contentone">
                    <div class="cont">
                        <h1>{{ trans('homePage.vod') }}</h1>
                        <p>
                            {{ trans('homePage.details-vod') }}
                        </p>
                    </div>
                    <i class="fa-solid fa-photo-film fa-4x"></i>
                </div>
                <!-- End conentOne -->
                <!-- start conentOne -->
                <div class="contentone">
                    <div class="cont">
                        <h1>{{ trans('homePage.packages') }}</h1>
                        <p>
                            {{ trans('homePage.details-pack') }}
                        </p>
                    </div>
                    <i class="fa-solid fa-server fa-4x"></i>
                </div>
                <!-- End conentOne -->
            </div>
            <!-- End all content  -->
        </div>
        <!-- End container -->
    </section>
    <!-- End content pro -->

    <!-- Start Button Scroll To Top -->
    <div class="scrollTop">
        <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
    </div>
    <!-- End Button Scroll To Top -->

    <!-- Start social icons -->
    <div class="socialIcons">
        <div class="allIcons">
            <div class="icon1">
                <a href="https://wa.me/+201001245613" target="_blank"> <i class="fa-brands fa-whatsapp fa-2x"></i></a>
                <h4>whatsapp</h4>
            </div>
            <div class="icon1">
                <a href="https://te.me/elking_server"> <i class="fa-brands fa-telegram fa-2x"></i></a>
                <h4>telegram</h4>
            </div>
            <div class="icon1">
                <a href="#"> <i class="fa-solid fa-phone fa-2x"></i></a>
                <h4>01555005687</h4>
            </div>
        </div>
        <div class="icongear1">
            <i class="fa iconGear fa-gear"></i>
        </div>
    </div>
    <!-- End social icons -->

    <!-- Start sextion Banner -->
    <section class="banner">
        <!-- Start container -->
        <div class="container">
            <!-- Start all banner -->
            <div class="allBanner">
                @isset($banners)
                    @forelse($banners as $banner)
                        <div class="imgbanner">
                            <div class="overlay1"></div>
                            <img src="{{ $banner->image }}" alt="" />
                        </div>
                        <div class="content">
                            <h1>{{ $banner->title }}</h1>
                            <p>
                                {!! $banner->description !!}
                            </p>
                            <a href="{{ $banner->url }}" target="_blank"><button class="btn">{{ trans('homePage.See More') }}</button></a>
                        </div>
                    @empty
                    @endforelse
                @endisset

            </div>
            <!-- End all banner -->
        </div>
        <!-- End container -->
    </section>
    <!-- End sextion Banner -->
    <!-- Start section icons -->
    <section id="icons">
        <!-- Satrt container -->
        <div class="container">
            <!-- start all icons -->
            <div class="row">
                <div class="col">
                    <i class="fa-brands fa-paypal fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.easy-to-pay') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa-solid fa-user-secret fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.secure') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa-solid fa-life-ring fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.support') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa-solid fa-gauge-high fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.stable') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa fa-tv fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">4K/ FHD/ SD</span></p>
                </div>

            </div>
            <!-- End all icons -->
        </div>
        <!-- End container -->
    </section>
    <!-- End section icons -->
    <!--Start section price -->
    <section class="price" id="price">
        <!-- Start container -->
        <div class="container">
            <div class="TitleSection">
                <h1>{{ trans('homePage.price-list') }}</h1>
                <p>
                    {{ trans('homePage.price-list-detail') }} <br/>
                    {{ trans('homePage.price-list-detail2') }}
                </p>
            </div>
            <!-- Start all price -->
            <div class="allPrice">
            {{--                <!-- Start card price -->--}}
            {{--                <div class="cardPrice_content">--}}
            {{--                    <div class="contentPrice">--}}
            {{--                        <h1>Choose package</h1>--}}
            {{--                        <p>Gold package 3 months</p>--}}
            {{--                        <div class="iconSat">--}}
            {{--                            <i class="fa fa-line-chart"></i>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}

            {{--                    <ul class="card__list">--}}
            {{--                        <li class="card__list-item">--}}
            {{--                            <i class="fa fa-check"></i>--}}
            {{--                            <p class="card_Text">3 user request</p>--}}
            {{--                        </li>--}}
            {{--                        <li class="card__list-item">--}}
            {{--                            <i class="fa fa-check"></i>--}}
            {{--                            <p class="card_Text">10 downloads por day</p>--}}
            {{--                        </li>--}}
            {{--                        <li class="card__list-item">--}}
            {{--                            <i class="fa fa-check"></i>--}}
            {{--                            <p class="card_Text">Daily content updates</p>--}}
            {{--                        </li>--}}
            {{--                        <li class="card__list-item">--}}
            {{--                            <i class="fa fa-close"></i>--}}
            {{--                            <p class="card_Text">Fully editable files</p>--}}
            {{--                        </li>--}}
            {{--                        <li class="card__list-item">--}}
            {{--                            <i class="fa fa-close"></i>--}}
            {{--                            <p class="card_Text">Fully editable files</p>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}

            {{--                    <div class="btn__price">--}}
            {{--                        <button class="btn">SUBSCRIB NOW</button>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <!-- End card price -->--}}

            <!-- Start card price -->
                @isset($servers)
                    @forelse($servers as $server)
                        <div class="cardPrice_content">

                            <div class="contentPrice">
                                <h1>{{ $server->name }}</h1>
                                <p> {{ $server->price }} {{trans('homePage.$')}} / {{$server->duration}} {{ trans('homePage.months') }}</p>
                                <div class="iconSat">
                                    <i class="fa fa-tv-alt fa-2x"></i>
                                </div>
                            </div>

                            <ul class="card__list">
                                <li class="card__list-item">
                                    <p class="card_Text text-center">
                                        {!! nl2br($server->details) !!}
                                    </p>
                                </li>
                            </ul>

                            <div class="btn__price">
                                <button class="btn">{{ trans('homePage.SUBSCRIB NOW') }}</button>
                            </div>
                        </div>
                @empty
                @endforelse
            @endisset

            <!-- End card price -->

            </div>
            <!-- End all price -->
        </div>
        <!-- End container -->
    </section>
    <!-- End section price -->




@endsection
