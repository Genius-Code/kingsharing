@extends('EndUser.layouts.master')

@section('title')
    Kingsharing
@endsection

@section('content')

    <!-- Start section bann -->
    <section class="bann">
        @isset($sliders)
            @forelse($sliders as $slider)
                <div class="bannerheader1">
                    <img
                        src="{{ $slider->image }}"
                        class="imgBanner1"
                        alt=""
                    />
                </div>
            @empty
            @endforelse
        @endisset
    </section>
    <!-- End section bann -->
    <!-- Start content pro -->
    <section class="ContentPro">
        <!-- Start container -->
        <div class="container">
            <!-- start all content  -->
            <div class="allContnet">
                <!-- start conentOne -->
                <div class="contentone">
                    <div class="cont">
                        <h1>{{ trans('homePage.channel-live') }}</h1>
                        <p>
                            {{ trans('homePage.details-live') }}
                        </p>
                    </div>
                    <i class="fa-solid fa-satellite-dish fa-4x"></i>
                </div>
                <!-- End conentOne -->
                <!-- start conentOne -->
                <div class="contentone">
                    <div class="cont">
                        <h1>{{ trans('homePage.vod') }}</h1>
                        <p>
                            {{ trans('homePage.details-vod') }}
                        </p>
                    </div>
                    <i class="fa-solid fa-photo-film fa-4x"></i>
                </div>
                <!-- End conentOne -->
                <!-- start conentOne -->
                <div class="contentone">
                    <div class="cont">
                        <h1>{{ trans('homePage.packages') }}</h1>
                        <p>
                            {{ trans('homePage.details-pack') }}
                        </p>
                    </div>
                    <i class="fa-solid fa-server fa-4x"></i>
                </div>
                <!-- End conentOne -->
            </div>
            <!-- End all content  -->
        </div>
        <!-- End container -->
    </section>
    <!-- End content pro -->

    <!-- Start Button Scroll To Top -->
    <div class="scrollTop">
        <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
    </div>
    <!-- End Button Scroll To Top -->

    <!-- Start social icons -->
    <div class="socialIcons">
        <div class="allIcons">
            <div class="icon1">
                <a href="https://wa.me/+201001245613" target="_blank"> <i class="fa-brands fa-whatsapp fa-2x"></i></a>
                <h4>whatsapp</h4>
            </div>
            <div class="icon1">
                <a href="https://te.me/elking_server"> <i class="fa-brands fa-telegram fa-2x"></i></a>
                <h4>telegram</h4>
            </div>
            <div class="icon1">
                <a href="#"> <i class="fa-solid fa-phone fa-2x"></i></a>
                <h4>01555005687</h4>
            </div>
        </div>
        <div class="icongear1">
            <i class="fa iconGear fa-gear"></i>
        </div>
    </div>
    <!-- End social icons -->

    <!-- Start sextion Banner -->
    <section class="banner">
        <!-- Start container -->
        <div class="container">
            <!-- Start all banner -->
            <div class="allBanner">
                @isset($banners)
                    @forelse($banners as $banner)
                        <div class="imgbanner">
                            <div class="overlay1"></div>
                            <img src="{{ $banner->image }}" alt="" />
                        </div>
                        <div class="content">
                            <h1>{{ $banner->title }}</h1>
                            <p>
                                {!! $banner->description !!}
                            </p>
                            <a href="{{ $banner->url }}" target="_blank"><button class="btn">{{ trans('homePage.See More') }}</button></a>
                        </div>
                    @empty
                    @endforelse
                @endisset

            </div>
            <!-- End all banner -->
        </div>
        <!-- End container -->
    </section>
    <!-- End sextion Banner -->
    <!-- Start section icons -->
    <section id="icons">
        <!-- Satrt container -->
        <div class="container">
            <!-- start all icons -->
            <div class="row">
                <div class="col">
                    <i class="fa-brands fa-paypal fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.easy-to-pay') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa-solid fa-user-secret fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.secure') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa-solid fa-life-ring fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.support') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa-solid fa-gauge-high fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">{{ trans('homePage.stable') }}</span></p>
                </div>
                <div class="col">
                    <i class="fa fa-tv fa-4x"></i>
                    <p class="font-weight-bolder"><span class="font-weight-bold">4K/ FHD/ SD</span></p>
                </div>

            </div>
            <!-- End all icons -->
        </div>
        <!-- End container -->
    </section>
    <!-- End section icons -->
    <!--Start section price -->
    <section class="price" id="price">
        <!-- Start container -->
        <div class="container">
            <div class="TitleSection">
                <h1>{{ trans('homePage.price-list') }}</h1>
                <p>
                    {{ trans('homePage.price-list-detail') }} <br/>
                    {{ trans('homePage.price-list-detail2') }}
                </p>
            </div>
            <!-- Start all price -->
            <div class="allPrice">
{{--                <!-- Start card price -->--}}
{{--                <div class="cardPrice_content">--}}
{{--                    <div class="contentPrice">--}}
{{--                        <h1>Choose package</h1>--}}
{{--                        <p>Gold package 3 months</p>--}}
{{--                        <div class="iconSat">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <ul class="card__list">--}}
{{--                        <li class="card__list-item">--}}
{{--                            <i class="fa fa-check"></i>--}}
{{--                            <p class="card_Text">3 user request</p>--}}
{{--                        </li>--}}
{{--                        <li class="card__list-item">--}}
{{--                            <i class="fa fa-check"></i>--}}
{{--                            <p class="card_Text">10 downloads por day</p>--}}
{{--                        </li>--}}
{{--                        <li class="card__list-item">--}}
{{--                            <i class="fa fa-check"></i>--}}
{{--                            <p class="card_Text">Daily content updates</p>--}}
{{--                        </li>--}}
{{--                        <li class="card__list-item">--}}
{{--                            <i class="fa fa-close"></i>--}}
{{--                            <p class="card_Text">Fully editable files</p>--}}
{{--                        </li>--}}
{{--                        <li class="card__list-item">--}}
{{--                            <i class="fa fa-close"></i>--}}
{{--                            <p class="card_Text">Fully editable files</p>--}}
{{--                        </li>--}}
{{--                    </ul>--}}

{{--                    <div class="btn__price">--}}
{{--                        <button class="btn">SUBSCRIB NOW</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- End card price -->--}}

                <!-- Start card price -->
                @isset($packages)
                    @forelse($packages as $package)
                    <div class="cardPrice_content">

                        <div class="contentPrice">
                            <h1>{{ $package->name }}</h1>
                            <p> {{ $package->price }} {{trans('homePage.$')}} / {{$package->duration}} {{ trans('homePage.months') }}</p>
                            <div class="iconSat">
                                <i class="fa fa-tv-alt fa-2x"></i>
                            </div>
                        </div>

                        <ul class="card__list">
                            <li class="card__list-item">
                                <p class="card_Text text-center">
                                    {!! nl2br($package->details) !!}
                                </p>
                            </li>
                        </ul>

                        <div class="btn__price">
                            <button class="btn">{{ trans('homePage.SUBSCRIB NOW') }}</button>
                        </div>
                    </div>
                    @empty
                    @endforelse
                @endisset

                <!-- End card price -->

            </div>
            <!-- End all price -->
        </div>
        <!-- End container -->
    </section>
    <!-- End section price -->

    <!-- Start Section New Server -->
    @if(isset($offers) && $offers->count() > 0)
    <section class="NewServer" id="NewServer">
        <!-- Start container -->
        <div class="container">
            <div class="TitleSection">
                <h1>{{ trans('homePage.New Server Offer') }}</h1>
            </div>
            <!-- Start all server -->
            <div class="allServer">
            @isset($offers)
                @forelse($offers as $offer)
                    <!-- Start server1 -->
                    <div class="server_One">
                        <div class="img">
                            <img src="{{ $offer->image }}" alt="{{ $offer->packageServer->name }}-logo" width="100px" height="100px"/>
                        </div>
                        <!-- Start content -->
                        <div class="content">
                            <h1>{{ $offer->packageServer->name }} - {{ $offer->duration }} {{ trans('homePage.months') }}</h1>
                            <p>
                                {!! nl2br($offer->packageServer->details) !!}
                            </p>
                            <div class="numbers_price">
                                <span>${{ $offer->new_price }}</span>
                                <span>${{ $offer->packageServer->price }}</span>
                            </div>
                            <button class="btn">{{ trans('homePage.BUY NOW') }}</button>
                        </div>
                        <!-- End content -->
                    </div>
                    <!-- End server1 -->
                @empty
                @endforelse
            @endisset
            </div>
            <!-- End all server -->
        </div>
        <!-- End container -->
    </section>
    @endif
    <!-- End Section New Server -->
    <!-- ==================================================================================== -->
    <!-- Strat teachersOne section -->
{{--    <section class="teachersOne">--}}
{{--        <div class="container">--}}
{{--            <div class="TitleSection">--}}
{{--                <h1>New Offers</h1>--}}
{{--                <p>--}}
{{--                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br />--}}
{{--                    Quo saepe delectus ea quas ducimus reiciendis.--}}
{{--                </p>--}}
{{--            </div>--}}

{{--            <div class="imgteachOne">--}}
{{--                <img src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0046.jpg') }}" alt="" />--}}
{{--            </div>--}}

{{--            <div class="swiper teachers-sliderOne">--}}
{{--                <div class="swiper-wrapper">--}}
{{--                    <div class="swiper-slide slideOne">--}}
{{--                        <div class="image1">--}}
{{--                            <img--}}
{{--                                src="https://aroma-iptv.com/wp-content/uploads/2020/02/OSN.jpg"--}}
{{--                                class="image lazyloaded"--}}
{{--                                alt="Partner"--}}
{{--                                data-src="https://aroma-iptv.com/wp-content/uploads/2020/02/OSN.jpg"--}}
{{--                            />--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="swiper-slide slideOne">--}}
{{--                        <div class="image1">--}}
{{--                            <img--}}
{{--                                src="https://aroma-iptv.com/wp-content/uploads/2020/02/bein.jpg"--}}
{{--                                class="image lazyloaded"--}}
{{--                                alt="Partner"--}}
{{--                                data-src="https://aroma-iptv.com/wp-content/uploads/2020/02/bein.jpg"--}}
{{--                            />--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="swiper-slide slideOne">--}}
{{--                        <div class="image1">--}}
{{--                            <img--}}
{{--                                src="https://aroma-iptv.com/wp-content/uploads/2020/02/fox.jpg"--}}
{{--                                class="image lazyloaded"--}}
{{--                                alt="Partner"--}}
{{--                                data-src="https://aroma-iptv.com/wp-content/uploads/2020/02/fox.jpg"--}}
{{--                            />--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="swiper-slide slideOne">--}}
{{--                        <div class="image1">--}}
{{--                            <img--}}
{{--                                src="https://aroma-iptv.com/wp-content/uploads/2020/02/myHD.jpg"--}}
{{--                                class="image lazyloaded"--}}
{{--                                alt="Partner"--}}
{{--                                data-src="https://aroma-iptv.com/wp-content/uploads/2020/02/myHD.jpg"--}}
{{--                            />--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="swiper-slide slideOne">--}}
{{--                        <div class="image1">--}}
{{--                            <img--}}
{{--                                src="https://aroma-iptv.com/wp-content/uploads/2020/02/netflex.jpg"--}}
{{--                                class="image lazyloaded"--}}
{{--                                alt="Partner"--}}
{{--                                data-src="https://aroma-iptv.com/wp-content/uploads/2020/02/netflex.jpg"--}}
{{--                            />--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="swiper-slide slideOne">--}}
{{--                        <div class="image1">--}}
{{--                            <img--}}
{{--                                src="https://aroma-iptv.com/wp-content/uploads/2020/02/myHD.jpg"--}}
{{--                                class="image lazyloaded"--}}
{{--                                alt="Partner"--}}
{{--                                data-src="https://aroma-iptv.com/wp-content/uploads/2020/02/myHD.jpg"--}}
{{--                            />--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- teachersOne section ends -->

    <!-- teachers section starts  -->
    @if (isset($blogs) && $blogs->count() > 0)
        <section class="teachers">
            <div class="container">
                <div class="TitleSection">
                    <h1>{{ trans('homePage.blog-new') }}</h1>
                    <p>
                        {{ trans('homePage.blog-new-detail') }}
                    </p>
                </div>

                <div class="swiper teachers-slider">
                    <div class="swiper-wrapper">
                        @isset($blogs)
                            @forelse($blogs as $blog)
                                <div class="swiper-slide slide">
                                    <div class="image">
                                        <a href="{{ route('user.blog.show', $blog) }}"> <img src="{{ $blog->image }}" alt="{{ $blog->title . '_image' }}" height="250px" width="400px"/></a>
                                    </div>
                                    <div class="content">
                                        <h3><a href="{{ route('user.blog.show', $blog) }}">{{ $blog->title }}</a></h3>
                                        <span><a href="{{ route('user.blog.index') }}">{{ trans('homePage.See-More') }}</a></span>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        @endisset
                    </div>
                </div>
            </div>
        </section>
        <!-- teachers section ends -->
    @endif


    <!-- ======================================================================================= -->
    <!-- start section Review -->
    @if (isset($reviews) && $reviews->count() > 0)
        @include('EndUser.review')
    @endif
    <!-- End section Review -->
@endsection

