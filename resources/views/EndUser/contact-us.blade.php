@extends('EndUser.layouts.master')

@section('title')
    Kingsharing | Contact-Us
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/contact.css')}}" />
@endsection

@section('content')

    </div>
    <!-- End main header -->
    <!-- Start container -->

    <!-- End container -->
    </div>
    <!-- End all header -->
    </header>
    <!-- End header -->
    <!-- Start section bann -->
    <section class="bann">
        <div class="bannerheader1">
            <img
                src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0031.jpg')}}"
                class="imgBanner1"
                alt=""
            />
        </div>
        <div class="bannerheader1">
            <img
                src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0033.jpg')}}"
                class="imgBanner1"
                alt=""
            />
        </div>
        <div class="bannerheader1">
            <img
                src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0038.jpg')}}"
                class="imgBanner1"
                alt=""
            />
        </div>
        <div class="bannerheader1">
            <img
                src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0040.jpg')}}"
                class="imgBanner1"
                alt=""
            />
        </div>
    </section>
    <!-- End section bann -->
    <div class="scrollTop">
        <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
    </div>
    <!-- End Button Scroll To Top -->

    <!-- Start section contact Us -->
    <section class="contact" id="contact">
        <!-- Start container -->
        <div class="container">
            <!-- Start all contact -->
            <div class="allcontactform">
                <!-- Start  Contact1 -->
                <div class="contact1">
                    <h1 class="title">{{ trans('homePage.tell-us') }}</h1>
                    <div class="form1">
                        <form action="{{ route('user.contact.send-message') }}" method="post">
                            @csrf
                            <label for="">{{ trans('homePage.name') }} <span>*</span> </label> <br />
                            <input type="text" class="inputform1" name="name" value="{{ old('name') }}" placeholder="{{ trans('homePage.name_ph') }}" id=""  required />
                            @error('name')
                            <div class="alert alert-danger mt-2 my-2 mb-2 font-weight-bold">{{$message}}</div>
                            @enderror

                            <label for="">{{ trans('homePage.email') }}<span>*</span> </label> <br />
                            <input type="text" class="inputform1" name="email" value="{{ old('email') }}" placeholder="{{ trans('homePage.email') }}" id="" required />
                            @error('email')
                            <div class="alert alert-danger mt-2 my-2 mb-2 font-weight-bold">{{$message}}</div>
                            @enderror

                            <label for="">{{ trans('homePage.whats') }}<span>*</span> </label> <br />
                            <input type="number" class="inputform1" name="phone" value="{{ old('phone') }}" placeholder="{{ trans('homePage.whats_ph') }}" id="" required />
                            @error('phone')
                            <div class="alert alert-danger mt-2 my-2 mb-2 font-weight-bold">{{$message}}</div>
                            @enderror

                            <label for="">{{ trans('homePage.subject') }} </label> <br />
                            <input type="text" class="inputform1" name="subject" value="{{ old('subject') }}" placeholder="{{ trans('homePage.subject_ph') }}" id="" required />
                            @error('subject')
                            <div class="alert alert-danger mt-2 my-2 mb-2 font-weight-bold">{{$message}}</div>
                            @enderror

                            <label for="">{{ trans('homePage.message') }} </label> <br />
                            <textarea
                                name="body"
                                id=""
                                cols="30"
                                rows="10"
                                class="inputform1"
                                placeholder="{{ trans('homePage.message_ph') }}"
                                required
                            >{{ old('body') }}</textarea>
                            @error('body')
                            <div class="alert alert-danger mt-2 my-2 mb-2 font-weight-bold">{{$message}}</div>
                            @enderror

                            <label for=""> {{ trans('homePage.confirm-cap') }} <span>*</span>
                                <div class="{{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }} mb-3">
                                    {!! NoCaptcha::display() !!}
                                </div>
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                @endif
                            </label>

                            <div class="send1">
                                <input type="submit" class="send" value="{{ trans('homePage.send') }}" />
                            </div>
                        </form>
                    </div>

                </div>
                <!-- End  Contact1 -->

                <!-- Start  Contact2 -->
                <div class="contact2">
                    <h1 class="title">{{ trans('homePage.Contact Us') }}</h1>
                    <p>
                        {{ trans('homePage.contact-body') }}
                    </p>
                    <div class="address">
                        <h2><i class="fa fa-phone"></i> {{ trans('homePage.phone') }}</h2>
                        <p>{{ trans('homePage.phone') }}: (08) 123 456 789</p>
                        <p>{{ trans('homePage.whats') }}: 1009 678 456</p>
                    </div>

                    <div class="address">
                        <h2><i class="fa fa-envelope"></i> {{ trans('homePage.email') }}</h2>
                        <p>yourmail@domain.com</p>
                        <p>support@hastech.company</p>
                    </div>
                </div>
                <!-- End  Contact2 -->
            </div>
            <!-- End all contact -->
        </div>
        <!-- End container -->
    </section>
    <!-- End section contact Us -->
@endsection
