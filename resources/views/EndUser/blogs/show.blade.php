@extends('EndUser.layouts.blog')

@section('title')
    {{ trans('homePage.Blogs') }} | {{ $blog->title }}
@endsection
@section('content')
    <!-- Start section blog -->
    <section class="blog">
        <!-- Start title section -->
        <div class="titleSection">
            <h1 class="title">{{ trans('homePage.our blog') }}</h1>
        </div>
        <!-- End title section -->

        <!-- Start container -->
        <div class="container">
            <!-- Start all blog -->
            <div class="allBlog">
                <div class="container">
                    <div class="row">
                        <div class="col-8 mx-auto">
                            <!-- start blog one -->
                            <div class="blogOnex mx-auto">

                                <h1 class="title text-center">
                                    {{ $blog->title }}
                                </h1>
                                <hr/>
                                <!-- Start img -->
                                <div class="text-center mb-3">
                                    <img src="{{ $blog->image }}" alt="{{ $blog->title . '_image' }}" width="600px" height="250px"/>
                                </div>
                                <!-- End img -->
                                <hr/>
                                <!-- start contnet -->
                                <div class="content text-right">

                                    <p class="text">
                                        {!! $blog->description !!}
                                    </p>
                                </div>
                                <!-- End contnet -->
                                <!-- Start views -->
                                <div class="views">
                                    <span>{{ trans('homePage.Created') }}: {{ $blog->created_at }}</span>
                                </div>
                                <!-- End views -->
                            </div>
                            <!-- End blog one -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End all blog -->
        </div>
        <!-- End container -->
    </section>
    <!-- End section blog -->
@endsection
