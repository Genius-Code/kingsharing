@extends('EndUser.layouts.blog')

@section('title')
    {{ trans('homePage.Blogs') }}
@endsection
@section('content')
    <!-- Start section blog -->
    <section class="blog">
        <!-- Start title section -->
        <div class="titleSection">
            <h1 class="title">{{ trans('homePage.our blog') }}</h1>
        </div>
        <!-- End title section -->

        <!-- Start container -->
        <div class="container">
            <!-- Start all blog -->
            <div class="allBlog">
                @isset($blogs)
                    @forelse($blogs as $blog)
                        <!-- start blog one -->
                        <div class="blogOne">
                            <!-- Start img -->
                            <div class="img">
                                <img src="{{ $blog->image }}" alt="{{ $blog->title . '_image' }}" width="350px" height="250px"/>
                            </div>
                            <!-- End img -->
                            <!-- start contnet -->
                            <div class="content">
                                <a href="{{ route('user.blog.show', $blog) }}"> <h1 class="title">
                                    {{ $blog->title }}
                                </h1></a>
                                <p class="text">
                                    {!! $blog->description !!}
                                </p>
                            </div>
                            <!-- End contnet -->
                            <!-- Start views -->
                            <div class="views">
                                <span>27 august</span>
                                <span>{{ trans('homePage.Created') }}: {{ $blog->created_at }}</span>
                                <span>25k views</span>
                            </div>
                            <!-- End views -->
                        </div>
                        <!-- End blog one -->
                     @empty
                     @endforelse
                @endisset
            </div>
            <!-- End all blog -->
        </div>
        <div class="text-center">
            {{ $blogs->links() }}
        </div>

        <!-- End container -->
    </section>
    <!-- End section blog -->
@endsection
