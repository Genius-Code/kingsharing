<!-- Start footer -->
<div class="footer">
    <!-- Start footerImg -->
    <div class="footerImg">
        <div class="imgfoot">
            <img src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0033.jpg') }}" alt="">
        </div>
        <div class="container">
            <div class="contentFooterImg">
                <div class="overlay"></div>
                <p>
                    {{ trans('homePage.enjoy') }}
                </p>
                <a href="https://wa.me/+201001245613" target="_blank"><button class="btn">{{ trans('homePage.try') }}</button></a>
            </div>
        </div>
    </div>
    <!-- End footerImg -->
    <!-- Start main footer -->
    <div class="mainFooter">
        <!-- start all footer -->
        <div class="container">
            <!-- Start container -->
            <div class="allFooter">
                <!-- Start foot1 -->
                <div class="foot1">
                    <div class="logo">
                        <img src="{{ URL::asset('assetsEndUser/shared/img/king-tv.png') }}" alt="" />
                    </div>
                    <p class="textFoot1">
                        {{ trans('homePage.comment-footer') }}
                    </p>
                    <p class="add">
                        <i class="fa fa-location-arrow"></i>: Cairo, Egypt
                    </p>
                    <p class="add"><i class="fa fa-phone"></i>: +123 321 345</p>
                    <p class="add">
                        <i class="fa fa-envelope"></i>: Example@gmail.com
                    </p>
                    <div class="social">
                        <i class="fa-brands fa-facebook "></i>
                        <i class="fa-brands fa-twitter "></i>
                        <i class="fa-brands fa-linkedin "></i>
                        <i class="fa-brands fa-youtube "></i>
                        <i class="fa-brands fa-instagram "></i>
                    </div>
                    <div class="payone">
                        <img src="{{ URL::asset('assetsEndUser/shared/img/payment/1.png') }}" alt="" />
                    </div>
                </div>

                <!-- Start foot1 -->
                <!-- Start foot 2 -->
                <div class="foot2">
                    <h1 class="titleFoot2">{{ trans('homePage.INFORMATION') }}</h1>
                    <ul>
                        <li><a href="{{ route('user.contact.index') }}">{{ trans('homePage.Contact Us') }} </a></li>
                        <li><a href="{{ route('user.privacy') }}">{{ trans('homePage.Privacy Policy') }} </a></li>
                    </ul>
                </div>
                <!-- End foot 2 -->
                <!-- Start foot 2 -->
                <div class="foot2">
                    <h1 class="titleFoot2">{{ trans('homePage.web-site') }}</h1>
                    <ul>
                        <li><a href="{{ route('user.index') }}">{{ trans('homePage.home') }}</a></li>
                        <li><a href="{{ route('user.cccam.index') }}">{{ trans('homePage.cccam') }}</a></li>
                        <li><a href="{{ route('user.blog.index') }}">{{ trans('homePage.blog') }}</a></li>
                        <li><a href="{{ route('user.contact.index') }}">{{ trans('homePage.contact-us') }}</a></li>
                    </ul>
                </div>
                <!-- End foot 2 -->

                <!-- Start foot 2 -->
                <div class="foot2">
                    <h1 class="titleFoot2">{{ trans('homePage.MY ACCOUNT') }}</h1>
                    <ul>
                        <li><a href="{{ route('auth') }}" target="_blank">{{ trans('homePage.login') }} </a></li>
                        <li><a href="{{ route('registerPage') }}">{{ trans('homePage.register') }} </a></li>
                        <li><a href="{{ route('forget-password-page') }}">{{ trans('homePage.forget-password') }} </a></li>
                    </ul>
                    <div class="pay" style="margin-top: 120px">
                        <img src="{{ URL::asset('assetsEndUser/shared/img/payment/1.png') }}" alt="" />
                    </div>
                </div>
                <!-- End foot 2 -->
            </div>
            <!-- End container -->
        </div>
        <!-- End all footer -->
        <div class="payment">
            <div class="name">
                <h3>Designed By Remon Magdy</h3>
            </div>
            <div class="king">
                <h3>&copy; Copyrigt 2022 kingsharing.All Rights Reserved</h3>
            </div>

            <div class="name">
                <h3>Developed By Mohamed Ali</h3>
            </div>
        </div>
    </div>
    <!-- End main footer -->
</div>
<!-- End Footer -->
