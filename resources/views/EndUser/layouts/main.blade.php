<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title')</title>
    <!-- Swiper cdn file -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
    <!-- bootstrap file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/bootstrap.min.css') }}" />

    @if(app()->getLocale() == 'ar')
        <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/ar/css/rtl.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/style.css') }}" />
    @else
    <!-- main css file -->
        <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/style.css') }}" />
    @endif
    <!-- bootstrap file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/all.min.css') }}" />
    @yield('css')
</head>
<body>
{{--<!-- Start loading -->--}}
{{--<div class="animation">--}}
{{--    <div class="content">--}}
{{--        <span class="animation1"></span>--}}
{{--        <span class="animation2"></span>--}}
{{--        <span class="animation3"></span>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- End loading -->--}}
<!-- Satrt header -->
<header id="header" class="ImgHeader">
    <!-- Start all header -->
    <div class="allHeader">
        <!--Start main header -->
        <div class="main_Header">
            <div class="logo">
                <img src="{{ URL::asset('assetsEndUser/shared/img/image.png') }}" alt="" />
            </div>
            <!-- start nav link -->
            <nav>
                <div class="closeBtnNav">
                    <i class="fa fa-close"></i>
                </div>
                <ul class="links">
                    <li class="activeLink"><a href="{{ route('user.index') }}">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Feature</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="{{ route('user.contact.index') }}">Contact Us</a></li>
                </ul>
            </nav>
            <!-- End  nav link -->
            <!-- Start icons User & languges -->
            <div class="icons">
                @auth
                    <div class="userIcon">
                        <a href="{{ route('users.home') }}">
                            <i class="fa fa-home" title="Dashboard"></i>
                        </a>
                    </div>
                @else
                    <div class="userIcon">
                        <a href="{{ route('auth') }}">
                            <i class="fa fa-user" title="LoginPage"></i>
                        </a>
                    </div>
                @endauth


                <div class="selone">
                    <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == 'ar' ? 'en' : 'ar', null, [], true) }}">
                        <img src="{{ LaravelLocalization::getCurrentLocale() == 'en' ? URL::asset('assetsEndUser/shared/img/flag icon/eg.svg') : URL::asset('assetsEndUser/shared/img/flag icon/gb.svg')}}" alt="" /></a>
                </div>

                <div class="toggle">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
            <!-- End icons User & languges -->
        </div>
        <!-- End main header -->

        <!-- Start container -->
        <div class="container">
            <!-- Satrt cotent Header -->
            <div class="content_header">
                <!-- Start box -->
                <div class="box">
                    <div class="content__Box">
                        <h1 class="title">Stable Server</h1>
                        <p class="text">
                            Asperiores fuga quam voluptatibus ad error accusamus possimus
                            iste laudantium, repudiandae culpa corporis sequi cum dolorum
                            veritatis. Ducimus modi quam quas est.
                        </p>
                        <div class="btns">
                            <button class="btn">Buy Now</button>
                            <button class="btn">See More</button>
                        </div>
                    </div>
                </div>
                <!-- End box -->
            </div>
            <!-- End Content header -->
        </div>
        <!-- End container -->
    </div>
    <!-- End all header -->
</header>
<!-- End header -->
<!-- Start section bann -->
<section class="bann">
    <div class="bannerheader1">
        <img
            src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0031.jpg')}}"
            class="imgBanner1"
            alt=""
        />
    </div>
    <div class="bannerheader1">
        <img
            src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0033.jpg')}}"
            class="imgBanner1"
            alt=""
        />
    </div>
    <div class="bannerheader1">
        <img
            src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0038.jpg')}}"
            class="imgBanner1"
            alt=""
        />
    </div>
    <div class="bannerheader1">
        <img
            src="{{ URL::asset('assetsEndUser/shared/img/banner/IMG-20211225-WA0040.jpg')}}"
            class="imgBanner1"
            alt=""
        />
    </div>
</section>
<!-- End section bann -->
{{--<!-- Start content pro -->--}}
{{--<section class="ContentPro">--}}
{{--    <!-- Start container -->--}}
{{--    <div class="container">--}}
{{--        <!-- start all content  -->--}}
{{--        <div class="allContnet">--}}
{{--            <!-- start conentOne -->--}}
{{--            <div class="contentone">--}}
{{--                <div class="cont">--}}
{{--                    <h1>Lorem ipsum dolor sit amet.</h1>--}}
{{--                    <p>--}}
{{--                        Lorem ipsum dolor sit amet consectetur adipisicing elit.--}}
{{--                        Asperiores, magni?--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <i class="fa fa-film"></i>--}}
{{--            </div>--}}
{{--            <!-- End conentOne -->--}}
{{--            <!-- start conentOne -->--}}
{{--            <div class="contentone">--}}
{{--                <div class="cont">--}}
{{--                    <h1>Lorem ipsum dolor sit amet.</h1>--}}
{{--                    <p>--}}
{{--                        Lorem ipsum dolor sit amet consectetur adipisicing elit.--}}
{{--                        Asperiores, magni?--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <i class="fa fa-film"></i>--}}
{{--            </div>--}}
{{--            <!-- End conentOne -->--}}
{{--            <!-- start conentOne -->--}}
{{--            <div class="contentone">--}}
{{--                <div class="cont">--}}
{{--                    <h1>Lorem ipsum dolor sit amet.</h1>--}}
{{--                    <p>--}}
{{--                        Lorem ipsum dolor sit amet consectetur adipisicing elit.--}}
{{--                        Asperiores, magni?--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <i class="fa fa-film"></i>--}}
{{--            </div>--}}
{{--            <!-- End conentOne -->--}}
{{--        </div>--}}
{{--        <!-- End all content  -->--}}
{{--    </div>--}}
{{--    <!-- End container -->--}}
{{--</section>--}}
{{--<!-- End content pro -->--}}

<!-- Start Button Scroll To Top -->
<div class="scrollTop">
    <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
</div>
<!-- End Button Scroll To Top -->

<!-- Start social icons -->
<div class="socialIcons">
    <div class="allIcons">
        <div class="icon1">
            <a href="#"> <i class="fa fa-whatsapp"></i></a>
            <h4>whatsapp</h4>
        </div>
        <div class="icon1">
            <a href="#"> <i class="fa fa-telegram"></i></a>
            <h4>telegram</h4>
        </div>
        <div class="icon1">
            <a href="#"> <i class="fa fa-phone"></i></a>
            <h4>phone</h4>
        </div>
    </div>
    <div class="icongear1">
        <i class="fa iconGear fa-gear"></i>
    </div>
</div>
<!-- End social icons -->
<div class="g">
@yield('content')
</div>
@include('EndUser.layouts.main-footer')
@include('EndUser.layouts.footer-js')
