<!-- Swiper cdn file -->
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
<!-- font Awesome file -->
{{--<link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/font-awesome.min.css') }}" />--}}
<!-- bootstrap file -->
<link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/bootstrap.min.css') }}" />
@if(app()->getLocale() == 'ar')
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/ar/css/rtl.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/style.css') }}" />
@else
<!-- main css file -->
<link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/style.css') }}" />
@endif
<link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/all.min.css') }}" />

@yield('css')
