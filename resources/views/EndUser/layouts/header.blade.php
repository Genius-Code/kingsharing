<header id="header" class="ImgHeader">
    <!-- Start all header -->
    <div class="allHeader">
        <!--Start main header -->
        <div class="main_Header">
            <div class="logo">
                <img src="{{ URL::asset('assetsEndUser/shared/img/image.png') }}" alt="" />
            </div>
            <!-- start nav link -->
            <nav>
                <div class="closeBtnNav">
                    <i class="fa fa-close"></i>
                </div>
                <ul class="links">
                    <li class="activeLink"><a href="{{ route('user.index') }}">{{ trans('homePage.home') }}</a></li>
                    <li><a href="{{ route('user.cccam.index') }}">{{ trans('homePage.cccam') }}</a></li>
                    <li><a href="{{ route('user.blog.index') }}">{{ trans('homePage.blog') }}</a></li>
                    <li><a href="{{ route('user.contact.index') }}">{{ trans('homePage.contact-us') }}</a></li>
                </ul>
            </nav>
            <!-- End  nav link -->
            <!-- Start icons User & languges -->
            <div class="icons">
                @auth
                    <div class="userIcon">
                        <a href="{{ route('users.home') }}">
                            <i class="fa fa-home" title="Dashboard"></i>
                        </a>
                    </div>
                    <div class="userIcon">
                        <form method="post" action="{{ route('user.logout') }}">
                            @csrf
                             <button type="submit" style="background: none; border: none; color: #fff;">
                                <i class="fa fa-sign-out fa-2x" title="Logout"></i>
                            </button>
                        </form>
                    </div>
                @else
                    <div class="userIcon">
                        <a href="{{ route('auth') }}">
                            <i class="fa fa-user" title="LoginPage"></i>
                        </a>
                    </div>
                @endauth


                <div class="selone">
                    <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == 'ar' ? 'en' : 'ar', null, [], true) }}">
                        <img src="{{ LaravelLocalization::getCurrentLocale() == 'en' ? URL::asset('assetsEndUser/shared/img/flag icon/eg.svg') : URL::asset('assetsEndUser/shared/img/flag icon/gb.svg')}}" alt="" /></a>
                </div>

                <div class="toggle">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
            <!-- End icons User & languges -->
        </div>
        <!-- End main header -->

        <!-- Start container -->


            @if(Route::is('user.contact.index'))
            <div class="container">
                <!-- Satrt cotent Header -->
                    <div class="contact">
                        <h1 class="contctTitle">{{ trans('homePage.Contact Us') }}</h1>
                    </div>
            </div>
            @else
                <!-- Start container -->
                <div class="container">
                    <!-- Satrt cotent Header -->
                    <div class="content_header">
                        <!-- Start box -->
                        <div class="box">
                            <div class="content__Box">
                                <h1 class="title">{{ trans('homePage.Stable Server') }}</h1>
                                <p class="text">
                                    {{ trans('homePage.details-head') }}
                                </p>
                                <div class="btns">
                                    <a href="https://wa.me/+201001245613" target="_blank"><button class="btn">{{ trans('homePage.try') }}</button></a>
                                </div>
                            </div>
                        </div>
                        <!-- End box -->
                    </div>
                    <!-- End Content header -->
                </div>
                <!-- End container -->
                </div>
                <!-- End all header -->
                </header>
            @endif
