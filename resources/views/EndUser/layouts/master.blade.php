<!DOCTYPE html>
<html lang="{{ (app()->getLocale() == 'ar') ? 'ar' : 'en' }}" dir="{{ (app()->getLocale() == 'ar') ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title')</title>
    @include('EndUser.layouts.head')
</head>
<body>
@if(!Route::is('user.contact.index'))
    <!-- Start loading -->
    <div class="animation">
        <div class="content">
            <span class="animation1"></span>
            <span class="animation2"></span>
            <span class="animation3"></span>
        </div>
    </div>
    <!-- End loading -->
@endif
<!-- Satrt header -->
@include('EndUser.layouts.header')
<!-- End header -->

@yield('content')

<!-- Start footer -->
@include('EndUser.layouts.main-footer')
@include('EndUser.layouts.footer-js')
</body>
</html>

