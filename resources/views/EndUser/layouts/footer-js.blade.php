<!-- Swiper Link cdn -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Main js -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ URL::asset('assetsEndUser/shared/js/brands.min.js') }}"></script>
<script src="{{ URL::asset('assetsEndUser/shared/js/main.js') }}"></script>
{!! NoCaptcha::renderJs() !!}
@include('sweetalert::alert')
@yield('js')
