<!DOCTYPE html>
<html lang="{{ (app()->getLocale() == 'ar') ? 'ar' : 'en' }}" dir="{{ (app()->getLocale() == 'ar') ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <!-- bootstrap link -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/bootstrap.min.css') }}" />
    <!-- Swiper cdn file -->
    <link
        rel="stylesheet"
        href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
    <!-- main css file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/style.css') }}" />
    <!-- Blog style -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/blog.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/all.min.css') }}" />
</head>
<body>
<!-- Satrt header -->
<header id="header" class="ImgHeader">
    <!-- Start all header -->
    <div class="allHeader">
        <!--Start main header -->
        <div class="main_Header">
            <div class="logo">
                <img src="{{ URL::asset('assetsEndUser/shared/img/image.png') }}" alt="" />
            </div>
            <!-- start nav link -->
            <nav>
                <div class="closeBtnNav">
                    <i class="fa fa-close"></i>
                </div>
                <ul class="links">
                    <li class="activeLink"><a href="{{ route('user.index') }}">{{ trans('homePage.home') }}</a></li>
                    <li><a href="{{ route('user.cccam.index') }}">{{ trans('homePage.cccam') }}</a></li>
                    <li><a href="{{ route('user.blog.index') }}">{{ trans('homePage.blog') }}</a></li>
                    <li><a href="{{ route('user.contact.index') }}">{{ trans('homePage.contact-us') }}</a></li>
                </ul>
            </nav>
            <!-- End  nav link -->
            <!-- Start icons User & languges -->
            <div class="icons">
                @auth
                    <div class="userIcon">
                        <a href="{{ route('users.home') }}">
                            <i class="fa fa-home" title="Dashboard"></i>
                        </a>
                    </div>
                @else
                    <div class="userIcon">
                        <a href="{{ route('auth') }}">
                            <i class="fa fa-user" title="LoginPage"></i>
                        </a>
                    </div>
                @endauth


                <div class="selone">
                    <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == 'ar' ? 'en' : 'ar', null, [], true) }}">
                        <img src="{{ LaravelLocalization::getCurrentLocale() == 'en' ? URL::asset('assetsEndUser/shared/img/flag icon/eg.svg') : URL::asset('assetsEndUser/shared/img/flag icon/gb.svg')}}" alt="" /></a>
                </div>

                <div class="toggle">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
            <!-- End icons User & languges -->
        </div>
        <!-- End main header -->
    </div>
    <!-- End all header -->
</header>
<!-- End header -->

@yield('content')

@include('EndUser.layouts.main-footer')
@include('EndUser.layouts.footer-js')
</body>
</html>
{




