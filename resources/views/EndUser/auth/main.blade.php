<!DOCTYPE html>
<html lang="{{ (app()->getLocale() == 'ar') ? 'ar' : 'en' }}" dir="{{ (app()->getLocale() == 'ar') ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title_page')</title>
    <!-- font awesome file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/all.min.css') }}" />
    <!-- main css file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/style.css') }}" />
    <!-- bootstrap file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/shared/font/bootstrap.min.css') }}" />
    <!-- contact css file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/contact.css') }}" />
    <!-- start sign file -->
    <link rel="stylesheet" href="{{ URL::asset('assetsEndUser/en/css/sign.css') }}">

    {!! NoCaptcha::renderJs() !!}
</head>
<body>
<!-- Start loading -->
<div class="animation">
    <div class="content">
        <span class="animation1"></span>
        <span class="animation2"></span>
        <span class="animation3"></span>
    </div>
</div>
<!-- End loading -->
<!-- Satrt header -->
<header id="header" class="ImgHeader">

    <!-- Start all header -->
    <div class="allHeader">
        <!--Start main header -->
        <div class="main_Header">
            <div class="logo">
                <img src="{{ URL::asset('assetsEndUser/shared/img/image.png') }}" alt="" />
            </div>

            <!-- start nav link -->
            <nav>
                <div class="closeBtnNav">
                    <i class="fa fa-close"></i>
                </div>
                <ul class="links">
                    <li class="activeLink"><a href="{{ route('user.index') }}">{{ trans('homePage.home') }}</a></li>
                    <li><a href="{{ route('user.cccam.index') }}">{{ trans('homePage.cccam') }}</a></li>
                    <li><a href="{{ route('user.blog.index') }}">{{ trans('homePage.blog') }}</a></li>
                    <li><a href="{{ route('user.contact.index') }}">{{ trans('homePage.contact-us') }}</a></li>
                </ul>
            </nav>
            <!-- End  nav link -->
            <!-- Start icons User & languges -->
            <div class="icons">

                <div class="selone">
                    <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == 'ar' ? 'en' : 'ar', null, [], true) }}">
                        <img src="{{ LaravelLocalization::getCurrentLocale() == 'en' ? URL::asset('assetsEndUser/shared/img/flag icon/eg.svg') : URL::asset('assetsEndUser/shared/img/flag icon/gb.svg')}}" alt="" /></a>
                </div>

                <div class="toggle">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
            <!-- End icons User & languges -->
        </div>
        <!-- End main header -->
        <!-- Start container -->
        <div class="container">
            <div class="contact">
                <h1 class="contctTitle">@yield('title_head')</h1>
            </div>
        </div>
        <!-- End container -->
    </div>
    <!-- End all header -->
</header>
<!-- End header -->


<!-- Start section sign up img -->
<div class="singupimg">

</div>
<!-- End section sign up img -->
@yield('content')

<!-- Start footer -->
@include('EndUser.layouts.main-footer')
<!-- End Footer -->

<!-- sign js file -->
{{--<script  src="{{ URL::asset('assetsEndUser/shared/js/signUp.js') }}"></script>--}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script  src="{{ URL::asset('assetsEndUser/shared/js/main.js') }}"></script>
</body>
</html>
