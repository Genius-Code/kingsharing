@extends('EndUser.auth.main')

@section('title_page')
    {{ trans('homePage.register') }}
@endsection
@section('title_head')
    {{ trans('homePage.register') }}
@endsection

@section('content')
    <!-- Start Form SingUp Login -->
    <section class="singin" id="all_Main_Form">
        <!-- Start container -->
        <div class="container">
            <!-- Start all singin -->
            <div class="allsingin">
                <!-- Start singin-1 -->

                <div class="newaccount">
                    <h1 class="titlelog">{{ trans('homePage.create-new-account') }}</h1>
                    <!-- Start contentnew -->
                    <div class="contentnew">
                        <h2 class="cont">{{ trans('homePage.create-your-account') }}</h2>
                        <!-- Start form new -->
                        <div class="formnew">
                            <form action="{{ route('register') }}" method="post">
                                @csrf

                                <label for=""> {{ trans('homePage.name') }} <span>*</span>
                                    <input type="text" class="inputnew" name="name" value="{{ old('name') }}" placeholder="{{ trans('homePage.name_ph') }}" required/>
                                    @error('name')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>


                                <label for=""> {{ trans('homePage.email') }} <span>*</span>
                                    <input type="email" class="inputnew" name="email" value="{{ old('email') }}" placeholder="{{ trans('homePage.email_ph') }}" required/>
                                    @error('email')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <label for=""> {{ trans('homePage.phone') }} <span>*</span>
                                    <input type="text" class="inputnew" name="phone" value="{{ old('phone') }}" placeholder="{{ trans('homePage.phone_ph') }}" required/>
                                    @error('phone')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <label for=""> {{ trans('homePage.password') }} <span>*</span>
                                    <input type="password" class="inputnew" name="password" value="{{ old('password') }}" placeholder="{{ trans('homePage.password_ph') }}" required/>
                                    @error('password')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <label for=""> {{ trans('homePage.confirm-pass') }} <span>*</span>
                                    <input type="password" class="inputnew" name="password_confirmation" placeholder="{{ trans('homePage.confirm-pass_ph') }}" required/>
                                    @error('password')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <label for=""> {{ trans('homePage.confirm-rec') }} <span>*</span>
                                    <div class="{{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }} mb-3">
                                        {!! NoCaptcha::display() !!}
                                    </div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </label>

                                <input type="submit" value="{{ trans('homePage.register-now') }}" class="btn-5" />
                            </form>
                            <a href="{{ route('auth') }}" class="newAccountSignUp">
                                {{ trans('homePage.have-account') }}</a>
                        </div>

                        <!-- End form new -->
                    </div>
                    <!-- End contentnew -->
                </div>
                <!-- End singin-1 -->
                <!-- End new account -->

            </div>

            <!-- End all singin -->
        </div>
        <!-- End container -->
    </section>

    <!-- End Form SingUp Login -->
@endsection

