@extends('EndUser.auth.main')

@section('title_page')
    {{ trans('homePage.forget-password') }}
@endsection

@section('title_head')
    {{ trans('homePage.forget-password') }}
@endsection
@section('content')
    <section class="singin" id="all_Main_Form">
        <!-- Start container -->
        <div class="container">
            <!-- Start all singin -->
            <div class="allsingin">
                <!-- Start singin-1 -->
                    <div class="singin-1">
                        <h1 class="titlelog">{{ trans('homePage.forget-password') }}</h1>
                        <!-- Start content -->
                        <div class="singincontent">
                            @if (Session::has('message'))
                                <div class="alert alert-success" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            <div class="formsing bg-primary-dark">
                                <form action="{{ route('forget-password') }}" method="POST">
                                    @csrf

                                    <label for=""> {{ trans('homePage.email') }} <span>*</span>
                                        <input type="email" class="inputsing" name="email" value="{{ old('email') }}" placeholder="{{ trans('homePage.email_ph') }}" required/>
                                        @error('email')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>

                                    <label for=""> {{ trans('homePage.confirm-rec') }} <span>*</span>
                                        <div class="{{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }} mb-3">
                                            {!! NoCaptcha::display() !!}
                                        </div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </label>

                                    <button type="submit" class="btn btn-primary"><h5>{{ trans('homePage.send-reset') }}</h5></button>
                                </form>
                            </div>
                        </div>
                        <!-- End content -->
                    </div>
                    <!-- End singin-1 -->
            </div>
        </div>
    </section>

@endsection
