@extends('EndUser.auth.main')

@section('title_head')
    {{ trans('homePage.reset-password') }}
@endsection

@section('title_page')
    {{ trans('homePage.reset-password') }}
@endsection

@section('content')
    <section class="singin" id="all_Main_Form">
        <!-- Start container -->
        <div class="container">
            <!-- Start all singin -->
            <div class="allsingin">
                <!-- Start singin-1 -->
                <div class="singin-1">
                    <h1 class="titlelog">{{ trans('homePage.reset-password') }}</h1>
                    <!-- Start content -->
                    <div class="singincontent">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <div class="formsing bg-primary-dark">
                            <form action="{{ route('reset-password') }}" method="POST">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">

                                <label for=""> {{ trans('homePage.email') }} <span>*</span>
                                    <input type="email" class="inputsing" name="email" value="{{ old('email') }}" placeholder="{{ trans('homePage.email_ph') }}" required/>
                                    @error('email')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <label for=""> {{ trans('homePage.password') }} <span>*</span>
                                    <input type="password" class="inputsing" name="password" value="{{ old('password') }}" placeholder="{{ trans('homePage.password_ph') }}" required/>
                                    @error('password')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <label for=""> {{ trans('homePage.confirm-pass') }} <span>*</span>
                                    <input type="password" class="inputsing" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="{{ trans('homePage.confirm-pass_ph') }}" required/>
                                    @error('password_confirmation')
                                    <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                    @enderror
                                </label>

                                <button type="submit" class="btn btn-primary"><h5>{{ trans('homePage.reset-password') }}</h5></button>
                            </form>
                        </div>
                    </div>
                    <!-- End content -->
                </div>
                <!-- End singin-1 -->
            </div>
        </div>
    </section>

@endsection


