@extends('EndUser.auth.main')

@section('title_page')
    {{ trans('homePage.login') }}
@endsection
@section('title_head')
    {{ trans('homePage.login') }}
@endsection
@section('content')
    <!-- Start Form SingUp Login -->
    <section class="singin" id="all_Main_Form">
        <!-- Start container -->
        <div class="container">
            <!-- Start all singin -->
            <div class="allsingin">
                <!-- Start singin-1 -->
                @if(Route::is('auth'))
                    <div class="singin-1">
                        <h1 class="titlelog">{{ trans('homePage.login') }}</h1>
                        <!-- Start content -->
                        <div class="singincontent">
                            <h2 class="cont">{{ trans('homePage.welcome') }}</h2>
                            <div class="socialsing">
                                <a href="{{ route('handle-facebook-redirect') }}" class="sing"
                                ><i class="fa-brands fa-facebook"></i>{{ trans('homePage.fb-log') }}</a
                                >
                                <a href="{{ route('handle-google-redirect') }}" class="sing"
                                ><i class="fa-brands fa-google"></i>{{ trans('homePage.google-log') }}</a
                                >
                            </div>
                            <div class="formsing">
                                <form action="{{ route('login') }}" method="post">
                                    @csrf

                                    <label for=""> {{ trans('homePage.email') }} <span>*</span>
                                        <input type="email" class="inputsing" name="email" placeholder="{{ trans('homePage.email_ph') }}" value="{{ old('email') }}" required />
                                        @error('email')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>

                                    <label for=""> {{ trans('homePage.password') }} <span>*</span>
                                        <input type="password" class="inputsing" name="password" placeholder="{{ trans('homePage.password_ph') }}" required/>
                                        @error('password')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>


                                    <div class="form-1">
                                        <div class="radio">
                                            <label for="">
                                                <input type="radio"  name="remember"/> {{ trans('homePage.remember') }}
                                            </label>
                                        </div>
                                        <a href="{{ route('forget-password-page') }}">{{ trans('homePage.forget-pass') }}</a>
                                    </div>

                                    <label for=""> {{ trans('homePage.confirm-rec') }} <span>*</span>
                                        <div class="{{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }} mb-3">
                                            {!! NoCaptcha::display() !!}
                                        </div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </label>

                                    <input type="submit" value="{{ trans('homePage.login') }}" class="btn-5" />
                                </form>

                            </div>
                            <a href="{{ route('register') }}" class="newAccountSignIn">{{ trans('homePage.create-new-account') }}</a>
                        </div>
                        <!-- End content -->
                    </div>
                    <!-- End singin-1 -->
                @elseif(Route::is('registerPage'))
                <!-- Start new account -->
                    <div class="newaccount">
                        <h1 class="titlelog">Create a new account</h1>
                        <!-- Start contentnew -->
                        <div class="contentnew">
                            <h2 class="cont">Create your new account.</h2>
                            <!-- Start form new -->
                            <div class="formnew">
                                <form action="{{ route('register') }}" method="post">
                                    @csrf

                                    <label for=""> Name <span>*</span>
                                        <input type="text" class="inputnew" name="name" value="{{ old('name') }}"/>
                                        @error('name')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>


                                    <label for=""> Email Address <span>*</span>
                                        <input type="email" class="inputnew" name="email" value="{{ old('email') }}"/>
                                        @error('email')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>

                                    <label for=""> Phone Number <span>*</span>
                                        <input type="text" class="inputnew" name="phone" value="{{ old('phone') }}"/>
                                        @error('phone')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>

                                    <label for=""> Password <span>*</span>
                                        <input type="password" class="inputnew" name="password" value="{{ old('password') }}"/>
                                        @error('password')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>

                                    <label for=""> Confirm Password <span>*</span>
                                        <input type="password" class="inputnew" name="password_confirmation"/>
                                        @error('password')
                                        <div class="alert alert-danger mt-2 my-2 mb-2">{{$message}}</div>
                                        @enderror
                                    </label>

                                    <input type="submit" value="SING UP" class="btn-5" />
                                </form>
                                <a href="{{ route('login') }}" class="newAccountSignUp">
                                    Already Have Account?</a>
                            </div>

                            <!-- End form new -->
                        </div>
                        <!-- End contentnew -->
                    </div>
                    <!-- End new account -->
                @endif
            </div>

            <!-- End all singin -->
        </div>
        <!-- End container -->
    </section>

    <!-- End Form SingUp Login -->
@endsection




