<?php

return [

        'Hi'                   => 'Hi',
        'welcome back!'        => 'welcome back!',
        'Dashboard User'       => 'Dashboard User',
        'Kingsharing'          => 'Kingsharing',
        'Messages'             => 'Messages',
        'You have'             => 'You have',
        'unread messages'      => 'unread messages',
        'VIEW ALL'             => 'VIEW ALL',
        'client'               => 'Client',
        'Profile'              => 'Profile',
        'Change-Password'      => 'Change-Password',
        'Sign Out'             => 'Logout',
        'Client status'        => 'Client status',
        'Status and Tracking'  => 'Status and Tracking.',
];
