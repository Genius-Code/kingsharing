<?php

return [

     'Change Password'  => 'Change Password',
     'Kingsharing'      => 'Kingsharing',
     'Dashboard'        => 'Dashboard',
     'Old Password'     => 'Old Password',
     'old-password_ph'  => 'Please Enter Your Old Password',
     'New Password'     => 'New Password',
     'password_ph'      => 'Please Enter your New Password',
     'Change'           => 'Change',
     'update-password'  => 'Password Updated Successfully',
     'not-old'          => 'New Password Can\'t Be OldPassword',
     'not-match'        => 'OldPassword Not Matched!',
 ];
