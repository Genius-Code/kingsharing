<?php

return [

    'dashboard'         => 'Dashboard',
    'Messages'          => 'Messages',
    'Inbox'             => 'Inbox',
    'You have'          => 'You have',
    'Send New Message'  => 'Send New Message',
    'Show Message'      => 'Show Message',
    'Files'             => 'Files',
    'Send Message'      => 'Send Message',
    'Subject'           => 'Subject',
    'Subject_ph'        => 'Enter Title Of Message Please',
    'Message'           => 'Message',
    'Message_ph'        => 'Enter Body Of Message Please',
    'Upload'            => 'Upload',
    'send-data'         => 'Message Has Sent Successfully'
];
