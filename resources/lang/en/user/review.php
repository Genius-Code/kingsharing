<?php

return [

    'dashboard'   => 'Dashboard',
    'review'      => 'Review',
    'add-review'  => 'Add Your Review',
    'comment'     => 'Comment',
    'comment_ph'  => 'Add your Comment Please',
    'add-comment' => 'We Want Your Review About Our Service',
    'data-store'  => 'Your Comment Has Been Added, Will Be shown soon',

];
