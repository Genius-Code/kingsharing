<?php

return [

    'user'            => 'user',
    'admin'           => 'admin',
    'dashboard'       => 'Dashboard',
    'site'            => 'Live WebSite',
    'main'            => 'Main',
    'sitting'         => 'Sitting',
    'Messages'        => 'Messages',
    'InBox'           => 'InBox',
    'Send Message'    => 'Send Message',
    'review'          => 'Review Service',
    'Add Your Review' => 'Add Your Review',

];
