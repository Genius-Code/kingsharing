<?php

return [

    'Update Profile'        => 'Update Profile',
    'Dashboard'             => 'Dashboard',
    'Social'                => 'Social',
    'Facebook'              => 'Facebook',
    'whatsapp-Number'       => 'Whatsapp-Number',
    'Mobile'                => 'Mobile',
    'Current Address'       => 'Current Address',
    'no-data'               => 'No Data Found !',
    'Name'                  => 'Name',
    'User Name'             => 'User Name',
    'User Name_ph'          => 'Please Enter Your Username',
    'Contact Info'          => 'Contact Info',
    'Email'                 => 'Email',
    'required'              => 'Required',
    'Email_ph'              => 'Please Enter Valid Email',
    'Phone'                 => 'Phone',
    'Phone_ph'              => 'Please Enter Phone',
    'Country'               => 'Country',
    'Country_ph'            => 'Please Enter Your Country',
    'Social Info'           => 'Social Info',
    'Facebook_ph'           => 'Please Enter Your Facebook Account URL',
    'whatsapp Number_ph'    => 'Please Enter Your Whatsapp Number',
    'Upload Image Profile'  => 'Upload Image Profile',
    'Upload Photo'          => 'Upload Photo',
    'Conatct'               => 'Conatct',
    'Personal Information'  => 'Personal Information',
    'update-data'           => 'Profile Updated Successfully'

];
