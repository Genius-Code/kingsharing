<?php

return [

    'show-all-blog'       => 'Show All Blog',
    'create-new-blog'     => 'Create New Blog',
    'blog'                => 'Blogs',
    'dashboard'           => 'Dashboard',
    'add-new-blog'        => 'Add New Blog',
    'title'               => 'Title',
    'image'               => 'Image',
    'description'         => 'Description',
    'title-en'            => 'Title In English',
    'title-ar'            => 'Title In Arabic',
    'title-en_ph'         => 'Enter Title In English',
    'title-ar_ph'         => 'Enter Title In Arabic',
    'update-blog'         => 'Update Blog',
    'store-data'          => 'Record Has Been Created',
    'update-data'         => 'Record Has Been Updated',
    'delete-data'         => 'Record Has Been Deleted',
    'no-data'             => 'Record Not Found !',


];
