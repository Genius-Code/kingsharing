<?php

return [

    'Show All Offers'        => 'Show All Offers',
    'Create New Offer'       => 'Create New Offer',
    'offers'                 => 'Offers',
    'dashboard'              => 'Dashboard',
    'add-new-offer'          => 'Add New Offer',
    'title'                  => 'Title',
    'package-server'         => 'Package Server',
    'price'                  => 'Price',
    'image'                  => 'Image',
    'status'                 => 'Status',
    'title-ar'               => 'Title In Arabic',
    'title-en_ph'            => 'Enter Title In English',
    'title-ar_ph'            => 'Enter Title In Arabic',
    'new-price'              => 'New Price',
    'price_ph'               => 'Enter New Price',
    'duration'               => 'Duration',
    'duration_ph'            => 'Enter Duration Package',
    'select-package-server'  => 'Select Package Server',
    'details-ar'             => 'Details In Arabic',
    'details-en'             => 'Details In English',
    'details-ar_ph'          => 'Enter Details In Arabic',
    'details-en_ph'          => 'Enter Details In English',
    'choose'                 => 'Choose Status',
    'update-offer'           => 'Update Offer',
    'store-data'             => 'Offer Created Successfully',
    'update-data'            => 'Offer Updated Successfully',
    'delete-data'            => 'Offer Deleted Successfully',

];
