<?php

return [

    'Show All Ads'          => 'Show All Ads',
    'Create New Ads'        => 'Create New Ads',
    'ads'                   => 'ADS',
    'dashboard'             => 'Dashboard',
    'add-new-ad'            => 'Add New AD',
    'create-new-ad'         => 'Create New Add',
    'image'                 => 'Image',
    'description'           => 'Description',
    'description-ar'        => 'Description In Arabic',
    'description-en'        => 'Description In English',
    'description-ar_ph'     => 'Enter Description In Arabic',
    'description-en_ph'     => 'Enter Description In English',
    'images-ads'            => 'Images Ads',
    'choose-file'           => 'Choose file',
    'image-slider'          => 'Images Sliders Ads',
    'update-ad'             => 'Update ِAd',
    'store-data'            => 'Ads Created Successfully',
    'update-data'           => 'Ads Updated Successfully',
    'delete-data'           => 'Ads Deleted Successfully',
    'no-data'               => 'No Record Found',


];
