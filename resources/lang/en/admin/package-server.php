<?php

return [

    'PackageServers'           => 'PackageServers',
    'Show All Package Servers' => 'Show All Package Servers',
    'Create New Package'       => 'Create New Package',
    'Dashboard'                => 'Dashboard',
    'Add New Package'          => 'Add New Server',
    'server'                   => 'Server',
    'price'                    => 'Price',
    'name-en'                  => 'Name Of Server In English',
    'name-ar'                  => 'Name Of Server In Arabic',
    'duration'                 => 'Duration',
    'details-ar'               => 'Details In Arabic',
    'details-en'               => 'Details In English',
    'details-ar_place_holder'  => 'Enter Details Server In Arabic',
    'details-en_place_holder'  => 'Enter Details Server In  English',
    'store-data'               => 'Package Has Been Created Successfully',
    'update-data'              => 'Package Has Been Updated Successfully',
    'delete-data'              => 'Package Has Been Deleted Successfully',
    'no-data'                  => 'Package Not Found !, Try again later',
    'details'                  => 'Details',
    'month'                    => 'Details'


];
