<?php

return [

    'Show All Messages'    => 'Show All Messages',
    'All Messages'         => 'All Messages',
    'Deleted Messages'     => 'Deleted Messages',
    'Dashboard'            => 'Dashboard',
    'ID'                   => 'ID',
    'Name'                 => 'Name',
    'Email'                => 'Email',
    'Title'                => 'Title',
    'Status'               => 'Status',
    'Inbox'                => 'Inbox',
    'message-sent'         => 'Message Sent Successfully',
    'delete-data'          => 'Message Deleted Successfully',
    'restore-data'         => 'Message Has Been Restored Successfully',
    'Compose new message'  => 'Compose new message',
    'Subject'              => 'Subject',
    'Message'              => 'Message',
    'Upload'               => 'Upload',
    'Show Message'         => 'Show Message',
    'Files'                => 'Files',
    'Files Send By Admin'  => 'Files Send By Admin',


];
