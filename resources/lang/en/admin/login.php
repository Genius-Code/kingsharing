<?php

return [
     'Admin Dashboard'              => 'Admin Dashboard',
     'SignIn'                       => 'SignIn',
     'Welcome back!'                => 'Welcome back!',
     'Please sign in to continue.'  => 'Please sign in to continue.',
     'Email'                        => 'Email',
     'email_ph'                     => 'Please Enter Your Email',
     'Password'                     => 'Password',
     'password_ph'                  => 'Please Enter Your Password',
     'invalid-login'                => 'Invalid Email Or Password',
     'not-allowed'                  => 'You Don\'t Allowed To Access This Page',
     'danger'                       => 'Danger!',
     'welcome'                      => 'Welcome Back '
];
