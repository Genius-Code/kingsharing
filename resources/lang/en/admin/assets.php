<?php

return [

    'Assets'              => 'Site Config',
    'Show All Sittings'   => 'Show All Sittings',
    'Create New Setting'  => 'Create New Setting',
    'Dashboard'           => 'Dashboard',
    'Settings'            => 'Settings',
    'Add New Setting'     => 'Add New Setting',
    'Assets WebSite'      => 'Config WebSite',
    'Create New'          => 'Create New',
    'Email'               => 'Email',
    'Logo'                => 'Logo',
    'Phone'               => 'Phone',
    'Address'             => 'Address',
    'FaceBook'            => 'FaceBook',
    'Edit Assets'         => 'Edit Config',
    'Setting'             => 'Setting',
    'Update  Setting'     => 'Update  Setting',
    'Email_ph'            => 'Enter Your Email',
    'Phone_ph'            => 'Enter Your Phone',
    'Address_ph'          => 'Enter Your Address',
    'FaceBook Page Url'   => 'FaceBook Page Url',
    'facebook_ph'         => 'Enter Your Facebook',
    'telegram'            => 'Telegram Page Url',
    'telegram_ph'         => 'Enter Your Telegram Url',
    'youtube'             => 'Youtube Page Url',
    'youtube_ph'          => 'Enter Your Youtube Url',
    'instagram'           => 'Instagram Page Url',
    'instagram_ph'        => 'Enter Your Instagram Url',
    'store-data'          => 'Data Has Been Created Successfully',
    'update-data'         => 'Data Has Been Updated Successfully',
    'delete-data'         => 'Data Has Been Deleted Successfully',
    'no-data'             => 'Record Not Found Please try again later !'

];
