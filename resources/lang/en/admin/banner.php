<?php

return [
    'Banners'           => 'Banners',
    'Show All Banners'  => 'Show All Banners',
    'Create New Banner' => 'Create New Banner',
    'Dashboard'         => 'Dashboard',
    'Add New Banner'    => 'Add New Banner',
    'title'             => 'Title',
    'image'             => 'Image',
    'description'       => 'Description',
    'url'               => 'Url',
    'update-banner'     => 'Update Banner',
    'title-en'          => 'Title In English',
    'title-ar'          => 'Title In Arabic',
    'url-place-holder'  => 'Enter Your Link',
    'description-ar'    => 'Desription In Arabic',
    'description-en'    => 'Desription In English',
    'store-data'        => 'Banner Has Been Added Successfully',
    'update-data'       => 'Banner Has Been Updated Successfully',
    'delete-data'       => 'Banner Has Been Deleted Successfully',




];
