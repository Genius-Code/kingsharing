<?php

return [

    'Action'            => 'Action',
    'Edit'              => 'Edit',
    'Delete'            => 'Delete',
    'Create'            => 'Create',
    'Choose Action'     => 'Choose Action',
    'update'            => 'Update',
    'Accept'            => 'Accept',
    'Show'              => 'Show',
    'Replay'            => 'Replay',
    'Soft Delete'       => 'Soft Delete',
    'Restore'           => 'Restore',
    'Send'              => 'Send',
    'Reply'             => 'Reply',



];
