<?php


return [

   'Update Profile'         => 'Update Profile',
   'Admin Dashboard'        => 'Admin Dashboard',
   'Dashboard'              => 'Dashboard',
   'Social'                 => 'Social',
   'no-data-found'          => 'No Data Found !',
   'Facebook'               => 'Facebook',
   'whatsapp-Number'        => 'Whatsapp Number',
   'Conatct'                => 'Conatct',
   'Mobile'                 => 'Mobile',
   'Country'                => 'Country',
   'Personal Information'   => 'Personal Information',
   'Name'                   => 'Name',
   'username'               => 'User Name',
   'username_ph'            => 'Please Enter Your Full Name',
   'contact-info'           => 'Contact Info',
   'email'                  => 'Email',
   'email_ph'               => 'Please Enter Your Email',
   'required'               => 'Required',
   'phone'                  => 'Phone',
   'phone_ph'               => 'Please Enter Your Phone',
   'country'                => 'Country',
   'country_ph'             => 'Please Enter Your Country',
   'facebook_ph'            => 'Please Enter Your Facebook Acoount Url',
   'whatsapp-Number_ph'     => 'Please Enter Your Whatsapp Number',
   'Upload Image Profile'   => 'Upload Image Profile',
   'Upload Photo'           => 'Upload Photo',
   'Update'                 => 'Update',
   'Social Info'            => 'Social Info',



];
