<?php

return [

    'manager'       => 'Admin',
    'main'          => 'Main',
    'dashboard'     => 'Dashboard',
    'live website'  => 'Go Site',
    'general'       => 'General',
    'slider'        => 'Sliders',
    'banner'        => 'Banners',
    'iptv-server'   => 'Iptv Server',
    'offers'        => 'Offers',
    'ads'           => 'ADS',
    'blogs'         => 'Blogs',
    'clients'       => 'Clients',
    'client-review' => 'Client Review',
    'inbox'         => 'Inbox',
    'users'         => 'Users',
    'contact'       => 'Contact',
    'mail-contact'  => 'Mail Contact',
    'settings'      => 'Settings',
    'cccam-server'  => 'Cccam Server'
];
