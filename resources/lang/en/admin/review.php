<?php

return [

    'Show All Client Review'  => 'Show All Client Review',
    'Show UnRead Review'      => 'Show UnRead Review',
    'Review Clients'          => 'Review Clients',
    'Dashboard'               => 'Dashboard',
    'Email'                   => 'Email',
    'Name'                    => 'Name',
    'Description'             => 'Description',
    'Status'                  => 'Status',
    'store-data'              => 'Your Review Added Successfully, Will Be Shown after moderation',
    'update-data'             => 'Review Approved',
    'no-data'                 => 'Record Not Found',
    'delete-data'             => 'Review Deleted Successfully'

];
