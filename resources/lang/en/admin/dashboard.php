<?php

return [

     'Dashboard Admin'       => 'Dashboard Admin',
     'Kingsharing'           => 'Kingsharing',
     'Hi'                    => 'Hi',
     'welcome back!'         => 'welcome back!',
     'saerch_ph'             => 'Search for anything',
     'Messages'              => 'Messages',
     'Un Read Messages'      => 'Un Read Messages',
     'You have'              => 'You have',
     'unread messages'       => 'unread messages',
     'VIEW ALL'              => 'VIEW ALL',
     'Notifications'         => 'Notifications',
     'UnRead Mails'          => 'UnRead Mails',
     'unread Notifications'  => 'unread Notifications',
     'Profile'               => 'Profile',
     'Change-Password'       => 'Change-Password',
     'Sign Out'              => 'Sign Out',
];
