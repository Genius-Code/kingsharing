<?php

return [

  'Show All Sliders'  => 'Show All Sliders',
  'Create New Slider' => 'Create New Slider',
  'Dashboard'         => 'Dashboard',
  'Slider'            => 'Slider',
  'Add New Slider'    => 'Add New Slider',
  'Sliders'           => 'Sliders',
  'Image'             => 'Image',
  'Status'            => 'Status',
  'publish'           => 'If You Want Publish It Click Here',
  'Update Slider'     => 'Update Slider',
  'store-data'        => 'Slider Created Successfully',
  'update-data'       => 'Slider Updated Successfully',
  'delete-data'       => 'Slider Deleted Successfully',

];
