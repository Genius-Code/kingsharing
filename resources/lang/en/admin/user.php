<?php

return [

    'Show All Users'        => 'Show All Users',
    'Add New User'          => 'Add New User',
    'Dashboard'             => 'Dashboard',
    'Users'                 => 'Users',
    'Create New User'       => 'Create New User',
    'ID'                    => 'ID',
    'Name'                  => 'Name',
    'Email'                 => 'Email',
    'Register By'           => 'Register By',
    'Name_ph'               => 'Enter your Name',
    'Email_ph'              => 'Enter your Email',
    'Phone'                 => 'Phone',
    'Phone_ph'              => 'Enter your Phone',
    'Password'              => 'Password',
    'Password_ph'           => 'Enter your Password',
    'Edit'                  => 'Edit',
    'Page'                  => 'Page',
    'Profile'               => 'Profile',
    'Show'                  => 'Show',
    'Social'                => 'Social',
    'Facebook'              => 'Facebook',
    'whatsapp-Number'       => 'whatsapp-Number',
    'Contact'               => 'Contact',
    'Mobile'                => 'Mobile',
    'Country'               => 'Country',
    'No Data Found !'       => 'No Data Found !',
    'update-profile'        => 'Profile Updated Successfully',
    'update-password'       => 'Password Updated Successfully',
    'old-password'          => 'New Password Can\'t Be OldPassword',
    'not-match'             => 'OldPassword Not Matched!',
    'store-data'            => 'User Created Successfully',
    'update-data'           => 'User Updated Successfully',
    'delete-data'           => 'User Deleted Successfully',
    'no-data'               => 'Record Not Found !, try again later',
    'Change Password'       => 'Change Password',
    'Kingsharing'           => 'Kingsharing',
    'Old Password'          => 'Old Password',
    'Old Password_ph'       => 'Please Enter Your Old Password',
    'New Password'          => 'New Password',
    'New Password_ph'       => 'Please Enter Your New Password',
    'Change'                => 'Change'



];
