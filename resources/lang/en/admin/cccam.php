<?php

return [

     'cccam'                    => 'Cccam',
     'dashboard'                => 'Dashboard',
     'add-new-package'          => 'Add New Package',
     'name'                     => 'Name',
     'duration'                 => 'Duration',
     'price'                    => 'Price',
     'details'                  => 'Details',
     'Show All Package Cccam'   => 'Show All Package Cccam',
     'details-ar'               => 'Details In Arabic',
     'details-ar_ph'            => 'Enter Details In Arabic',
     'details-en'               => 'Details In English',
     'details-en_ph'            => 'Enter Details In English',
     'name_ar'                  => 'Name In Arabic',
     'name_ar_ph'               => 'Enter Name In Arabic ',
     'name_en'                  => 'Name In English',
     'name_en_ph'               => 'Enter Name In English ',
     'duration_ph'              => 'Enter Duration Please',
     'price_ph'                 => 'Enter Price Please',
     'update-new-package'       => 'Update Package',
     'data-store'               => 'Package Added Successfully',
     'data-update'              => 'Package Updated Successfully',
     'data-delete'              => 'Package Deleted Successfully',
];
