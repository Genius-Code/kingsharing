<?php

return [

     'Contact'            => 'Contact',
     'Dashboard'          => 'Dashboard',
     'Mail-Contact'       => 'Mail-Contact',
     'ID'                 => 'ID',
     'Name'               => 'Name',
     'Title'              => 'Title',
     'Email'              => 'Email',
     'Status'             => 'Status',
     'Read'               => 'Read',
     'UnRead'             => 'UnRead',
     'Show Mail Contact'  => 'Show Mail Contact',
     'Mail'               => 'Mail',
     'Back'               => 'Back',
     'delete-data'        => 'Message Deleted Successfully',
];
