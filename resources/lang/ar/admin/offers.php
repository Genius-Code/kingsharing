<?php

return [

    'Show All Offers'        => 'شاهد جميع العروض',
    'Create New Offer'       => 'إنشاء عرض جديد',
    'offers'                 => 'العروض',
    'dashboard'              => 'الرئيسية',
    'add-new-offer'          => 'إضافة عرض جديد',
    'title'                  => 'العنوان',
    'package-server'         => 'السيرفر',
    'price'                  => 'السعر',
    'image'                  => 'الصوره',
    'status'                 => 'الحالة',
    'title-ar'               => 'العنوان باللغه العربية',
    'title-en_ph'            => 'ادخل العنوان باللغه الانجليزية',
    'title-ar_ph'            => 'ادخل العنوان باللغه العربية',
    'new-price'              => 'السعر الجديد',
    'price_ph'               => 'ادخل السعر الجديد',
    'duration'               => 'المده',
    'duration_ph'            => 'ادخل المده الخاص بالسيرفر',
    'select-package-server'  => 'حدد السيرفر',
    'details-ar'             => 'التفاصيل باللغة العربية',
    'details-en'             => 'التفاصيل باللغة الانجليزية',
    'details-ar_ph'          => 'ادخل التفاصيل باللغه العربيه',
    'details-en_ph'          => 'ادخل التفاصيل باللغه الانجليزيه',
    'choose'                 => 'اختر الحالة',
    'update-offer'           => 'تحديث العروض',
    'store-data'             => 'تم اضافة العرض بنجاح',
    'update-data'            => 'تم تعديل العرض بنجاح',
    'delete-data'            => 'تم حذف العرض بنجاح',

];
