<?php

return [

    'Show All Sliders'  => 'جميع السلايدر',
    'Create New Slider' => 'اضافة سلايدر جديد',
    'Dashboard'         => 'الرئيسيه',
    'Slider'            => 'السلايد',
    'Add New Slider'    => 'إضافة سلايد جديد',
    'Sliders'           => 'السلايدز',
    'Image'             => 'الصوره',
    'Status'            => 'الحاله',
    'publish'           => 'إضغط هنا اذا اردت نشر السلايد',
    'Update Slider'     => 'تعديل السلايد',
    'store-data'        => 'تم إضافة السلايد بنجاح',
    'update-data'       => 'تمت عملية التحديث بنجاح',
    'delete-data'       => 'تم الحذف بنجاح',


];
