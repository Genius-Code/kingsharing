<?php

return [

   'Show All Messages'    => 'عرض جميع الرسائل',
   'All Messages'         => 'الرسائل',
   'Deleted Messages'     => 'الرسائل المحذوفة',
   'Dashboard'            => 'الرئيسية',
   'ID'                   => 'المعرف',
   'Name'                 => 'الإسم',
   'Email'                => 'البريد الالكتروني',
   'Title'                => 'العنوان',
   'Status'               => 'الحالة',
   'Inbox'                => 'صندوق الرسائل',
   'message-sent'         => 'تم ارسال الرساة بنجاح',
   'delete-data'          => 'تم حذف الرساله بنجاح',
   'restore-data'         => 'تم استعاده الرسالة بنجاح',
   'Compose new message'  => 'ارسال رساله جديده',
   'Subject'              => 'العنوان',
   'Message'              => 'الرساله',
   'Upload'               => 'المرفقات',
   'Show Message'         => 'عرض الرسالة',
   'Files'                => 'الملفات المرفقه',
   'Files Send By Admin'  => 'ملفات مرسله بواسطة الأدمين',


];
