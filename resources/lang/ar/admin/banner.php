<?php

return [

    'Banners'           => 'البانرز',
    'Show All Banners'  => 'تصفح جميع البانرز',
    'Create New Banner' => 'إضافة بانر جديد',
    'Dashboard'         => 'الرئيسية',
    'Add New Banner'    => 'أضف بانر جديد',
    'title'             => 'العنوان',
    'image'             => 'الصوره',
    'description'       => 'الوصف',
    'url'               => 'الرابط',
    'update-banner'     => 'تحديث بيانات البانر',
    'title-en'          => 'العنوان باللغه الانجليزيه',
    'title-ar'          => 'العنوان باللغه العربية',
    'url-place-holder'  => 'ادخل رابط سليم',
    'description-ar'    => 'الوصف باللغه العربيه',
    'description-en'    => 'الوصف باللغه الانجليزية',
    'store-data'        => 'تم اضافة البانر بنجاح',
    'update-data'       => 'تم تحديث البانر بنجاح',
    'delete-data'       => 'تم حذف البانر بنجاح',

];
