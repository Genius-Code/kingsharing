<?php

return [

     'Dashboard Admin'       => 'لوحة الأدمين',
     'Kingsharing'           => 'كينج شيرنج',
     'Hi'                    => 'أهلاَ',
     'welcome back!'         => 'مرحبا بعودتك',
     'saerch_ph'             => 'البحث',
     'Messages'              => 'الرسائل',
     'Un Read Messages'      => 'رسائل غير مقروءة',
     'You have'              => 'لديك',
     'unread messages'       => 'رسايل غير مقروءة',
     'VIEW ALL'              => 'عرض الكل',
     'Notifications'         => 'رسائل اتصل بنا',
     'UnRead Mails'          => 'ايميلات غير مقروءة',
     'unread Notifications'  => 'ايميلات غير مقروءة',
     'Profile'               => 'الملف الشخصي',
     'Change-Password'       => 'تغيير كلمة المرور',
     'Sign Out'              => 'تسجيل الخروج',
];
