<?php


return [

   'Update Profile'         => 'تحديث الملف الشخصي',
   'Admin Dashboard'        => 'لوحة تحكم الأدمين',
   'Dashboard'              => 'الرئيسية',
   'Social'                 => 'مواقع التواصل الاجتماعي',
   'no-data-found'          => 'لا توجد بيانات حاليا',
   'Facebook'               => 'الفيس بوك',
   'whatsapp-Number'        => 'رقم جوال الواتساب',
   'Conatct'                => 'بيانات الاتصال',
   'Mobile'                 => 'رقم الهاتف',
   'Country'                => 'الدولة',
   'Personal Information'   => 'المعلومات الشخصية',
   'Name'                   => 'البيانات الشخصية',
   'username'               => 'الاسم',
   'username_ph'            => 'من فضلك أدخل الإسم كاملا',
   'contact-info'           => 'معلومات الاتصال',
   'email'                  => 'البريد الالكتروني',
   'email_ph'               => 'من فضلك أدخل البريد الالكتروني',
   'required'               => 'هذا الحقل مطلوب',
   'phone'                  => 'الهاتف',
   'phone_ph'               => 'من فضلك أدخل رقم الهاتف',
   'country'                => 'الدولة',
   'country_ph'             => 'من فضلك أدخل الدولة',
   'facebook_ph'            => 'من فضلك أدخل رابط صفحة الفيس بوك الشخصية',
   'whatsapp-Number_ph'     => 'من فضلك أدخل رقم حساب الواتس',
   'Upload Image Profile'   => 'معلومات صورة البروفايل',
   'Upload Photo'           => 'رفع الصورة',
   'Update'                 => 'تحديث',
   'Social Info'            => 'معلومات مواقع التواصل الاجتماعي',



];
