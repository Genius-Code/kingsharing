<?php

return [

    'Show All Client Review'  => 'آراء العملاء',
    'Show UnRead Review'      => 'أراء غير مقرؤه',
    'Review Clients'          => 'آراء العملاء',
    'Dashboard'               => 'الرئيسية',
    'Email'                   => 'البريد الالكتروني',
    'Name'                    => 'الإسم',
    'Description'             => 'نص الرساله',
    'Status'                  => 'الحاله',
    'store-data'              => 'تم اضافة رأيك بنجاح وسيكون متاح علي الموقع قريبا,شكرا لك',
    'update-data'             => 'تمت الموافقه علي المنشور',
    'no-data'                 => 'البحث غير مطابق',
    'delete-data'             => 'تم الحذف بنجاح'

];
