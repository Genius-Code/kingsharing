<?php

return [

    'PackageServers'           => 'السيرفرات',
    'Show All Package Servers' => 'شاهد جميع السيرفرات',
    'Create New Package'       => 'إضافة سيرفر جديد',
    'Dashboard'                => 'الرئيسية',
    'Add New Package'          => 'إضافة سيرفر جديد',
    'server'                   => 'اسم السيرفر',
    'price'                    => 'السعر',
    'name-en'                  => 'اسم السيرفر باللغه الانجليزيه',
    'name-ar'                  => 'اسم السيرفر باللغه العربيه',
    'duration'                 => 'المده',
    'details-ar'               => 'التفاصيل باللغه العربيه',
    'details-en'               => 'التفاصيل باللغه الانجليزيه',
    'details-ar_place_holder'  => 'ادخل التفاصيل باللغه العربيه',
    'details-en_place_holder'  => 'ادخل التفاصيل باللغه الانجليزيه',
    'store-data'               => 'تم اضافة السيرفر بنجاح',
    'update-data'              => 'تم تعديل السيرفر بنجاح',
    'delete-data'              => 'تم حذف السيرفر بنجاح',
    'no-data'                  => 'العنصر الذي تبحث عنه غير متاح حاليا, حاول مرة أخري ببيانات سليمه',
    'details'                  => 'التفاصيل',
    'month'                    => 'شهر'

];
