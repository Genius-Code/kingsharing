<?php

return [

    'Show All Ads'          => 'عرض جميع الاعلانات',
    'Create New Ads'        => 'إضافة إعلان جديد',
    'ads'                   => 'الإعلان',
    'dashboard'             => 'اللوحه الرئيسية',
    'add-new-ad'            => 'إنشاء اعلان جديد',
    'create-new-ad'         => 'إضافة اعلان جديد',
    'image'                 => 'الصور',
    'description'           => 'الوصف',
    'description-ar'        => 'التفاصيل باللغة العربية',
    'description-en'        => 'التفاصيل باللغة الانجليزية',
    'description-ar_ph'     => 'أدخل التفاصيل باللغة العربية',
    'description-en_ph'     => 'أدخل التفاصيل باللغة الانجليزية',
    'images-ads'            => 'صور الاعلانات',
    'choose-file'           => 'ارفق ملف',
    'image-slider'          => 'صور اعلانات السلايد',
    'update-ad'             => 'تحديث الاعلان',
    'store-data'            => 'تم اضافة الاعلان بنجاح',
    'update-data'           => 'تم تحديث الاعلان بنجاح',
    'delete-data'           => 'تم حذف الاعلان بنجاح',
    'no-data'               => 'حاول مره اخري',

];
