<?php


return [

    'Action'            => 'العمليات',
    'Edit'              => 'تعديل',
    'Delete'            => 'حذف',
    'Create'            => 'أضف',
    'Choose Action'     => 'اختر الاجراء المناسب',
    'update'            => 'تحديث',
    'Accept'            => 'مقبول',
    'Show'              => 'عرض',
    'Replay'            => 'رد',
    'Soft Delete'       => 'حذف مؤقت',
    'Restore'           => 'استعادة',
    'Send'              => 'ارسال',
    'Reply'             => 'رد',


];
