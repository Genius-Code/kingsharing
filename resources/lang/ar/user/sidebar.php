<?php

return [

    'user'          => 'عميل',
    'admin'         => 'الأدمين',
    'dashboard'     => 'لوحة التحكم ',
    'site'          => 'زيارة الموقع',
    'main'          => 'الرئيسية',
    'sitting'       => 'الإعدادات',
    'Messages'      => 'الرسائل',
    'InBox'         => 'الرسائل الوارده',
    'Send Message'  => 'ارسال رساله',

];
