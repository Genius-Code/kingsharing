<?php

return [

    'dashboard'         => 'الرئيسية',
    'Messages'          => 'الرسائل',
    'Inbox'             => 'صندوق الوارد',
    'You have'          => 'لديك',
    'Send New Message'  => 'إرسال رسالة جديدة',
    'Show Message'      => 'عرض الرسالة',
    'Files'             => 'الملفات',
    'Send Message'      => 'ارسل رسالة جديدة',
    'Subject'           => 'العنوان',
    'Subject_ph'        => 'من فضلك أدخل عنوان الرسالة',
    'Message'           => 'الرسالة',
    'Message_ph'        => 'من فضلك أدخل نص الرسالة',
    'Upload'            => 'الملفات المرفقه',
    'send-data'         => 'تم ارسال الرسالة بنجاح'
];
