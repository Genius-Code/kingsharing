<?php

return [

    'dashboard'   => 'Dashboard',
    'review'      => 'رأيك يهمنا',
    'add-review'  => 'أضف رأيك',
    'comment'     => 'تعليقك',
    'comment_ph'  => 'أضف تعليقك الآن',
    'add-comment' => 'ننتظر تعليقك عن الخدمات التي نقدمها',
    'data-store'  => 'تم اضافة تعليقك بنجاح سيكون متاح عبر الموقع بعد وقت قليل'

];
