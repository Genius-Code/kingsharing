<?php

return [

        'Hi'                   => 'أهلا ',
        'welcome back!'        => 'مرحبا بعودتك',
        'Dashboard User'       => 'لوحة تحكم المستخدم',
        'Kingsharing'          => 'كينج شيرنج',
        'Messages'             => 'الرسائل',
        'You have'             => 'لديك',
        'unread messages'      => 'رسائل غير مقروءة',
        'VIEW ALL'             => 'عرض الكل',
        'client'               => 'عميل',
        'Profile'              => 'الملف الشخصي',
        'Change-Password'      => 'تغيير كلمة المرور',
        'Sign Out'             => 'تسجيل خروج',
        'Client status'        => 'إحصائيات العميل',
];
