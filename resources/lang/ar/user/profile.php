<?php

return [

    'Update Profile'        => 'تحديث الملف الشخصي',
    'Dashboard'             => 'الرئيسية',
    'Social'                => 'التواصل الاجتماعي',
    'Facebook'              => 'الفيس بوك',
    'whatsapp-Number'       => 'رقم جوال الواتساب',
    'Mobile'                => 'رقم الهاتف',
    'Current Address'       => 'العنوان',
    'no-data'               => 'لا توجد بيانات متاحه حاليا',
    'Name'                  => 'البيانات الشخصية',
    'User Name'             => 'الإسـم',
    'User Name_ph'          => 'من فضلك أدخل الإسـم كامـلاً',
    'Contact Info'          => 'بيانات التواصل',
    'Email'                 => 'البريد الالكتـروني',
    'required'              => 'مطلوب',
    'Email_ph'              => 'من فضلك أدخل بريد الكتروني صالح',
    'Phone'                 => 'رقم الهاتف',
    'Phone_ph'              => 'من فضلك أدخل رقم الهاتف ',
    'Country'               => 'الدولة',
    'Country_ph'            => 'من فضلك أدخل اسم الدولة',
    'Social Info'           => 'معلومات التواصل الاجتماعي',
    'Facebook_ph'           => 'من فضلك أدخل رابط حساب الفيس بوك الشخصي',
    'whatsapp Number_ph'    => 'من فضلك أدخل رقم جوال الواتساب',
    'Upload Image Profile'  => 'رفع صورة البروفايل',
    'Upload Photo'          => 'رفع مرفق',
    'Conatct'               => 'بيانات الاتصال',
    'Personal Information'  => 'معلومات الحساب الشخصي',
    'update-data'           => 'تم تحديث البيانات بنجاح'

];
