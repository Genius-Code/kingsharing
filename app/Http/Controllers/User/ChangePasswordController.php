<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\User\ChangePasswordInterface;
use App\Http\Requests\User\Password\UpdatePassword;
use App\Models\User;
use Illuminate\Http\Request;

class ChangePasswordController extends Controller
{
    private $changePasswordInterface;

    public function __construct(ChangePasswordInterface $changePassword)
    {
        $this->changePasswordInterface = $changePassword;
    }

    public function index()
    {
        return $this->changePasswordInterface->index();
    }

    public function update(User $user, UpdatePassword $request)
    {
        return $this->changePasswordInterface->updatePassword($user, $request);
    }
}
