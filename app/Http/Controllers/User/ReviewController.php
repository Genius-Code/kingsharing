<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\User\ReviewInterface;
use App\Http\Requests\EndUser\Review\ReviewRequest;

class ReviewController extends Controller
{
    private $reviewInterface;

    public function __construct(ReviewInterface $review)
    {
        $this->reviewInterface = $review;
    }

    public function index()
    {
        return $this->reviewInterface->reviewPage();
    }

    public function store(ReviewRequest $request)
    {
        return $this->reviewInterface->storeReview($request);
    }
}
