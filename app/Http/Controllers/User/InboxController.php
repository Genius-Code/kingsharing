<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\User\InboxInterface;
use App\Http\Requests\User\Inbox\InboxRequest;
use App\Models\ReplayMessage;

class InboxController extends Controller
{
    private $inboxInterface;

    public function __construct(InboxInterface $inbox)
    {
        $this->inboxInterface = $inbox;
    }

    public function index()
    {
        return $this->inboxInterface->index();
    }

    public function show(ReplayMessage $message)
    {
        return $this->inboxInterface->show($message);
    }

    public function sendMessagePage()
    {
        return $this->inboxInterface->sendMessagePage();
    }

    public function sendMessage(InboxRequest $request)
    {
        return $this->inboxInterface->sendMessage($request);
    }
}
