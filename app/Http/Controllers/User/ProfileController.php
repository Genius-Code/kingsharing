<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\User\ProfileInterface;
use App\Http\Requests\User\Profile\UpdateProfile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $profileInterface;

    public function __construct(ProfileInterface $profile)
    {
        $this->profileInterface = $profile;
    }

    public function index()
    {
        return $this->profileInterface->index();
    }

    public function update(UpdateProfile $request)
    {
        return $this->profileInterface->update($request);
    }
}
