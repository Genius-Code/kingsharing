<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\EndUser\BlogInterface;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $blogInterface;

    public function __construct(BlogInterface $blog)
    {
        $this->blogInterface = $blog;
    }

    public function index()
    {
        return $this->blogInterface->index();
    }

    public function show(Blog $blog)
    {
        return $this->blogInterface->show($blog);
    }
}
