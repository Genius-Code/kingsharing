<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\EndUser\GoogleInterface;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    private $googleInterface;

    public function __construct(GoogleInterface $google)
    {
        $this->googleInterface = $google;
    }

    public function handleGoogleRedirect()
    {
        return $this->googleInterface->handleGoogleRedirect();
    }

    public function handleGoogleCallback()
    {
        return $this->googleInterface->handleGoogleCallback();
    }
}
