<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\EndUser\AuthInterface;
use App\Http\Requests\EndUser\Auth\LoginRequest;
use App\Http\Requests\EndUser\Auth\ForgetPassword;
use App\Http\Requests\EndUser\Auth\RegisterRequest;
use App\Http\Requests\EndUser\Auth\ResetPassword;
use App\Models\PasswordReset;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $authInterface;

    public function __construct(AuthInterface $auth)
    {
        $this->authInterface = $auth;
    }

    public function authPage()
    {
        return $this->authInterface->authPage();
    }

    public function login(LoginRequest $request)
    {
        return $this->authInterface->login($request);
    }

    public function registerPage()
    {
        return $this->authInterface->registerPage();
    }

    public function register(RegisterRequest $request)
    {
        return $this->authInterface->register($request);
    }

    public function logout(Request $request)
    {
        return $this->authInterface->logout($request);
    }

    public function redirectToService($service)
    {
        return $this->authInterface->redirectToService($service);
    }

    public function handleCallback($service)
    {
        return $this->authInterface->handleCallback($service);
    }

    public function forgetPasswordPage()
    {
        return $this->authInterface->forgetPasswordPage();
    }

    public function forgetPassword(ForgetPassword $request)
    {
        return $this->authInterface->forgetPassword($request);
    }

    public function resetPasswordPage($token)
    {
        return $this->authInterface->resetPasswordPage($token);
    }

    public function resetPassword(ResetPassword $request)
    {
        return $this->authInterface->resetPassword($request);
    }

    public function redirectToGoogle()
    {
        return $this->authInterface->redirectToGoogle();
    }

    public function handleGoogleCallback()
    {
        return $this->authInterface->handleGoogleCallback();
    }
}
