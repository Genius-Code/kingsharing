<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\EndUser\FacebookInterface;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
    private $facebookInterface;

    public function __construct(FacebookInterface $facebook)
    {
        $this->facebookInterface = $facebook;
    }

    public function handleFacebookRedirect()
    {
        return $this->facebookInterface->handleFacebookRedirect();
    }

    public function handleFacebookCallback()
    {
        return $this->facebookInterface->handleFacebookCallback();
    }
}
