<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\EndUser\CccamServerInterface;
use Illuminate\Http\Request;

class CccamServerController extends Controller
{
    private $cccamServerInterface;

    public function __construct(CccamServerInterface $cccamServer)
    {
        $this->cccamServerInterface = $cccamServer;
    }

    public function index()
    {
        return $this->cccamServerInterface->index();
    }
}
