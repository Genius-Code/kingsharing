<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\InboxInterface;
use App\Http\Requests\Admin\Inbox\InboxRequest;
use App\Http\Requests\Admin\Replay\ReplayMessageRequest;
use App\Models\Message;


class InboxController extends Controller
{
    private $inboxInterface;

    public function __construct(InboxInterface $inbox)
    {
        $this->inboxInterface = $inbox;
    }

    public function index()
    {
        return $this->inboxInterface->index();
    }

    public function show(Message $message)
    {
        return $this->inboxInterface->show($message);
    }

    public function repalyToUserPage(Message $message)
    {
        return $this->inboxInterface->replayPage($message);
    }

    public function repalyToUser(ReplayMessageRequest $request)
    {
        return $this->inboxInterface->replayTo($request);
    }

    public function delete(Message $message)
    {
        return $this->inboxInterface->delete($message);
    }

    public function softDelete(Message $message)
    {
        return $this->inboxInterface->softDelete($message);
    }

    public function deletedMessages()
    {
        return $this->inboxInterface->deletedMeassges();
    }

    public function restoreDeletedMeassges($id)
    {
        return $this->inboxInterface->restoreDeletedMeassges($id);
    }
}
