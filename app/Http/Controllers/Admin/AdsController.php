<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\AdsInterface;
use App\Http\Requests\Admin\Ads\UpdateNewAds;
use App\Http\Requests\Admin\Ads\AddNewAds;
use App\Models\Ads;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    private $adsInterface;

    public function __construct(AdsInterface $ads)
    {
        $this->adsInterface = $ads;
    }

    public function index()
    {
        return $this->adsInterface->index();
    }

    public function create()
    {
        return $this->adsInterface->create();
    }

    public function store(AddNewAds $request)
    {
        return $this->adsInterface->store($request);
    }

    public function edit(Ads $ads)
    {
        return $this->adsInterface->edit($ads);
    }

    public function update(UpdateNewAds $request)
    {
        return $this->adsInterface->update($request);
    }

    public function destroy(Ads $ads)
    {
        return $this->adsInterface->destroy($ads);
    }
}

