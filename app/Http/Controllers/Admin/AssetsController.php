<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\AssetsInterface;
use App\Http\Requests\Admin\Assets\AddAssetRequest;
use App\Http\Requests\Admin\Assets\DeleteAssetRequest;
use App\Http\Requests\Admin\Assets\UpdateAssetRequest;
use App\Models\Asset;
use Illuminate\Http\Request;

class AssetsController extends Controller
{
    private $assetsInterface;

    public function __construct(AssetsInterface $assets)
    {
        $this->assetsInterface = $assets;
    }

    public function index()
    {
        return $this->assetsInterface->index();
    }

    public function create()
    {
        return $this->assetsInterface->create();
    }

    public function store(AddAssetRequest $request)
    {
        return $this->assetsInterface->store($request);
    }

    public function edit(Asset $asset)
    {
        return $this->assetsInterface->edit($asset);
    }

    public function update(UpdateAssetRequest $request)
    {
        return $this->assetsInterface->update($request);
    }

    public function destroy(DeleteAssetRequest $request)
    {
        return $this->assetsInterface->destroy($request);
    }
}
