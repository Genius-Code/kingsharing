<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\SliderInterface;
use App\Http\Requests\Admin\Sliders\AddSlider;
use App\Http\Requests\Admin\Sliders\DeleteSlider;
use App\Http\Requests\Admin\Sliders\UpdateSlider;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    private $sliderInterface;

    public function __construct(SliderInterface $slider)
    {
        $this->sliderInterface = $slider;
    }

    public function index()
    {
        return $this->sliderInterface->index();
    }

    public function create()
    {
        return $this->sliderInterface->create();
    }

    public function store(AddSlider $request)
    {
        return $this->sliderInterface->store($request);
    }

    public function edit(Slider $slider)
    {
        return $this->sliderInterface->edit($slider);
    }

    public function update(UpdateSlider $request)
    {
        return $this->sliderInterface->update($request);
    }

    public function destroy(Slider $slider)
    {
        return $this->sliderInterface->destroy($slider);
    }
}
