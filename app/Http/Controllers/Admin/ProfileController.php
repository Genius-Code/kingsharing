<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\ProfileInterface;
use App\Http\Requests\Admin\Profile\ChangePasswordRequest;
use App\Http\Requests\Admin\Profile\ProfileRequest;

class ProfileController extends Controller
{
    private $profileInterface;

    public function __construct(ProfileInterface $profile)
    {
        $this->profileInterface = $profile;
    }

    public function index()
    {
        return $this->profileInterface->index();
    }

    public function update(ProfileRequest $request)
    {
        return $this->profileInterface->update($request);
    }

    public function passwordPage()
    {
        return $this->profileInterface->passwordPage();
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        return $this->profileInterface->changePassword($request);
    }
}
