<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\PackageServerInterface;
use App\Http\Requests\Admin\PackageServer\AddPackageServer;
use App\Http\Requests\Admin\PackageServer\UpdatePackageServer;
use App\Models\PackageServer;
use Illuminate\Http\Request;

class PackageServerController extends Controller
{
    private $packageServerInterface;

    public function __construct(PackageServerInterface $server)
    {
        $this->packageServerInterface = $server;
    }

    public function index()
    {
        return $this->packageServerInterface->index();
    }

    public function create()
    {
        return $this->packageServerInterface->create();
    }

    public function store(AddPackageServer $request)
    {
        return $this->packageServerInterface->store($request);
    }

    public function show(PackageServer $server)
    {
        return $this->packageServerInterface->show($server);
    }

    public function edit(PackageServer $server)
    {
        return $this->packageServerInterface->edit($server);
    }

    public function update(UpdatePackageServer $request)
    {
        return $this->packageServerInterface->update($request);
    }

    public function destroy(PackageServer $server)
    {
        return $this->packageServerInterface->destroy($server);
    }
}
