<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\UserInterface;
use App\Http\Requests\Admin\User\UserRequest;
use App\Models\User;

class UserController extends Controller
{
    private $userInterface;

    public function __construct(UserInterface $user)
    {
        $this->userInterface = $user;
    }

    public function index()
    {
        return $this->userInterface->index();
    }

    public function show(User $user)
    {
        return $this->userInterface->show($user);
    }

    public function create()
    {
        return $this->userInterface->addNewUserPage();
    }

    public function store(UserRequest $request)
    {
        return $this->userInterface->addNewUser($request);
    }

    public function edit(User $user)
    {
        return $this->userInterface->editUser($user);
    }

    public function update(User $user, UserRequest $request)
    {
        return $this->userInterface->updateUser($user, $request);
    }

    public function delete(User $user)
    {
        return $this->userInterface->delete($user);
    }
}
