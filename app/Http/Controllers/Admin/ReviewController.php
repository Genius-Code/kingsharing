<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\ReviewInterface;
use App\Http\Requests\Admin\Review\AddReview;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    private $reviewInterface;

    public function __construct(ReviewInterface $review)
    {
        $this->reviewInterface = $review;
    }

    public function index()
    {
        return $this->reviewInterface->index();
    }

    public function create()
    {
        return $this->reviewInterface->create();
    }

    public function store(AddReview $request)
    {
        return $this->reviewInterface->store($request);
    }

    public function update(Review $review)
    {
        return $this->reviewInterface->update($review);
    }

    public function destroy(Review $review)
    {
        return $this->reviewInterface->destroy($review);
    }

    public function unread()
    {
        return $this->reviewInterface->unread();
    }
}
