<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\BlogInterface;
use App\Http\Requests\Admin\Blog\AddBlog;
use App\Http\Requests\Admin\Blog\UpdateBlog;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $blogInterface;

    public function __construct(BlogInterface $blog)
    {
        $this->blogInterface = $blog;
    }

    public function index()
    {
        return $this->blogInterface->index();
    }

    public function create()
    {
        return $this->blogInterface->create();
    }

    public function store(AddBlog $request)
    {
        return $this->blogInterface->store($request);
    }

    public function edit(Blog $blog)
    {
        return $this->blogInterface->edit($blog);
    }

    public function update(UpdateBlog $request)
    {
        return $this->blogInterface->update($request);
    }

    public function destroy(Blog $blog)
    {
        return $this->blogInterface->destroy($blog);
    }
}
