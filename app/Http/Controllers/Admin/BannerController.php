<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\BannerInterface;
use App\Http\Requests\Admin\Banner\AddBanner;
use App\Http\Requests\Admin\Banner\UpdateBanner;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $bannerInterface;

    public function __construct(BannerInterface $banner)
    {
        $this->bannerInterface = $banner;
    }

    public function index()
    {
        return $this->bannerInterface->index();
    }

    public function create()
    {
        return $this->bannerInterface->create();
    }

    public function store(AddBanner $banner)
    {
        return $this->bannerInterface->store($banner);
    }

    public function edit(Banner $banner)
    {
        return $this->bannerInterface->edit($banner);
    }

    public function update(UpdateBanner $banner)
    {
        return $this->bannerInterface->update($banner);
    }

    public function destroy(Banner $banner)
    {
        return $this->bannerInterface->destroy($banner);
    }
}
