<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\OfferInterface;
use App\Http\Requests\Admin\Offer\AddOffer;
use App\Http\Requests\Admin\Offer\UpdateOffer;
use App\Models\OfferServer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    private $offerInterface;

    public function __construct(OfferInterface $offer)
    {
        $this->offerInterface = $offer;
    }

    public function index()
    {
        return $this->offerInterface->index();
    }

    public function create()
    {
        return $this->offerInterface->create();
    }

    public function store(AddOffer $request)
    {
        return $this->offerInterface->store($request);
    }

    public function edit(OfferServer $offer)
    {
        return $this->offerInterface->edit($offer);
    }

    public function update(UpdateOffer $offer)
    {
        return $this->offerInterface->update($offer);
    }

    public function destroy(OfferServer $offer)
    {
        return $this->offerInterface->destroy($offer);
    }
}
