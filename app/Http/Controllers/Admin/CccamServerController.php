<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\Admin\CccamServerInterface;
use App\Http\Requests\Admin\Cccam\CccamServerRequest;
use App\Models\CccamServer;

class CccamServerController extends Controller
{
    private $cccamServerInterface;

    public function __construct(CccamServerInterface $cccamServer)
    {
        $this->cccamServerInterface = $cccamServer;
    }

    public function index()
    {
        return $this->cccamServerInterface->index();
    }

    public function create()
    {
        return $this->cccamServerInterface->create();
    }

    public function store(CccamServerRequest $request)
    {
        return $this->cccamServerInterface->store($request);
    }

    public function edit(CccamServer $cccam)
    {
        return $this->cccamServerInterface->edit($cccam);
    }

    public function update(CccamServer $cccam, CccamServerRequest $request)
    {
        return $this->cccamServerInterface->update($cccam, $request);
    }

    public function delete(CccamServer $cccam)
    {
        return $this->cccamServerInterface->delete($cccam);
    }
}
