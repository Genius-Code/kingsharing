<?php


namespace App\Http\Repositories\Admin;


use App\Http\Traits\AssetsTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Asset;
use Illuminate\Http\RedirectResponse;

class AssetsRepository implements \App\Http\Interfaces\Admin\AssetsInterface
{
    use UploaderTrait,
        AssetsTrait;

    private $assetModel;

    public function __construct(Asset $asset)
    {
        $this->assetModel = $asset;
    }

    public function index()
    {
        $settings = $this->showAllAssets();
        return view('Admin.assets.index', compact('settings'));
    }

    public function create()
    {
        return view('Admin.assets.create');
    }

    public function store($request)
    {
        $imageName = $this->upload($request->logo, 'assets', null);

        $this->assetModel::create([
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'facebook' => $request->facebook,
            'telegram' => $request->telegram,
            'youtube' => $request->youtube,
            'instagram' => $request->instagram,
            'logo' => $imageName,
        ]);

        toast(trans('admin/assets.store-data'), 'Success');
        return redirect(route('admin.assets.index'));
    }

    public function edit($setting)
    {
        return view('Admin.assets.edit', compact('setting'));
    }

    public function update($request)
    {
        $asset = $this->showAssetById($request->asset_id);
        if ($request->hasFile('logo')){
            $image = $this->upload($request->logo, 'assets', $asset->getRawOriginal('logo'));
        }
        $asset->update([
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'facebook' => $request->facebook,
            'telegram' => $request->telegram,
            'youtube' => $request->youtube,
            'instagram' => $request->instagram,
            'logo' => isset($image) ? $image : $asset->getRawOriginal('logo')
        ]);

        toast(trans('admin/assets.update-data'), 'Update');
        return redirect(route('admin.assets.index'));
    }

    public function destroy($request): RedirectResponse
    {
        $asset = $this->showAssetById($request->asset_id);

        if ($asset){

            if ($asset->logo){
                $imageUrl = 'assets/'. $asset->getRawOriginal('logo');
                $this->deleteFile($imageUrl);
            }

            $asset->destroy($request->asset_id);

            toast(trans('admin/assets.delete-data'), 'Success');
        }else{
            toast(trans('admin/assets.no-data'), 'Error');
        }
        return redirect()->back();
    }
}
