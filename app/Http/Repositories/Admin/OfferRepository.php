<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\OfferInterface;
use App\Http\Traits\OfferTrait;
use App\Http\Traits\PackageServerTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\OfferServer;
use App\Models\PackageServer;

class OfferRepository implements OfferInterface
{
    use OfferTrait, PackageServerTrait, UploaderTrait;

    private $offerServerModel,
            $packageServerModel;

    public function __construct(OfferServer $offer, PackageServer $server)
    {
        $this->offerServerModel = $offer;
        $this->packageServerModel = $server;
    }

    public function index()
    {
        $offers = $this->showAllOffers();
        return view('Admin.offers.index', compact('offers'));
    }

    public function create()
    {
        $servers = $this->showAllPackages();
        return view('Admin.offers.create', compact('servers'));
    }

    public function store($request)
    {
        $image = $this->upload($request->image, 'offers', null);
        $this->offerServerModel::create([
            'image' => $image,
            'duration' => $request->duration,
            'new_price' => $request->price,
            'status' => $request->status,
            'server_id' => $request->package_server,
        ]);

        toast( trans('admin/offers.store-data'), 'success');
        return redirect(route('admin.offer.index'));
    }

    public function edit($offer)
    {
        $servers = $this->showAllPackages();
        return view('Admin.offers.edit', compact('offer', 'servers'));
    }

    public function update($request)
    {
        $offer = $this->offerServerModel::find($request->offer_id);
        if ($offer)
        {
            if ($request->hasFile('image'))
            {
                $image = $this->upload($request->image, 'offers', $offer->getRawOriginal('image'));
            }

            $offer->update([
                'image' => isset($image) ? $image : $offer->getRawOriginal('image'),
                'duration' => $request->duration,
                'new_price' => $request->price,
                'status' => $request->status,
                'server_id' => $request->package_server,
            ]);
            toast( trans('admin/offers.update-data'), 'success');
        }else
        {
            toast('Offer Not Found !', 'error');
        }
        return redirect(route('admin.offer.index'));
    }

    public function destroy($offer)
    {
        if ($offer)
        {
            if ($offer->image)
            {
                $urlImage = 'offers/' . $offer->getRawOriginal('image');
                $this->deleteFile($urlImage);
            }
            $offer->delete();
            toast( trans('admin/offers.delete-data'), 'delete');
            return back();
        }
    }
}
