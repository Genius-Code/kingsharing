<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\BannerInterface;
use App\Http\Traits\BannersTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Banner;

class BannerRepository implements BannerInterface
{
    use BannersTrait, UploaderTrait;

    private $bannerModel;

    public function __construct(Banner $banner)
    {
        $this->bannerModel = $banner;
    }

    public function index()
    {
        $banners = $this->showAllBanners();
        return view('Admin.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('Admin.banners.create');
    }

    public function store($request)
    {
        $image = $this->upload($request->image, 'ban-image', null);
        $this->bannerModel::create([
            'title' => ['en' => $request->title_en, 'ar' => $request->title_ar],
            'url' => $request->url,
            'image' => $image,
            'description' => ['en' => $request->desc_en, 'ar' => $request->desc_ar]
        ]);
        toast(trans('admin/banner.store-data'), 'success');
        return redirect(route('admin.banner.index'));
    }

    public function edit($banner)
    {
        return view('Admin.banners.edit', compact('banner'));
    }

    public function update($request)
    {
        $banner = $this->bannerModel::find($request->banner_id);

        if ($banner)
        {
            if ($request->hasFile('image'))
            {
                $image = $this->upload($request->image, 'ban-image', $banner->getRawOriginal('image'));
            }

            $banner->update([
                'title' => ['en' => $request->title_en, 'ar' => $request->title_ar],
                'url' => $request->url,
                'image' => isset($image) ? $image :  $banner->getRawOriginal('image'),
                'description' => ['en' => $request->desc_en, 'ar' => $request->desc_ar]
            ]);
            toast(trans('admin/banner.update-data'), 'Update');
            return redirect(route('admin.banner.index'));
        }
    }

    public function destroy($banner)
    {
        if ($banner)
        {
            if ($banner->image)
            {
                $UrlImage = 'ban-image/' . $banner->getRawOriginal('image');
                $this->deleteFile($UrlImage);
            }

            $banner->destroy($banner->id);
            toast(trans('admin/banner.delete-data'), 'Delete');
            return back();
        }
    }
}
