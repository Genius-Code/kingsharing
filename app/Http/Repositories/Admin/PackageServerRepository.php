<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\PackageServerInterface;
use App\Http\Traits\PackageServerTrait;
use App\Models\PackageServer;
use Illuminate\Http\RedirectResponse;

class PackageServerRepository implements PackageServerInterface
{
    use PackageServerTrait;

    private $packageServerModel;

    public function __construct(PackageServer $server)
    {
        $this->packageServerModel = $server;
    }

    public function index()
    {
        $servers = $this->showAllPackages();
        return view('Admin.servers.index', compact('servers'));
    }

    public function create()
    {
        return view('Admin.servers.create');
    }

    public function store($request)
    {
        $this->packageServerModel::create([
            'name' => ['en' => $request->name_en, 'ar' => $request->name_ar],
            'price' => $request->price,
            'duration' => $request->duration,
            'details' => ['en' => $request->desc_en, 'ar' => $request->desc_ar]
        ]);

        toast(trans('admin/package-server.store-data'),'success');
        return redirect(route('admin.server.index'));
    }

    public function edit($server)
    {
        return view('Admin.servers.edit', compact('server'));
    }

    public function show($server)
    {
        return view('Admin.servers.show', compact('server'));
    }

    public function update($request)
    {
        $server = $this->packageServerModel::find($request->server_id);
        if ($server)
        {
            $server->update([
                'name' => ['en' => $request->name_en, 'ar' => $request->name_ar],
                'price' => $request->price,
                'duration' => $request->duration,
                'details' => ['en' => $request->desc_en, 'ar' => $request->desc_ar]
            ]);
            toast(trans('admin/package-server.update-data'), 'update');
            return redirect(route('admin.server.index'));
        }
    }

    public function destroy($server): RedirectResponse
    {
        if ($server)
        {
            $server->delete();
            toast(trans('admin/package-server.store-data'), 'update');
        }
        else
        {
            toast(trans('admin/package-server.no-data'), 'error');
        }
        return back();
    }
}
