<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\AdsInterface;
use App\Http\Traits\AdsTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Ads;
use App\Models\ImageAds;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class AdsRepository implements AdsInterface
{
    use AdsTrait,
        UploaderTrait;

    private $adsModel,
            $imageAdsModel;

    public function __construct(Ads $ads, ImageAds $imageAds)
    {
        $this->adsModel = $ads;
        $this->imageAdsModel = $imageAds;
    }

    public function index()
    {
        $ads = $this->showAllAds();
        return view('Admin.ads.index', compact('ads'));
    }

    public function create()
    {
        return view('Admin.ads.create');
    }

    public function store($request)
    {
        DB::transaction(function () use ($request) {

            $image = $this->upload($request->main_image, 'ads', null);

           $ads = $this->adsModel::create([
                'main_image' => $image,
                'description' => ['en' => $request->desc_en, 'ar' => $request->desc_ar]
            ]);

            $images = $request->file('images');

            if ($request->hasFile('images'))
            {
                foreach ($images as $image)
                {
                    $newImage = $this->upload($image, 'image_ads', null);
                    $this->imageAdsModel::create([
                        'images' => $newImage,
                        'ads_id' => $ads->id
                    ]);
                }
            }
        });
        toast( trans('admin/ads.store-data'), 'success');
        return redirect(route('admin.ads.index'));
    }

    public function edit($ads)
    {
        return view('Admin.ads.edit', compact('ads'));
    }

    public function update($request)
    {
        DB::transaction(function () use ($request) {

            $ads = $this->adsModel::find($request->ads_id);
            if ($request->hasFile('main_image'))
            {
                $image = $this->upload($request->main_image, 'ads', $ads->getRawOriginal('main_image'));
                $ads->update([
                    'main_image' => isset($image) ? $image : $ads->getRawOriginal('main_image'),
                    'description' => ['en' => $request->desc_en, 'ar' => $request->desc_ar]
                ]);
            }

            $imageAds = $this->imageAdsModel::with('ads')->where('ads_id', $ads->id)->get();
            foreach ($imageAds as $imageAd)
            {
                $urlImage = 'image_ads/' . $imageAd->getRawOriginal('images');
                $this->deleteFile($urlImage);
                $imageAd->delete();
            }

            if ($request->hasFile('images'))
            {

               foreach ($request->file('images') as $image)
               {
                   $newImages = $this->upload($image, 'image_ads');
                   $this->imageAdsModel::create([
                       'images' => $newImages,
                       'ads_id' => $ads->id
                   ]);

               }

            }


        });

        toast( trans('admin/ads.update-data'), 'update');
        return redirect(route('admin.ads.index'));
    }

    public function destroy($ads)
    {
        if ($ads)
        {
            DB::transaction(function () use ($ads) {
                if ($ads->main_image)
                {
                    $urlImage = 'ads/'. $ads->getRawOriginal('main_image');
                    $this->deleteFile($urlImage);
                }

                if ($ads->imageAd != null)
                {
                    $images = ImageAds::where('ads_id', $ads->id)->get();
                    foreach ($images as $image)
                    {
                        $urlImage2 = 'image_ads/' . $image->getRawOriginal('images');
                        $this->deleteFile($urlImage2);
                    }
                }

            });

            $ads->destroy(request('ads_id'));
            toast(trans('admin/ads.delete-data'), 'Delete');
        }
        else
        {
            toast(trans('admin/ads.no-data'), 'Error');
        }

        return back();
    }
}
