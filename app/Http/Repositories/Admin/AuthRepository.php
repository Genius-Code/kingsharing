<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\AuthInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AuthRepository implements AuthInterface
{
    private $userModel;

    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    public function loginPage()
    {
        return view('Admin.auth.login');
    }

    public function login($request)
    {
        $credentials = $request->only(['email', 'password']);

        if (Auth::attempt($credentials)){
            toast(trans('admin/login.welcome' ). \auth()->user()->name,'success');
            return redirect(route('admin.dashboard'));
        }
        return redirect()->back()->withErrors(['error' => trans('admin/login.invalid-login')]);

    }

    public function logout($request)
    {
        Session::flush();
        Auth::logout();
        return redirect(route('admin.loginPage'));
    }
}
