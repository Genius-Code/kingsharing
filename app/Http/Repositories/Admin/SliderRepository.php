<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\SliderInterface;
use App\Http\Traits\SliderTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Slider;

class SliderRepository implements SliderInterface
{
    use SliderTrait, UploaderTrait;

    private $sliderModel;

    public function __construct(Slider $slider)
    {
        $this->sliderModel = $slider;
    }

    public function index()
    {
        $sliders = $this->showAllSliders();
        return view('Admin.sliders.index', compact('sliders'));
    }

    public function create()
    {
        return view('Admin.sliders.create');
    }

    public function store($request)
    {
        $image = $this->upload($request->image, 'sliders', null);
        $this->sliderModel::create([
            'status' => isset($request->status) ? 'Published' : 'Not Published',
            'image' => $image
        ]);
        toast(trans("admin/slider.store-data"), 'Success');
        return redirect(route('admin.slider.index'));
    }

    public function edit($slider)
    {
        return view('Admin.sliders.edit', compact('slider'));
    }

    public function update($request)
    {
        $slider = $this->sliderModel::find($request->slider_id);
        if ($request->hasFile('image'))
        {
            $image = $this->upload($request->image, 'sliders', $slider->getRawOriginal('image'));
        }
        $slider->update([
            'image' => isset($image) ? $image : $slider->getRawOriginal('image'),
            'status' => isset($request->status) ? 'Published' : 'Not Published',
        ]);

        toast( trans("admin/slider.update-data"), 'Update');
        return redirect(route('admin.slider.index'));
    }

    public function destroy($slider)
    {
        if ($slider)
        {
            if ($slider->image)
            {
                $imageUrl = 'sliders/'. $slider->getRawOriginal('image');
                $this->deleteFile($imageUrl);
            }
            $slider->delete();
            toast(trans('admin/slider.delete-data'), 'Delete');
            return redirect()->back();
        }
    }
}
