<?php

namespace App\Http\Repositories\Admin;

use App\Http\Interfaces\Admin\ProfileInterface;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileRepository implements ProfileInterface
{
    private $userModel, $profileModel;

    public function __construct(User $user, Profile $profile)
    {
        $this->userModel = $user;
        $this->profileModel = $profile;
    }

    public function index()
    {
        $data = $this->profileModel::with('userProfile')->where('id', auth()->user()->id)->firstOrFail();
        return view('Admin.profile.index', compact('data'));
    }

    public function update($request)
    {
        \DB::transaction(function () use ($request) {

            $profile = $this->profileModel::findOrFail($request->profile_id);
            $user = $this->userModel::findOrFail(\Auth::user()->id);

            if ($request->hasFile('image')) {
                $image = $this->upload($request->image, 'profiles', $profile->getRawOriginal('image'));
            }

            $profile->update([
                'image'           =>  isset($image) ? $image : $profile->getRawOriginal('image'),
                'whatsapp_number' =>  $request->whatsapp,
                'facebook_url'    =>  $request->facebook,
                'user_id'         =>  \Auth::user()->id,
                'country'         =>  $request->country,
            ]);

            $user->update([
                'email' => $request->email,
                'name' => $request->name,
                'phone' => $request->phone
            ]);
        });
        toast(trans('admin/user.update-profile'), 'update');
        return redirect()->back();

    }

    public function passwordPage()
    {
        return view('Admin.profile.password');
    }

    public function changePassword($request): RedirectResponse
    {
        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->old_password, $hashedPassword)) {

            if (!Hash::check($request->password, $hashedPassword)) {
                $user = $this->userModel::where('id', auth()->user()->id)->select('id', 'password')->first();
                $user->update([
                    'password' => Hash::make($request->password)
                ]);
                toast(trans('admin/user.update-password'), 'success');
            }
            else{
                toast(trans('admin/user.old-password'), 'warning');
            }
        }
        else{
            toast(trans('admin/user.not-match'), 'warning');
        }
        return redirect()->back();
    }
}
