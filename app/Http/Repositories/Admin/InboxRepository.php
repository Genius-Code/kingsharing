<?php

namespace App\Http\Repositories\Admin;

use App\Http\Interfaces\Admin\InboxInterface;
use App\Http\Traits\InboxTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Message;
use App\Models\ReplayMessage;

class InboxRepository implements InboxInterface
{
    use InboxTrait, UploaderTrait;

    private $messageModel, $replayMessageModel;

    public function __construct(Message $message, ReplayMessage $replayMessage)
    {
        $this->messageModel       = $message;
        $this->replayMessageModel = $replayMessage;
    }

    public function index()
    {
        $allMessages = $this->allMessages();

        return view('Admin.message.messages', compact('allMessages'));
    }

    public function show($message)
    {
        if (request()->routeIs('admin.message.show', $message)) {

            $message->update([
                'status' => 'read'
            ]);

        }
        $data = $this->replayMessageModel::with('message')->orderBy('id', 'DESC' )->get();
        return view('Admin.message.show', compact('message', 'data'));
    }

    public function replayPage($message)
    {
        return view('Admin.message.replay', compact('message'));
    }

    public function replayTo($request)
    {
        if ($request->hasFile('file')) {
            $file = $this->upload($request->file, 'file-replay', null);
        }

        $this->replayMessageModel::create([
           'message_id' => $request->message_id,
           'replay' => $request->description,
           'file' => isset($file) ? $file : '',
           'sender_id' => \Auth::user()->id,
            'recipient_id' => $request->recipient
        ]);

        toast(trans('admin/message-clients.message-sent'), 'success');
        return redirect(route('admin.message.index'));
    }

    public function delete($message)
    {
        if ($message) {

            $message->forceDelete();
        }
        toast(trans('admin/message-clients.delete-data'), 'delete');
        return back();
    }

    public function softDelete($message)
    {
        if ($message) {
            $message->delete();
        }
        toast(trans('admin/message-clients.delete-data'), 'delete');
        return back();
    }

    public function deletedMeassges()
    {
        $deletedMessages = $this->deletedMessages();
        return view('Admin.message.deleted-messages', compact('deletedMessages'));
    }

    public function restoreDeletedMeassges($id)
    {

        $this->messageModel::withTrashed()->find($id)->restore();

        toast(trans('admin/message-clients.restore-data'), 'success');
        return back();
    }
}
