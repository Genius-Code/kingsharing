<?php

namespace App\Http\Repositories\Admin;

use App\Http\Interfaces\Admin\CccamServerInterface;
use App\Http\Traits\CccamServerTrait;
use App\Models\CccamServer;
use Illuminate\Http\RedirectResponse;

class CccamServerRepository implements CccamServerInterface
{
    use CccamServerTrait;

    private $cccamServerModel;

    public function __construct(CccamServer $cccamServer)
    {
        $this->cccamServerModel = $cccamServer;
    }

    public function index()
    {
        $servers = $this->showAllServers();

        return view('Admin.cccam.index', compact('servers'));
    }

    public function create()
    {
        return view('Admin.cccam.create');
    }

    public function store($request)
    {
        $this->cccamServerModel::create([
            'name'     => ['en' => $request->name_en, 'ar' => $request->name_ar],
            'details'  => $request->desc_en,
            'price'    => $request->price,
            'duration' => $request->duration
        ]);

        toast(trans('admin/cccam.data-store'), 'success');
        return redirect(route('admin.cccam.index'));
    }

    public function edit($cccam)
    {
        return view('Admin.cccam.edit', compact('cccam'));
    }

    public function update($cccam, $request)
    {
        $cccam->update([
            'name'     => ['en' => $request->name_en, 'ar' => $request->name_ar],
            'details'  => $request->desc_en,
            'price'    => $request->price,
            'duration' => $request->duration
        ]);

        toast(trans('admin/cccam.data-update'), 'success');
        return redirect(route('admin.cccam.index'));
    }

    public function delete($cccam): RedirectResponse
    {
        $cccam->destroy($cccam->id);
        toast(trans('admin/cccam.data-delete'), 'success');
        return back();

    }
}
