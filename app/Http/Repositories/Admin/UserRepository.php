<?php

namespace App\Http\Repositories\Admin;

use App\Http\Interfaces\Admin\UserInterface;
use App\Http\Traits\UsersTrait;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserInterface
{
    use UsersTrait;

    private $userModel, $roleModel, $userRoleModel;

    public function __construct(User $user, Role $role, UserRole $userRole)
    {
        $this->userModel = $user;
        $this->roleModel = $role;
        $this->userRoleModel = $userRole;
    }

    public function index()
    {
        $users = $this->showAllUsers();
        return view('Admin.users.index', compact('users'));
    }

    public function show($user)
    {
        return view('Admin.users.show', compact('user'));
    }

    public function addNewUserPage()
    {
        return view('Admin.users.create');
    }

    public function addNewUser($request)
    {
        \DB::transaction(function () use ($request) {

          $user =  $this->userModel::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password)
            ]);

          $role = $this->roleModel::where('name', 'user')->first();

          $this->userRoleModel::create([
              'user_id' => $user->id,
              'role_id' => $role->id
          ]);

        });
        toast(trans('admin/user.store-data'), 'Success');
        return redirect(route('admin.user.index'));
    }

    public function editUser($user)
    {
        return view('Admin.users.edit', compact('user'));
    }

    public function updateUser($user, $request)
    {

        if ($user) {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password)
            ]);
            toast(trans('admin/user.update-data'), 'message');
        }else{
            toast(trans('admin/user.no-data'), 'error');
        }
        return redirect(route('admin.user.index'));
    }

    public function delete($user)
    {
        $user->delete();
        toast(trans('admin/user.delete-data'), 'success');
        return back();
    }
}
