<?php


namespace App\Http\Repositories\Admin;


use App\Http\Traits\ReviewTrait;
use App\Models\Review;

class ReviewRepository implements \App\Http\Interfaces\Admin\ReviewInterface
{
    use ReviewTrait;
    private $reviewModel;

    public function __construct(Review $review)
    {
        $this->reviewModel = $review;
    }

    public function index()
    {
        $reviews = $this->showAllReview();
        return view('Admin.review.index', compact('reviews'));
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function store($request)
    {
        $this->reviewModel::create([
            'email' => $request->email,
            'name' => $request->name,
            'description' => $request->dsecription
        ]);

        toast(trans('admin/review.store-data'), 'success');
        return redirect()->back();
    }

    public function update($review)
    {
        if ($review)
        {
            $review->update([
                'status' => 'accept',
            ]);
            toast(trans('admin/review.update-data'), 'success');
        }
        else
        {
            toast(trans('admin/review.no-data'), 'error');
        }
        return redirect()->back();
    }

    public function destroy($review)
    {
        if ($review)
        {
            $review->destroy(request('review_id'));

            toast(trans('admin/review.delete-data'), 'success');
        }
        else
        {
            toast(trans('admin/review.no-data'), 'error');
        }
        return redirect()->back();
    }

    public function unread()
    {
        $reviews = $this->showUnReadReview();
        return view('Admin.review.unread', compact('reviews'));
    }
}
