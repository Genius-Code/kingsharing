<?php

namespace App\Http\Repositories\Admin;

use App\Http\Interfaces\Admin\ContactInterface;
use App\Http\Traits\ContactTrait;
use App\Models\Contact;

class ContactRepository implements ContactInterface
{
    use ContactTrait;
    private $contactModel;

    public function __construct(Contact $contact)
    {
        $this->contactModel = $contact;
    }

    public function index()
    {
        $contacts = $this->showAllContacts();
        return view('Admin.contact.index', compact('contacts'));
    }

    public function show($contact)
    {
        $contact->update([
            'status' => 'read'
        ]);
        return view('Admin.contact.show', compact('contact'));
    }

    public function delete($contact)
    {
        $contact->delete();
        toast(trans('admin/contact.delete-data'), 'message');
        return back();
    }
}
