<?php


namespace App\Http\Repositories\Admin;


use App\Http\Interfaces\Admin\BlogInterface;
use App\Http\Traits\BlogTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Blog;
use Illuminate\Http\RedirectResponse;

class BlogRepository implements BlogInterface
{
    use UploaderTrait, BlogTrait;

    private $blogModel;

    public function __construct(Blog $blog)
    {
        $this->blogModel = $blog;
    }

    public function index()
    {
        $blogs = $this->showAllBlogs();
        return view('Admin.blog.index', compact('blogs'));
    }

    public function create()
    {
        return view('Admin.blog.create');
    }

    public function store($request)
    {
        $image = $this->upload($request->image, 'blogs', null);
        $this->blogModel::create([
            'image' => $image,
            'title' => ['en' => $request->title_en, 'ar' => $request->title_ar],
            'description' => $request->description
        ]);
        toast( trans('admin/blog.store-data'), 'success');
        return redirect(route('admin.blog.index'));
    }

    public function edit($blog)
    {
        return view('Admin.blog.edit', compact('blog'));
    }

    public function update($request)
    {
        $blog = $this->getBlogById($request->blog_id);
        if ($blog)
        {
            if ($request->hasFile('image'))
            {
                $image = $this->upload($request->image, 'blogs', $blog->getRawOriginal('image'));
            }

            $blog->update([
                'image' => isset($image) ? $image : $blog->getRawOriginal('image'),
                'title' => ['en' => $request->title_en, 'ar' => $request->title_ar],
                'description' => $request->description
            ]);

            toast( trans('admin/blog.update-data'), 'success');
        }
        else
        {
            toast(trans('admin/blog.no-data'), 'error');
        }
        return redirect(route('admin.blog.index'));
    }

    public function destroy($blog): RedirectResponse
    {
        if ($blog)
        {
            if ($blog->image)
            {
                $urlImage = 'blogs/' . $blog->getRawOriginal('image');
                $this->deleteFile($urlImage);
            }
            $blog->destroy(request('blog_id'));
            toast(trans('admin/blog.delete-data'), 'success');
        }
        else
        {
            toast(trans('admin/blog.no-data'), 'error');
        }
        return back();
    }
}
