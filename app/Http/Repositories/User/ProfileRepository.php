<?php

namespace App\Http\Repositories\User;

use App\Http\Interfaces\User\ProfileInterface;
use App\Http\Traits\PofileTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Profile;
use App\Models\User;

class ProfileRepository implements ProfileInterface
{
    use PofileTrait, UploaderTrait;

    private $profileModel, $userModel;

    public function __construct(Profile $profile, User $user)
    {
        $this->profileModel = $profile;
        $this->userModel = $user;
    }

    public function index()
    {
        $data = $this->profileModel::with('userProfile')->where('user_id', auth()->user()->id)->first();
        return view('User.profile.index', compact('data'));
    }

    public function update($request)
    {
        \DB::transaction(function () use ($request) {

            $profile = $this->profileModel::findOrFail($request->profile_id);
            $user = $this->userModel::findOrFail(\Auth::user()->id);

            if ($request->hasFile('image')) {
                $image = $this->upload($request->image, 'profiles', $profile->getRawOriginal('image'));
            }

            $profile->update([
                    'image'           =>  isset($image) ? $image : $profile->getRawOriginal('image'),
                    'whatsapp_number' =>  $request->whatsapp,
                    'facebook_url'    =>  $request->facebook,
                    'user_id'         =>  \Auth::user()->id,
                    'country'         =>  $request->country,
            ]);

            $user->update([
                'email' => $request->email,
                'name' => $request->name,
                'phone' => $request->phone
            ]);
        });
        toast(trans('user/profile.update-data'), 'update');
        return redirect()->back();
    }
}
