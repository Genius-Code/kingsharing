<?php

namespace App\Http\Repositories\User;

use App\Http\Interfaces\User\ReviewInterface;
use App\Http\Traits\UploaderTrait;
use App\Models\Review;
use RealRashid\SweetAlert\Facades\Alert;
use function redirect;
use function view;

class ReviewRepository implements ReviewInterface
{
    use UploaderTrait;
    private $reviewModel;

    public function __construct(Review $review)
    {
        $this->reviewModel = $review;
    }

    public function reviewPage()
    {
        return view('User.review.create');
    }

    public function storeReview($request)
    {

        $this->reviewModel::create([
            'name' => auth()->user()->name,
            'email' => auth()->user()->email,
            'comment' => $request->description,
        ]);
        Alert::success('Success', trans('user/review.data-store'));
        return redirect()->back();
    }
}
