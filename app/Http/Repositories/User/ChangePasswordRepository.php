<?php

namespace App\Http\Repositories\User;

use App\Http\Interfaces\User\ChangePasswordInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class ChangePasswordRepository implements ChangePasswordInterface
{
    private $userModel;

    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    public function index()
    {
        $user = $this->userModel::where('id', auth()->user()->id)->select('id', 'password')->first();
        return view('User.password.index', compact('user'));
    }

    public function updatePassword($user, $request)
    {
        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->old_password, $hashedPassword)) {

            if (!Hash::check($request->password, $hashedPassword)) {
                $user = $this->userModel::where('id', auth()->user()->id)->select('id', 'password')->first();
                $user->update([
                    'password' => Hash::make($request->password)
                ]);
                toast(trans('user/password.update-password'), 'success');
            }
            else{
                toast(trans('user/password.not-old'), 'warning');
            }
        }
        else{
            toast(trans('user/password.not-match'), 'warning');
        }
        return redirect()->back();

    }
}
