<?php

namespace App\Http\Repositories\User;

use App\Http\Interfaces\User\HomeInterface;
use Stevebauman\Location\Facades\Location;

class HomeRepository implements HomeInterface
{

    public function dashboard()
    {
        $locationData = Location::get('https://'. request()->ip());
        return view('User.index', compact('locationData'));
    }
}
