<?php

namespace App\Http\Repositories\User;

use App\Http\Interfaces\User\InboxInterface;
use App\Http\Traits\InboxTrait;
use App\Http\Traits\UploaderTrait;
use App\Models\Message;
use App\Models\ReplayMessage;
use App\Models\User;

class InboxRepository implements InboxInterface
{
    use InboxTrait, UploaderTrait;

    private $messageModel, $replayMessageModel;

    public function __construct(Message $message, ReplayMessage $replayMessage)
    {
        $this->messageModel        = $message;
        $this->replayMessageModel  = $replayMessage;
    }

    public function index()
    {
        $allMessages = $this->replayMessageModel::with('message')->where('recipient_id', auth()->user()->id)->orderBy('id', 'desc')->paginate(10);

        return view('User.message.index', compact('allMessages'));
    }

    public function show($message)
    {
        if ($message) {
            $message->update([
                'status' => 'read'
            ]);
        }
        return view('User.message.show', compact('message'));
    }

    public function sendMessagePage()
    {
        return view('User.message.send-message');
    }

    public function sendMessage($request)
    {
        $admin = User::with('roles')->first();
        if ($request->hasFile('file')) {
            $file = $this->upload($request->file, 'files', null);
        }
        $this->messageModel::create([
            'title' => $request->title,
            'description' => $request->description,
            'file' => isset($file) ? $file : '',
            'sender_id' => auth()->user()->id,
            'recipient_id' => $admin->id
        ]);
        toast(trans('user/message.send-data'), 'success');
        return redirect(route('users.message.index'));
    }
}
