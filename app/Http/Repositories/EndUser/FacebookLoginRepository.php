<?php

namespace App\Http\Repositories\EndUser;

use App\Http\Interfaces\EndUser\FacebookInterface;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class FacebookLoginRepository implements FacebookInterface
{
    private $userModel, $userRoleModel, $roleModel, $profileModel;

    public function __construct(User $user, Role $role, UserRole $userRole, Profile $profile)
    {
        $this->userModel     = $user;
        $this->userRoleModel = $userRole;
        $this->roleModel     = $role;
        $this->profileModel  = $profile;
    }

    CONST DRIVER_TYPE = 'facebook';

    public function handleFacebookRedirect()
    {
        return Socialite::driver(static::DRIVER_TYPE)->redirect();
    }

    public function handleFacebookCallback()
    {
        try {

            $fbUser = Socialite::driver(static::DRIVER_TYPE)->stateless()->user();
            $userExisted = $this->userModel::where('provider_id', $fbUser->id)->where('provider_type', static::DRIVER_TYPE)->first();

            if($userExisted) {

                Auth::login($userExisted);

            }else {

                $newUser = $this->userModel::create([
                    'name' => $fbUser->name,
                    'email' => $fbUser->email,
                    'phone' => '',
                    'provider_id' => $fbUser->id,
                    'provider_type' => static::DRIVER_TYPE,
                    'password' => Hash::make($fbUser->id)
                ]);

                $role = $this->roleModel::where('name', 'user')->first();

                $this->userRoleModel::create([
                    'user_id' => $newUser->id,
                    'role_id' => $role->id
                ]);

                $this->profileModel::create([
                    'user_id' => $newUser->id
                ]);
                Auth::login($newUser);

            }
            return redirect()->route('users.home');


        } catch (Exception $e) {
            return  $e->getMessage();
        }
    }
}
