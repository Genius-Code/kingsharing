<?php


namespace App\Http\Repositories\EndUser;


use App\Http\Interfaces\EndUser\ContactInterface;
use App\Models\Contact;
use RealRashid\SweetAlert\Facades\Alert;

class ContactRepository implements ContactInterface
{
    private $contactModel;

    public function __construct(Contact $contact)
    {
        $this->contactModel = $contact;
    }

    public function contactPage()
    {
        return view('EndUser.contact-us');
    }

    public function sendMessage($request)
    {
        $data = $request->all();
        $this->contactModel::create($data);

        \Mail::send('email.contact_email', [

            'name' => $data['name'],

            'email' => $data['email'],

            'phone' => $data['phone'],

            'subject' => $data['subject'],

            'body' => $data['body'],

        ], function($message) use ($request){

            $message->from($request->email);

            $message->to('dreamshar14@gmail.com', 'Admin')->subject($request->get('subject'));
        });
        Alert::success('success','Your Message Has Been Sent, We Will be Replay soon ');
        return redirect()->back();
    }
}
