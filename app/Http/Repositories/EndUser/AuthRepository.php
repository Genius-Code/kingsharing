<?php

namespace App\Http\Repositories\EndUser;

use App\Http\Interfaces\EndUser\AuthInterface;
use App\Models\PasswordReset;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use RealRashid\SweetAlert\Facades\Alert;

class AuthRepository implements AuthInterface
{
    private $userModel, $roleModel, $userRoleModel, $profileModel;

    public function __construct(User $user, Role $role, UserRole $userRole, Profile $profile)
    {
        $this->userModel          = $user;
        $this->roleModel          = $role;
        $this->userRoleModel      = $userRole;
        $this->profileModel       = $profile;
    }

    public function authPage()
    {
        return view('EndUser.auth.login');
    }

    public function login($request)
    {
        $userData = $request->only('email', 'password');
        if (Auth::attempt($userData)) {
            return redirect(route('users.home'));
        }
        Alert::warning('error', 'Email Or Password Is Wrong');
        return redirect()->back();
    }

    public function registerPage()
    {
        return view('EndUser.auth.register');
    }

    public function register($request)
    {
        \DB::transaction(function () use ($request) {
            $user = $this->userModel::create([
                'name'     => $request->name,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'password' => Hash::make($request->password)
            ]);

            $role = $this->roleModel::where('name', 'user')->first();

            $this->userRoleModel::create([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);

            $this->profileModel::create([
                'user_id' => $user->id
            ]);

        });
        toast('Your Account Created Successfully, You can Login Now');
        return redirect(route('auth'));
    }

    public function logout($request)
    {
        Session::flush();
        Auth::logout();
        return redirect(route('user.index'));
    }

    public function redirectToService($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function handleCallback($service)
    {
//        $user = Socialite::with($service)->stateless()->user();
//        return response()->json($user);

        try {
            $userService = Socialite::driver($service)->user();
            //dd($userService);
            $notOldUser = $this->userModel::where('provider_id', $userService->getId())->doesntExist();
            if ($notOldUser) {
                $user = new User();
                $user->name = $userService->name;
                $user->email = isset($userService->email) ? $userService->email : $userService->name . rand(0, 999) . '@mail.com';
                $user->password = Hash::make($userService->id);
                $user->provider_id = $userService->id;
                $user->provider_name = $service;
                $user->save();
                Auth::login($user);
            }else{
                Auth::login($notOldUser);
            }
            return redirect(route('users.home'));


        } catch (Exception $e) {
            return back();
        }
    }

    public function forgetPasswordPage()
    {
        return view('EndUser.auth.forget-password');
    }

    public function forgetPassword($request): RedirectResponse
    {
        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        Mail::send('email.forgetPassword', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });

        toast( 'We have e-mailed your password reset link!', 'message');
        return back();
    }

    public function resetPasswordPage($token)
    {
        return view('EndUser.auth.reset-password', compact('token'));
    }

    public function resetPassword($request)
    {
        $updatePassword = DB::table('password_resets')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])
            ->first();

        if(!$updatePassword){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $user = User::where('email', $request->email)
            ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        return redirect('/auth')->with('message', 'Your password has been changed!');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {

            $user = Socialite::driver('google')->user();

            $finduser = User::where('provider_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);

                return redirect(route('users.home'));

            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone' => isset($user->phone) ? $user->phone : '01234567890',
                    'provider_id'=> $user->id,
                    'password' => encrypt('123456dummy')
                ]);

                Auth::login($newUser);

                return redirect(route('users.home'));
            }

        } catch (Exception $e) {
            ($e->getMessage());
        }
    }

}
