<?php

namespace App\Http\Repositories\EndUser;

use App\Http\Interfaces\EndUser\CccamServerInterface;
use App\Http\Traits\CccamServerTrait;
use App\Models\CccamServer;

class CccamServerRepository implements CccamServerInterface
{
    use CccamServerTrait;

    private $cccamServerModel;

    public function __construct(CccamServer $cccamServer)
    {
        $this->cccamServerModel = $cccamServer;
    }

    public function index()
    {
        $servers = $this->showAllServers();
        return view('EndUser.cccam', compact('servers'));
    }
}
