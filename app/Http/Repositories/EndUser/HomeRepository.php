<?php


namespace App\Http\Repositories\EndUser;


use App\Http\Interfaces\EndUser\HomeInterface;
use App\Http\Traits\BannersTrait;
use App\Http\Traits\BlogTrait;
use App\Http\Traits\OfferTrait;
use App\Http\Traits\PackageServerTrait;
use App\Http\Traits\ReviewTrait;
use App\Http\Traits\SliderTrait;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\OfferServer;
use App\Models\PackageServer;
use App\Models\Review;
use App\Models\Slider;
use Stevebauman\Location\Facades\Location;

class HomeRepository implements HomeInterface
{

    use SliderTrait, BannersTrait, PackageServerTrait, OfferTrait, BlogTrait, ReviewTrait;

    private $sliderModel;
    private $bannerModel;
    private $packageServerModel;
    private $offerServerModel;
    private $blogModel;
    private $reviewModel;

    public function __construct(Slider $slider, Banner $banner, PackageServer $packageServer, OfferServer $offerServer, Blog $blog, Review $review)
    {
        $this->sliderModel        = $slider;
        $this->bannerModel        = $banner;
        $this->packageServerModel = $packageServer;
        $this->offerServerModel   = $offerServer;
        $this->blogModel          = $blog;
        $this->reviewModel        = $review;
    }

    public function HomePage()
    {
        $locationData = Location::get('https://'. request()->ip()); // https or http according to your necessary.
        //dd($locationData);
//        @if ($locationData->countryCode == 'US')
//        Thanks
//                    @else

        $sliders  = $this->showPublishedSlider();
        $banners  = $this->showAllBanners();
        $packages = $this->showAllPackages();
        $offers   = $this->showApprovedOffers();
        $blogs    = $this->showAllBlogs();
        $reviews  = $this->showAcceptReview();
        return view('EndUser.index', compact('locationData', 'banners', 'sliders', 'packages', 'offers', 'blogs', 'reviews'));
    }
}
