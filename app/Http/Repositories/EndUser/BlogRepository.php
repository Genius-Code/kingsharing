<?php

namespace App\Http\Repositories\EndUser;

use App\Http\Interfaces\EndUser\BlogInterface;
use App\Http\Traits\BlogTrait;
use App\Models\Blog;

class BlogRepository implements BlogInterface
{
    use BlogTrait;

    private $blogModel;

    public function __construct(Blog $blog)
    {
        $this->blogModel = $blog;
    }

    public function index()
    {
        $blogs = $this->blogModel::paginate(6);

        return view('EndUser.blogs.index', compact('blogs'));
    }

    public function show($blog)
    {
        return view('EndUSer.blogs.show', compact('blog'));
    }
}
