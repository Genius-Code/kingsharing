<?php

namespace App\Http\Requests\User\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'profile_id' => 'required|exists:profiles,id',
            'name'       => 'required',
            'email'      => 'required|email',
            'image'      => 'image|mimes:png,jpg,jpeg,webp',
            'country'    => 'required',
            'whatsapp'   => 'required'
        ];
    }
}
