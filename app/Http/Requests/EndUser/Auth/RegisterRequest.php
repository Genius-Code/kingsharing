<?php

namespace App\Http\Requests\EndUser\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'                   => 'required|string|min:3',
            'email'                  => 'required|email',
            'phone'                  => 'required|min:10',
            'password'               => 'required|confirmed|min:8',
            'g-recaptcha-response'   => 'required|captcha'
        ];
    }
}
