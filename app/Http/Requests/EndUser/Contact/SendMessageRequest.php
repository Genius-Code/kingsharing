<?php

namespace App\Http\Requests\EndUser\Contact;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email',
            'phone' => 'required|numeric|min:10',
            'subject' => 'required|string',
            'body' => 'required|string|min:10',
            'g-recaptcha-response'   => 'required|captcha'
        ];
    }
}
