<?php

namespace App\Http\Requests\Admin\Cccam;

use Illuminate\Foundation\Http\FormRequest;

class CccamServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name_ar'  => 'required|string|min:3',
            'name_en'  => 'required|string|min:3',
            'desc_en'  => 'required|string',
            'duration' => 'required|string',
            'price'    => 'required|string',
        ];
    }

    public function attributes(): array
    {
        return [
             'name_ar'  => trans('admin/cccam.name_ar'),
             'name_en'  => trans('admin/cccam.name_en'),
             'desc_en'  => trans('admin/cccam.details-en'),
             'price'    => trans('admin/cccam.price'),
             'duration' => trans('admin/cccam.duration'),
        ];
    }
}
