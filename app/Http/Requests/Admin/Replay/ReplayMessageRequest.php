<?php

namespace App\Http\Requests\Admin\Replay;

use Illuminate\Foundation\Http\FormRequest;

class ReplayMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'message_id'  => 'required|exists:messages,id',
            'description' => 'required|string',
            'file'        => 'sometimes'
        ];
    }
}
