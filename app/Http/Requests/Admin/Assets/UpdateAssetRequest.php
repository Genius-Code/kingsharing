<?php

namespace App\Http\Requests\Admin\Assets;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_id' => 'required|exists:assets,id',
            'email' => 'required|email',
            'address' => 'required|min:3',
            'phone' => 'required|min:10',
            'logo' => 'mimes:png,jpg,jpeg,webp',
            'facebook' => 'required|url',
            'telegram' => 'required|url',
            'youtube' => 'required|url',
            'instagram' => 'required|url'
        ];
    }
}
