<?php

namespace App\Http\Requests\Admin\PackageServer;

use Illuminate\Foundation\Http\FormRequest;

class AddPackageServer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|regex:/(^([a-zA-Z ]+)(\d+)?$)/u',
            'name_ar' => 'required|regex:/\p{Arabic}/u',
            'price' => 'required|numeric',
            'duration' => 'required|numeric',
            'desc_en' => 'required|min:10|string',
            'desc_ar' => 'required|min:10|regex:/\p{Arabic}/u',
        ];
    }
}
