<?php

namespace App\Http\Requests\Admin\Inbox;

use Illuminate\Foundation\Http\FormRequest;

class InboxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        return [
            'recipient_id' => 'required|exists:messages,sender_id',
            'title' => 'required|string|min:3',
            'description' => 'required|string|min:10'
        ];
    }

    protected function onUpdate(): array
    {
        return [
            ''
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return request()->isMethod('PUT') || request()->isMethod('PATCH') ? $this->onUpdate() : $this->onCreate() ;
    }
}
