<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|min:10|numeric',
            'password' => 'required|min:8|max:30|string'
        ];
    }

    protected function onUpdate()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:users,email,'.$this->user->id,
            'phone' => 'required|min:10|numeric',
            'password' => 'required|min:8|max:30|string'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return (request()->isMethod('PUT') || request()->isMethod('PATCH')) ? $this->onUpdate() : $this->onCreate();
    }
}
