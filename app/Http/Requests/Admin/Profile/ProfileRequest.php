<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * valadation create
     * @return string[]
     */
    protected function onCreate(): array
    {
        return [
            'name' => 'required|regex:/^[A-Za-z ]*$/i',
            'email' => 'required|email',
            'country' => 'required|regex:/^[A-Za-z ]*$/i'
        ];
    }

    /**
     * validation update
     * @return string[]
     */
    protected function onUpdate(): array
    {
        return [
            'name' => 'required|regex:/^[A-Za-z ]*$/i',
            'email' => 'required|email',
            'country' => 'required|regex:/^[A-Za-z ]*$/i'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onCreate();
    }
}
