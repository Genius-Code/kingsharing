<?php

namespace App\Http\Requests\Admin\Ads;

use Illuminate\Foundation\Http\FormRequest;

class AddNewAds extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'desc_en' => 'required|min:10|regex:/(^([a-zA-Z ]+)(\d+)?$)/u',
            'desc_ar' => 'required|min:10|regex:/\p{Arabic}/u',
            'main_image' => 'required|image|mimes:png,jpg,jpeg,webp',
            'images[]' => 'image|mimes:png,jpg,jpeg,webp'
        ];
    }
}
