<?php

namespace App\Http\Requests\Admin\Blog;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blog_id' => 'required|exists:blogs,id',
            'title_en' => 'required|regex:/(^([a-zA-Z ]+)(\d+)?$)/u',
            'title_ar' => 'required|regex:/\p{Arabic}/u',
            'description' => 'required|min:10|string',
            'image' => 'image|mimes:png,jpg,jpeg,webp',
        ];
    }
}
