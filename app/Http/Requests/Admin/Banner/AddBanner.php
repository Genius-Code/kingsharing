<?php

namespace App\Http\Requests\Admin\Banner;

use Illuminate\Foundation\Http\FormRequest;

class AddBanner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'required|regex:/(^([a-zA-Z0-9 ]+)(\d+)?$)/u',
            'title_ar' => 'required|regex:/\p{Arabic}/u',
            'url' => 'required|url',
            'desc_en' => 'required|min:10',
            'desc_ar' => 'required|min:10|regex:/\p{Arabic}/u',
            'image' => 'required|image|mimes:png,jpg,jpeg,webp'
        ];
    }
}
