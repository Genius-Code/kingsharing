<?php

namespace App\Http\ViewComposers;



use App\Models\Contact;
use Illuminate\Contracts\View\View;

class MailNotificationsComposer
{

    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $contactMails = 0 ;

        $contactMails = Contact::where('status', 'unread')->get();

        $view->with('contactMails', $contactMails);
    }
}
