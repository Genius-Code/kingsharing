<?php

namespace App\Http\ViewComposers;


use App\Models\Message;
use App\Models\ReplayMessage;
use Illuminate\Contracts\View\View;

class MessagesComposer
{

    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $messages = '';
        $userMessages = 0;
        $countMessages = 0 ;
        if (auth()->user()) {
            $messages = Message::where('status', 'unread')->where('recipient_id',  \Auth::user()->id)->get();
            $countMessages = Message::where('status', 'unread')->where('recipient_id',  \Auth::user()->id)->get();
            $userMessages = ReplayMessage::with('message')
                ->where('recipient_id', \Auth::user()->id)
                ->where('status', 'unread')
                ->get();
        }

        $view->with('messages', $messages);
        $view->with('countMessages', $countMessages);
        $view->with('userMessages', $userMessages);
    }
}
