<?php

namespace App\Http\Interfaces\EndUser;

interface FacebookInterface
{
    public function handleFacebookRedirect();

    public function handleFacebookCallback();
}
