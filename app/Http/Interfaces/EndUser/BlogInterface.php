<?php

namespace App\Http\Interfaces\EndUser;

interface BlogInterface
{
    public function index();

    public function show($blog);
}
