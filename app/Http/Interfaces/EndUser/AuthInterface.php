<?php

namespace App\Http\Interfaces\EndUser;

interface AuthInterface
{
    public function authPage();

    public function login($request);

    public function registerPage();

    public function register($request);

    public function logout($request);

    public function redirectToService($service);

    public function handleCallback($service);

    public function forgetPasswordPage();

    public function forgetPassword($request);

    public function resetPasswordPage($token);

    public function resetPassword($request);

    public function redirectToGoogle();

    public function handleGoogleCallback();
}
