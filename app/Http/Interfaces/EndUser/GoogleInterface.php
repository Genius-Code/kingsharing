<?php

namespace App\Http\Interfaces\EndUser;

interface GoogleInterface
{
    public function handleGoogleRedirect();

    public function handleGoogleCallback();
}
