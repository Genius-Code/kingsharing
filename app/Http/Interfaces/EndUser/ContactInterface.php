<?php


namespace App\Http\Interfaces\EndUser;


interface ContactInterface
{
    public function contactPage();

    public function sendMessage($request);
}
