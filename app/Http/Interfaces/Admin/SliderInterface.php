<?php


namespace App\Http\Interfaces\Admin;


interface SliderInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($slider);

    public function update($request);

    public function destroy($slider);
}
