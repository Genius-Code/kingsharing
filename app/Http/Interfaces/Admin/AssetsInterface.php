<?php


namespace App\Http\Interfaces\Admin;


interface AssetsInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($model);

    public function update($request);

    public function destroy($request);
}
