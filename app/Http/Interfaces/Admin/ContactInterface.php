<?php

namespace App\Http\Interfaces\Admin;

interface ContactInterface
{
    public function index();

    public function show($contact);

    public function delete($contact);
}
