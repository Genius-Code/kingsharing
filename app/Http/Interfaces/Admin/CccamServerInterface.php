<?php

namespace App\Http\Interfaces\Admin;

interface CccamServerInterface
{
    public function index();
    public function create();
    public function store($request);
    public function edit($cccam);
    public function update($cccam, $request);
    public function delete($cccam);
}
