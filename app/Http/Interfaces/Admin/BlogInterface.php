<?php


namespace App\Http\Interfaces\Admin;


interface BlogInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($blog);

    public function update($request);

    public function destroy($blog);
}
