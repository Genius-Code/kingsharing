<?php


namespace App\Http\Interfaces\Admin;


interface AdsInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($ads);

    public function update($request);

    public function destroy($ads);
}
