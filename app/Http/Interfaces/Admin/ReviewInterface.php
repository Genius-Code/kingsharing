<?php


namespace App\Http\Interfaces\Admin;


interface ReviewInterface
{
    public function index();

    public function create();

    public function store($request);

    public function update($review);

    public function destroy($review);

    public function unread();
}
