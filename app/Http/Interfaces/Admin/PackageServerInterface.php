<?php


namespace App\Http\Interfaces\Admin;


interface PackageServerInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($server);

    public function show($server);

    public function update($request);

    public function destroy($server);
}
