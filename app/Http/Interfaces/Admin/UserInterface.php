<?php

namespace App\Http\Interfaces\Admin;

interface UserInterface
{
    public function index();

    public function show($user);

    public function addNewUserPage();

    public function addNewUser($request);

    public function editUser($user);

    public function updateUser($user, $request);

    public function delete($user);
}
