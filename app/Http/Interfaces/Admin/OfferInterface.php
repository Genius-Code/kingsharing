<?php


namespace App\Http\Interfaces\Admin;


interface OfferInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($offer);

    public function update($request);

    public function destroy($offer);
}
