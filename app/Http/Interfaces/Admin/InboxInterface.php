<?php

namespace App\Http\Interfaces\Admin;

interface InboxInterface
{
    public function index();

    public function show($message);

    public function replayPage($message);

    public function replayTo($request);

    public function delete($message);

    public function softDelete($message);

    public function deletedMeassges();

    public function restoreDeletedMeassges($id);
}
