<?php

namespace App\Http\Interfaces\Admin;

interface ProfileInterface
{
    public function index();

    public function update($request);

    public function passwordPage();

    public function changePassword($request);
}
