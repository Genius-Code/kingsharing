<?php


namespace App\Http\Interfaces\Admin;


interface BannerInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($banner);

    public function update($request);

    public function destroy($banner);

}
