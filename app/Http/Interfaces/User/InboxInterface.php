<?php

namespace App\Http\Interfaces\User;

interface InboxInterface
{
    public function index();

    public function show($message);

    public function sendMessagePage();

    public function sendMessage($request);

}
