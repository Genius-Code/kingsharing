<?php

namespace App\Http\Interfaces\User;

interface ProfileInterface
{
    public function index();

    public function update($request);
}
