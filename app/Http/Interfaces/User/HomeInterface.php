<?php

namespace App\Http\Interfaces\User;

interface HomeInterface
{
    public function dashboard();
}
