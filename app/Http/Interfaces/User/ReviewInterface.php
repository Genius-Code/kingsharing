<?php

namespace App\Http\Interfaces\User;

interface ReviewInterface
{
    public function reviewPage();

    public function storeReview($request);
}
