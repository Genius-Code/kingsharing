<?php

namespace App\Http\Interfaces\User;

interface ChangePasswordInterface
{
    public function index();

    public function updatePassword($user, $request);
}
