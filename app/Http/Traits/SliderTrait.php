<?php


namespace App\Http\Traits;


trait SliderTrait
{
    private function showAllSliders()
    {
        return $this->sliderModel::get();
    }

    private function showPublishedSlider()
    {
        return $this->sliderModel::where('status', 'Published')->get();
    }
}
