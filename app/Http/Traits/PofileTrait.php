<?php

namespace App\Http\Traits;

trait PofileTrait
{
    private function showAllDataProfile($id)
    {
        return $this->profileModel::with('userProfile')->where('id', $id)->firstOrFail();
    }
}
