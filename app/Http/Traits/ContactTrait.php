<?php

namespace App\Http\Traits;

trait ContactTrait
{
    private function showAllContacts()
    {
        return $this->contactModel::orderBy('id', 'desc')->get();
    }
}
