<?php

namespace App\Http\Traits;

use App\Models\Role;

trait UsersTrait
{
    private function showAllUsers()
    {
        $role = Role::where('name', 'admin')->first();
        return $this->userModel::where('id', '!=', $role->id)->get();
    }
}
