<?php


namespace App\Http\Traits;


trait AssetsTrait
{
    private function showAllAssets()
    {
        return $this->assetModel::get();
    }

    private function showAssetById($id)
    {
        return $this->assetModel::find($id);
    }
}
