<?php


namespace App\Http\Traits;


trait OfferTrait
{
    private function showAllOffers()
    {
        return $this->offerServerModel::get();
    }

    private function showApprovedOffers()
    {
        return $this->offerServerModel::with('packageServer')->where('status', 'approve')->get();
    }

}
