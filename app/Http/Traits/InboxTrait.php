<?php

namespace App\Http\Traits;

trait InboxTrait
{
    private function allMessages()
    {
        return $this->messageModel::where('recipient_id', \Auth::user()->id)->get();
    }

    private function deletedMessages()
    {
        return $this->messageModel::onlyTrashed()->get();
    }
}
