<?php

namespace App\Http\Traits;

trait CccamServerTrait
{
    private function showAllServers()
    {
        return $this->cccamServerModel::get();
    }

    private function getServerById($id)
    {
        return $this->cccamServerModel::findOrFail($id);
    }
}
