<?php

namespace App\Http\Traits;

trait BlogTrait
{
    private function showAllBlogs()
    {
        return $this->blogModel::get();
    }

    private function getBlogById($id)
    {
        return $this->blogModel::findOrFail($id);
    }
}
