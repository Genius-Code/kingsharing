<?php


namespace App\Http\Traits;


trait ReviewTrait
{
    private function showAllReview()
    {
        return $this->reviewModel::get();
    }

    private function showAcceptReview()
    {
        return $this->reviewModel::where('status','accept')->get();
    }

    private function showUnReadReview()
    {
        return $this->reviewModel::where('status','not_accept')->get();
    }
}
