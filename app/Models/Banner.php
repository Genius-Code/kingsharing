<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Banner extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['title', 'description'];

    protected $fillable = ['title', 'description', 'image', 'url'];

    public function getImageAttribute($value): string
    {
        return env('AWS_S3_URL'). '/ban-image/' . $value;
    }
}
