<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = ['email', 'address', 'logo', 'phone', 'facebook', 'instagram', 'youtube', 'telegram'];

    public function getLogoAttribute($value)
    {
        return env('AWS_S3_URL'). '/assets/' . $value;
    }
}
