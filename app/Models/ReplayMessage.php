<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReplayMessage extends Model
{
    use HasFactory;

    protected $fillable = ['message_id', 'sender_id', 'recipient_id','status', 'file', 'replay'];

    public function message(): BelongsTo
    {
        return $this->belongsTo(Message::class, 'message_id', 'id');
    }

    public function sender(): BelongsTo
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function recieved(): BelongsTo
    {
        return $this->belongsTo(User::class, 'recipient_id', 'id');
    }

    public function getFileAttribute($value): string
    {
        return env('AWS_S3_URL'). '/file-replay/' . $value;
    }
}
