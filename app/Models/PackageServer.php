<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class PackageServer extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['name', 'details'];

    protected $fillable = ['name', 'details', 'price', 'duration'];

    public function offers(): HasMany
    {
        return $this->hasMany(OfferServer::class, 'server_id', 'id');
    }

}
