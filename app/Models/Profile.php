<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = ['image', 'facebook_url', 'whatsapp_number', 'country', 'user_id'];

    public function userProfile(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getImageAttribute($value)
    {
        return env('AWS_S3_URL'). '/profiles/' . $value;
    }
}
