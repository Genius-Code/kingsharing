<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;

    protected $fillable = ['image', 'status'];

    public function getImageAttribute($value)
    {
        return env('AWS_S3_URL'). '/sliders/' . $value;
    }
}
