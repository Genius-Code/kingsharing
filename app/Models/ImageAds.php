<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageAds extends Model
{
    use HasFactory;

    protected $fillable = ['images', 'ads_id'];

    public function ads()
    {
        return $this->belongsTo(Ads::class, 'ads_id', 'id');
    }

    public function getImagesAttribute($value)
    {
        return env('AWS_S3_URL'). '/image_ads/' . $value;
    }
}
