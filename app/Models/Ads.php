<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Ads extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['description'];

    protected $fillable = ['description', 'main_image'];

    public function getMainImageAttribute($value)
    {
        return env('AWS_S3_URL'). '/ads/' . $value;
    }

    public function imageAd()
    {
        return $this->hasMany(ImageAds::class, 'ads_id', 'id');
    }
}
