<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Blog extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['title'];

    protected $fillable = ['title', 'image', 'description'];

    public function getImageAttribute($value)
    {
        return env('AWS_S3_URL'). '/blogs/' . $value;
    }
}
