<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Translatable\HasTranslations;

class OfferServer extends Model
{
    use HasFactory;


    protected $fillable = ['status', 'image', 'duration','server_id', 'new_price'];

    public function packageServer(): BelongsTo
    {
        return $this->belongsTo(PackageServer::class, 'server_id', 'id');
    }

    public function getImageAttribute($value): string
    {
        return env('AWS_S3_URL'). '/offers/' . $value;
    }
}
