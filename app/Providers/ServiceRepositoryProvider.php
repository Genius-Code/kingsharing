<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ServiceRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Http\Interfaces\Admin\HomeInterface',
            'App\Http\Repositories\Admin\HomeRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\AuthInterface',
            'App\Http\Repositories\Admin\AuthRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\AssetsInterface',
            'App\Http\Repositories\Admin\AssetsRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\SliderInterface',
            'App\Http\Repositories\Admin\SliderRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\BannerInterface',
            'App\Http\Repositories\Admin\BannerRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\PackageServerInterface',
            'App\Http\Repositories\Admin\PackageServerRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\OfferInterface',
            'App\Http\Repositories\Admin\OfferRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\AdsInterface',
            'App\Http\Repositories\Admin\AdsRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\BlogInterface',
            'App\Http\Repositories\Admin\BlogRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\ReviewInterface',
            'App\Http\Repositories\Admin\ReviewRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\ProfileInterface',
            'App\Http\Repositories\Admin\ProfileRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\InboxInterface',
            'App\Http\Repositories\Admin\InboxRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\ContactInterface',
            'App\Http\Repositories\Admin\ContactRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\UserInterface',
            'App\Http\Repositories\Admin\UserRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\Admin\CccamServerInterface',
            'App\Http\Repositories\Admin\CccamServerRepository'
        );

        /** EndUSer **/
        $this->app->bind(
            'App\Http\Interfaces\EndUser\HomeInterface',
            'App\Http\Repositories\EndUser\HomeRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\EndUser\ContactInterface',
            'App\Http\Repositories\EndUser\ContactRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\EndUser\AuthInterface',
            'App\Http\Repositories\EndUser\AuthRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\EndUser\FacebookInterface',
            'App\Http\Repositories\EndUser\FacebookLoginRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\EndUser\GoogleInterface',
            'App\Http\Repositories\EndUser\GoogleRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\EndUser\BlogInterface',
            'App\Http\Repositories\EndUser\BlogRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\EndUser\CccamServerInterface',
            'App\Http\Repositories\EndUser\CccamServerRepository'
        );

        /** Users **/
        $this->app->bind(
            'App\Http\Interfaces\User\HomeInterface',
            'App\Http\Repositories\User\HomeRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\User\ProfileInterface',
            'App\Http\Repositories\User\ProfileRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\User\ChangePasswordInterface',
            'App\Http\Repositories\User\ChangePasswordRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\User\InboxInterface',
            'App\Http\Repositories\User\InboxRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\User\ReviewInterface',
            'App\Http\Repositories\User\ReviewRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
