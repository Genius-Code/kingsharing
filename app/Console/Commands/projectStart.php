<?php

namespace App\Console\Commands;

use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class projectStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start Project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call('migrate:fresh');
        $this->info('Database Was Migrated Successfully');

        Artisan::call('db:seed');
        $this->info('seeders was seeded');



        DB::transaction(function (){

            $name = $this->ask('Enter Your Name');
            $email = $this->ask('Enter Your Email');
            $phone = $this->ask('Enter Your Phone');
            $password = $this->secret('Enter Your Password');

            $user = User::create([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'password' => Hash::make($password)
            ]);

            $role = Role::where('name', 'admin')->first();

            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]);

            Profile::create([
                'user_id' => $user->id
             ]);
        });
        $this->info('Your Super Account was created');
        return Command::SUCCESS;
    }
}
