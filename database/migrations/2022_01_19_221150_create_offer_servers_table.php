<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_servers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('image');
            $table->double('new_price');
            $table->text('details');
            $table->string('duration');
            $table->unsignedBigInteger('server_id');
            $table->foreign('server_id')->references('id')->on('package_servers')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_servers');
    }
}
